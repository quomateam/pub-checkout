<?php

return [
    'Checkout Online' => 'Pagos en línea',

    'Payment' => 'Pago',
    'Receipt' => 'Recibo',
    'Platform' => 'Plataforma',
    'Platforms' => 'Plataformas',
    'Select Platform' => 'Seleccionar Plataforma',
    'Select a payment method' => 'Seleccione una forma de pago',
    'Main Class' => 'Clase Principal',
    'Status' => 'Estado',
    'Enabled' => 'Activo',
    'Disabled' => 'Inactivo',
    'Method of Payment' => 'Método de Pago',
    
    'Site' => 'Sitio',
    'Sites' => 'Sitios',
    'Name' => 'Nombre',
    'Assign Company' => 'Asignar Empresa',
    'Company' => 'Compañía',
    'Server Name' => 'Nombre del Servidor',
    
    'Installments' => 'Cuotas',
    'Installment' => 'Cuota',
    'Payment Method' => 'Medio de Pago',
    'Payment Methods' => 'Medios de Pago',
    'Key' => 'Clave',
    'Qty' => 'Cantidad',
    'Code' => 'Código',
    
    'Payment Method Type' => 'Tipo',
    'Payment by credit card' => 'Pago con tarjeta de crédito',
    'Printed ticket' => 'Ticket impreso',
    'Payment by ATM' => 'Pago por cajero automático(ATM)',
    'Payment by debit card' => 'Pago con tarjeta de débito',
    'Payment by prepaid card' => 'Pago por tarjeta prepago',
    
    'Payment Method Type' => 'Tipo de Medio de Pago',
    'Payment Method Types' => 'Tipos de Medio de Pago',
    'Admin Email' => 'Email de Administrador',
    'Web Payments' => 'Pagos',
    'Payments' => 'Pagos',
    'Concept' => 'Concepto',
    'UUID' => 'ID único',
    'Amount' => 'Monto',
    
    'Init' => 'Iniciado',
    'Error' => 'Error',
    'Paid' => 'Pago',
    'Pending' => 'Pendiente',
    'Partial' => 'Parcial',
    'Timeout' => 'Expirado',
    'Canceled' => 'Cancelado',
    'Rejected' => 'Rechazado',
    'Refunded' => 'Devuelto',
    'In_mediation' => 'En mediación',
    'Charged Back' => 'Cargado de nuevo',
    'Linking To Market Place' => 'Vincular Mercado Pago',
    'Your account has been linked to the Wepass Market Payment account.' => 'Su cuenta ha sido vinculada con la cuenta de Mercado Pago de Wepass.',
    'The account has already been linked' => 'La cuenta ya fue vinculada',
];
