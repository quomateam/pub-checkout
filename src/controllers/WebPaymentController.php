<?php

namespace quoma\checkout\controllers;

use Yii;
use quoma\checkout\models\WebPayment;
use quoma\checkout\models\search\WebPaymentSearch;
use quoma\core\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \quoma\checkout\platforms\MercadoPago\MercadoPago;

/**
 * WebPaymentController implements the CRUD actions for WebPayment model.
 */
class WebPaymentController extends Controller {
    
     public function init() {
        parent::init();
        //Agregado porq si no puedo enviarle post
        $this->enableCsrfValidation = false;
    }


    /**
     * Lists all WebPayment models.
     * @return mixed
     */
    public function actionIndex($site_id) {
        $searchModel = new WebPaymentSearch();
        
        $searchModel->site_id = $site_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WebPayment model.
     * @param integer $web_payment_id
     * @param integer $site_id
     * @return mixed
     */
    public function actionView($web_payment_id) {
        return $this->render('view', [
            'model' => $this->findModel($web_payment_id),
        ]);
    }

    /**
     * Creates a new WebPayment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($site_id) {
        $model = new WebPayment();
        $model->site_id = $site_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'web_payment_id' => $model->web_payment_id, 'site_id' => $model->site_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing WebPayment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $web_payment_id
     * @param integer $site_id
     * @return mixed
     */
    public function actionUpdate($web_payment_id) {
        $model = $this->findModel($web_payment_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'web_payment_id' => $model->web_payment_id, 'site_id' => $model->site_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing WebPayment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $web_payment_id
     * @param integer $site_id
     * @return mixed
     */
    public function actionDelete($web_payment_id) {
        $this->findModel($web_payment_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * TODO: verificar si se usa, caso contrario quitar
     *
     * Chequea el estado del pago de un web_payment
     * @return type
     */
    public function actionCancelPayment() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = $_POST['data'];

        $web_payment = WebPayment::find()->where(['request_uuid' => $data])->one();
       
        if (!$web_payment) {
            return [
                'status' => 'error',
                'message' => 'No se pudo encontrar el Pago en Checkout',
            ];
        }

        try {
            //si aun no se ha aprobado cancelo el pago, puede existir más de un pago en MP para el mismo external reference
            $response = MercadoPago::cancelPayment($web_payment);
        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => 'No se pudo cancelar el Pago en Mercado Pago',
                'exception' => $exc->getTraceAsString()
            ];
        }
        echo "MercadoPago::cancelPayments result:". implode($response);
        echo "\n";

        if ($response['status'] != 'success') {
            return [
                'status' => 'error',
                'message' => 'No se pudo cancelar el Pago en Mercado Pago',
            ];
        }

        $web_payment->changeStatus('canceled');

        return [
            'status' => 'ok',
        ];
    }

    /**
     * TODO: verificar si se usa, caso contrario quitar
     *
     * Chequea el estado del pago de un web_payment
     * @return type
     */
    public function actionCheckPayment() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                 
        if(!Yii::$app->request->isPost || !isset($_POST['data'])){
            return [
                'status' => 'error',
                'paid' => NULL,
                'message' => 'Error en los datos enviados'
            ];
        }
        

        $data = $_POST['data'];

        $web_payment = WebPayment::find()->where(['request_uuid' => $data])->one();

        if (!$web_payment) {
            return [
                'status' => 'error',
                'paid' => NULL,
            ];
        }
        
        $mp = \quoma\checkout\platforms\MercadoPago\MercadoPago::getMercadoPago($web_payment);

        if (!$mp) {
            return [
                'status' => 'error',
                'paid' => NULL,
                'message' => 'No existe la plataforma con nombre "Mercado Pago" para el sitio ' . $web_payment->site->server_name
            ];
        }

        try {
              //Buscamos el pago en Mercado Pago
            $mp_payment = MercadoPago::searchPayments($mp, ["external_reference" => $web_payment->uuid], 0, 10);
        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => 'No se pudo encontrar el Pago en Mercado Pago',
                'exception' => $exc->getTraceAsString()
            ];
        }
        

        if (isset($mp_payment['status'])) {
            if ($mp_payment['status'] == 'error') {
                return [
                    'status' => 'error',
                    'message' => $mp_payment['message'],
                ];
            }

            if ($mp_payment['status'] == 'warning') {
                return [
                    'status' => 'success',
                    'paid' => 0,
                ];
            }
        }
         
        //si hay mas de un pago asociado al reference
        if (count($mp_payment) > 1) {
            $paid = 0;
            $status = [];
            foreach ($mp_payment as $key => $payment) {
              
                if ($payment['status'] == 'approved') {
                   $paid = 1;
                }else{
                    $status[] = $payment['status'];
                }
            }
            
            if (!$paid) {
                return [
                    'status' => 'success',
                    'paid' => 0,
                    'mp_status' => $status
                ];
            }
            
        } 
       
        if ($mp_payment[0]['collection']['status'] != 'approved') {
            return [
                'status' => 'success',
                'paid' => 0,
                'mp_status' => $mp_payment[0]['collection']['status']
            ];
        }
        
        $web_payment->status = 'paid';
        if(!$web_payment->save(['status'])){
            $web_payment->notifyAdmin('Falló salvado de pago en Checkout',"El pago $web_payment->uuid está pagado en ".$web_payment->platform->name." pero no se pudo actualizar el estado en checkout. Que lo disfrutes!");
        }
        
        return [
                'status' => 'success',
                'paid' => 1,
        ];
        
    }


    /**
     * Finds the WebPayment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $web_payment_id
     * @param integer $site_id
     * @return WebPayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($web_payment_id) {
        if (($model = WebPayment::findOne(['web_payment_id' => $web_payment_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
