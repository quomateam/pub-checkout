<?php

namespace quoma\checkout\controllers;

use quoma\checkout\models\Platform;
use quoma\checkout\modules\siteConfig\models\SiteConfig;
use quoma\checkout\modules\siteConfig\models\SiteConfigCategory;
use quoma\checkout\modules\siteConfig\models\SiteConfigItem;
use quoma\modules\config\models\Config;
use Yii;
use quoma\checkout\models\Site;
use quoma\checkout\models\search\SiteSearch;
use quoma\core\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SiteController implements the CRUD actions for Site model.
 */
class SiteController extends Controller {


    /**
     * Lists all Site models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Site model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Site model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Site();

        if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post()['Site'];

            if ($model->validateRelations($post) && $model->save()) {

                return $this->redirect(['siteConfig/site-config/index', 'id' => $model->site_id]);
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /*
     * 
     */

    public function actionGetConfig() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $json = [];

        if ($post = \Yii::$app->request->post()) {

            if (isset($post['site_id']))
                $site = Site::findOne($post['site_id']);
            else
                $site = new Site();

            $platform = Platform::findOne($post['select']);

            $installments = \quoma\checkout\models\Installment::find()->all();

            $payment_methods = \quoma\checkout\models\PaymentMethod::find()->where(['platform_id' => $post['select']])->all();

            $payment_method_types = \quoma\checkout\models\PaymentMethodType::find()->all();

            $platform = Platform::findOne($post['select']);

            $json['status'] = 'success';
            $json['platform_id'] = $post['select'];
            $json['html'] = $this->renderPartial('partials/_view', [
                'model' => $site,
                'installments' => $installments,
                'payment_methods' => $payment_methods,
                'payment_method_types' => $payment_method_types,
                'platform' => $platform,
//                'platform_id' => $post['select'],
//                'site_has_platform' => $site_has_platform,
                'site_id' => (isset($post['site_id'])) ? $post['site_id'] : NULL,
            ]);

            return $json;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing Site model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post()['Site'];

            if ($model->validateRelations($post) && $model->save()) {

                return $this->redirect(['siteConfig/site-config/index', 'id' => $model->site_id]);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Site model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {

        $model = $this->findModel($id);
        $model->unlinkAll('installments', true);
        $model->unlinkAll('paymentMethods', true);

        $category = SiteConfigCategory::find()->where(['slug' => 'checkout-online', 'site_id' => $model->site_id])->one();

        $items = SiteConfigItem::getItems($category);

        foreach($items as $item){
            Site::getDb()->createCommand()->delete('site_config', [
                'site_config_item_id' => $item->site_config_item_id,
            ])->execute();

            Site::getDb()->createCommand()->delete('site_config_rule', [
                'site_config_item_id' => $item->site_config_item_id,
            ])->execute();

            $item->delete();
        }
        $category->delete();
        $model->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Site model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Site the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Site::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
