<?php

namespace quoma\checkout\controllers;

use quoma\checkout\CheckoutModule;
use Yii;
use yii\web\Controller;
use quoma\checkout\models\WebPayment;
use quoma\checkout\models\Site;
use quoma\checkout\components\PlatformFactory;
use quoma\checkout\models\SiteData;
use quoma\checkout\components\DataForm;
use quoma\checkout\platforms\MercadoPago\MercadoPago;
use quoma\modules\config\models\Config;
use quoma\checkout\assets\CheckoutAssets;
use yii\web\ForbiddenHttpException;


class DefaultController extends Controller {

    /**
     * @var quoma\checkout\models\Site
     */
    private $_site;
    
    public function init() {
        parent::init();
        //Agregado porq si no puedo enviarle post
        $this->enableCsrfValidation = false;
    }

    public function behaviors()
    {
        /**
         * Si en la configuracion no se especifican behaviors para este controlador, se conservan los
         * behaviors heredados
         */
        if (CheckoutModule::getInstance()->defaultControllerBehaviors === null){
            return parent::behaviors();
        }

        return CheckoutModule::getInstance()->defaultControllerBehaviors;
    }

    public function beforeAction($action) {

        parent::beforeAction($action);

        // $headers is an object of yii\web\HeaderCollection
        $headers = Yii::$app->request->headers;

        if(in_array($action->id ,['assets'])){
            return true;
        }

        //Valido el token enviado en el header
        if ($headers->has('token')) {
            $token = $headers->get('token');

            $site = Site::find()->where(
                "server_name='$token'"
            )->one();

            if (!empty($site)) {
                $this->_site = $site;

                return true;
            }
        }

        throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
    }

    /**
     * Inicializa un pago
     *
     * @return array
     */
    public function actionInitPayment() {

        \Yii::$app->response->format = 'json';

        $site = $this->_site;
        $site->updateAttributes(['image' => Yii::$app->request->post('picture_url')]);

        $payment = new \quoma\checkout\models\WebPayment();
        $payment->load(Yii::$app->request->post(), '');

        $payment->site_id = $site->site_id;
        $payment->request_uuid = Yii::$app->request->post('id');
        $payment->platform_id = $site->platform_id;

        if ($payment->save()) {
            //le damos el control a la plataforma
            $platform = $site->loadPlatform();

            return $platform->pay($payment);
        } else {
            return [
                'status' => 'error',
                'errors' => $payment->getErrors()
            ];
        }
    }

    /**
     * Utilizamos JSONP para cargar los assets necesarios. Esta acción arma el
     * json necesario. El callback utilizado es __Q.init.
     * 
     * Solo funciona con https
     */
    public function actionAssets()
    {
        //Solo https
        if(!YII_DEBUG && !Yii::$app->request->isSecureConnection){
            return;
        }
        
        $bundle = CheckoutAssets::register(Yii::$app->view);
        $bundle->registerAssetFiles(Yii::$app->view);
        
        $bundles = [];

        //Ordenamos los js de acuerdo al orden de dependencia
        foreach($this->getView()->assetBundles as $key => $b){
            foreach ($b->depends as $depends){
                if(!isset($bundles[$depends]) && isset($this->getView()->assetBundles[$depends])){
                    $bundles[$depends] = $this->getView()->assetBundles[$depends];
                }
            }
            if(!isset($bundles[$key]) && isset($this->getView()->assetBundles[$key])){
                $bundles[$key] = $this->getView()->assetBundles[$key];
            }
        }
        
        $data = [
            'css' => [],
            'js' => [],
        ];
        foreach($bundles as $key => $b){
            //JS
            foreach($b->js as $js){
                $data['js'][] = str_replace('http:','', Yii::$app->urlManager->hostInfo).$b->baseUrl.'/'.$js;
            }
            //CSS
            foreach($b->css as $css){
                $data['css'][] = str_replace('http:','', Yii::$app->urlManager->hostInfo).$b->baseUrl.'/'.$css;
            }
        }

        $data['baseUrl'] = \yii\helpers\Url::to(['/checkout'], '');
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSONP;
        
        return [
            'data' => $data,
            'callback' => '__Q.init'
        ];
    }

    /**
     * Chequea el estado del pago de un web_payment
     *
     * @deprecated
     */
    public function actionGetPaymentId() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if(!Yii::$app->request->isPost || !isset($_POST['data'])){
            return [
                'status' => 'error',
                'id' => NULL,
                'message' => 'Error en los datos enviados'
            ];
        }

        $data = $_POST['data'];

        $web_payment = WebPayment::find()->where(['request_uuid' => $data])->one();

        if (!$web_payment) {
            return [
                'status' => 'error',
                'id' => NULL,
            ];
        }

        $mp = \quoma\checkout\platforms\MercadoPago\MercadoPago::getMercadoPago($web_payment);

        if (!$mp) {
            return [
                'status' => 'error',
                'id' => NULL,
                'message' => 'No existe la plataforma con nombre "Mercado Pago" para el sitio ' . $web_payment->site->server_name
            ];
        }

        try {
            //Buscamos el pago en Mercado Pago
            $mp_payment = MercadoPago::searchPayments($mp, ["external_reference" => $web_payment->uuid], 0, 10);
        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => 'No se pudo encontrar el Pago en Mercado Pago',
                'exception' => $exc->getTraceAsString()
            ];
        }


        if (isset($mp_payment['status'])) {
            if ($mp_payment['status'] == 'error') {
                return [
                    'status' => 'error',
                    'message' => $mp_payment['message'],
                ];
            }

            if ($mp_payment['status'] == 'warning') {
                return [
                    'status' => 'success',
                    'id' =>  implode($mp_payment),
                ];
            }
        }

        //si hay mas de un pago asociado al reference
        if (count($mp_payment) > 1) {
            $id = NULL;
            foreach ($mp_payment as $key => $payment) {

                if ($payment['status'] == 'approved') {
                    $id = $payment['id'];
                }
            }
        }else{

            $id = $mp_payment[0]['collection']['id'];
        }


        return [
            'status' => 'success',
            'id' => $id,
        ];

    }
}
