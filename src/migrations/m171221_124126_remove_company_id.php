<?php

use yii\db\Migration;

class m171221_124126_remove_company_id extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->dropColumn("$db.site", 'company_id');

    }

    public function down()
    {
        echo "m171221_124126_remove_company_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
