<?php

use yii\db\Migration;

class m171212_130841_site_has_platform extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->addColumn("$db.site",'platform_id', $this->integer());
        $this->addForeignKey('fk_site_has_platform', "$db.site",'platform_id',"$db.platform",'platform_id');
        $this->addColumn("$db.site",'platform_timeout', $this->integer());

        $sites = \quoma\checkout\models\Site::find()->all();

        if($sites){
            foreach ($sites as $site){
                $query = new \yii\db\Query();
                $query->from("$db.site_has_platform")
                    ->where(['site_id' => $site->site_id])
                ->orderBy(['platform_id' => SORT_DESC]);
                $has_platform = $query->one();
                if($has_platform){
                    $site->updateAttributes(['platform_id' => $has_platform['platform_id'], 'platform_timeout' => $has_platform['time_out']]);
                }
            }
        }

        $this->dropTable("$db.site_has_platform");

    }

    public function down()
    {
        echo "m171212_130841_site_has_platform cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
