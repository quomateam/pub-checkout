<?php

use yii\db\Migration;

class m170215_210537_add_test_site extends Migration
{

    public function init()
    {
        $this->db = 'db_checkout';
        parent::init();
    }

    public function up()
    {
        if (YII_ENV_TEST) {
            $this->execute("INSERT INTO `site` (`site_id`, `name`, `server_name`, `status`, `company_id`, `image`, `admin_email`) VALUES
                (1, 'ADN Mujer', 'adn-mujer', 'enabled', 1, NULL, 'ing@marcelobriones.com.ar'),
                (2, 'Los Andes', 'los-andes', 'enabled', 1, NULL, 'ing@marcelobriones.com.ar');
                ");

            $this->execute("INSERT INTO `site_has_platform` (`site_id`, `platform_id`, `time_out`) VALUES
                (1, 1, NULL),
                (2, 1, NULL);
                ");

            $this->execute("INSERT INTO `site_has_installment` (`site_has_installment_id`, `site_id`, `installment_id`, `platform_id`) VALUES
               (1, 1, 1, 1),
               (2, 2, 1, 1);
               ");

            $this->execute("INSERT INTO `site_has_payment_method` (`site_has_payment_method_id`, `site_id`, `payment_method_id`, `platform_id`, `time_out`) VALUES
                (1, 1, 1, 1, NULL),
                (2, 1, 2, 1, NULL),
                (3, 2, 1, 1, NULL),
                (4, 2, 2, 1, NULL);
                ");
        }
    }

    public function down()
    {
        
    }

}
