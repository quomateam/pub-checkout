<?php

use yii\db\Migration;

class m171204_155030_change_field_web_payment extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        $this->renameColumn("$db.web_payment", 'payment_start_date', 'start_date');
        $this->renameColumn("$db.web_payment", 'payment_end_date', 'end_date');

    }

    public function down()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        $this->renameColumn("$db.web_payment", 'start_date', 'payment_start_date');
        $this->renameColumn("$db.web_payment", 'end_date', 'payment_end_date');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
