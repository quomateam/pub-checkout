<?php

use yii\db\Migration;

/**
 * Class m181201_184015_manual_platform
 */
class m181201_184015_manual_platform extends Migration
{
    public function init()
    {
        $this->db= 'db_checkout';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert("platform", [
            'main_class' => 'quoma\checkout\platforms\Manual\Manual',
            'status' => 'enabled',
            'name' => 'Manual'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('platform', ['name' => 'Manual']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181201_184015_manual_platform cannot be reverted.\n";

        return false;
    }
    */
}
