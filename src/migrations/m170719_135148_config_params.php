<?php

use yii\db\Migration;

class m170719_135148_config_params extends Migration {

    public function init() {
        $this->db = 'db_config';
        parent::init();
    }

    public function up() {
        $this->insert('category', [
            'name' => 'Ecommerce',
            'status' => 'enabled'
        ]);

        $categoryId = $this->db->getLastInsertID();

        $this->insert('item', [
            'attr' => 'checkout_admin_email',
            'type' => 'textInput',
            'label' => 'Email de administrador del modulo checkout para notificaciones de errores por email',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => 'admin@mail.com'
        ]);
        //
        $this->insert('item', [
            'attr' => 'timeout_confim_payment',
            'type' => 'textInput',
            'label' => 'Tiempo con que cuenta el cliente para continuar la operación en pantalla inicial del Módulo(Minutos)',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => 5
        ]);
        
       
        
        $this->insert('item', [
            'attr' => 'mercado_pago_client_id',
            'type' => 'textInput',
            'label' => 'Client ID de Mercado Pago(Opcional)',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => NULL
        ]);
        
        $this->insert('item', [
            'attr' => 'mercado_pago_secret',
            'type' => 'textInput',
            'label' => 'Secret de Mercado Pago(Opcional)',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => NULL
        ]);
        
        $this->insert('item', [
            'attr' => 'mercado_pago_sandbox',
            'type' => 'checkbox',
            'label' => '¿Activar Modo Sandbox para Mercado Pago?(Opcional)',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => 0
        ]);
        
        $this->insert('item', [
            'attr' => 'mercado_pago_back_url',
            'type' => 'textInput',
            'label' => 'Url del proyecto que será usado por el actionBack que retorna al usuario dentro del modulo checkout para Mercado Pago.(Opcional)',
            'description' => 'Este parámetro es necesario si la aplicación se encuentra en modo testing y se quiere crear un pago del tipo preapproval payment',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => 'http://localhost/arya2/web/'
        ]);
        
         $this->insert('item', [
            'attr' => 'only_secure_connection',
            'type' => 'checkbox',
            'label' => '¿Sólo se admitirán conexiones seguras?',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => 0
        ]);
        
        $this->insert('item', [
            'attr' => 'checkout_mode_testing',
            'type' => 'checkbox',
            'label' => 'Modo testing',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => 0
        ]);
        
        
    }

    public function down() {
        echo "m170719_135148_config_params cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
