<?php

use yii\db\Migration;

class m170905_184925_config_params_decidir_prisma extends Migration
{

    public function init() {
        $this->db = 'db_config';
        parent::init();
    }

    public function up()
    {

        $categoryId = \quoma\modules\config\models\Category::find()->where(['name' => 'Ecommerce'])->one()->category_id;

        $this->insert('item', [
            'attr' => 'public_key_decidir_prisma',
            'type' => 'textInput',
            'label' => 'Public Key de Decidir Prisma(Opcional)',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => NULL
        ]);

        $this->insert('item', [
            'attr' => 'private_key_decidir_prisma',
            'type' => 'textInput',
            'label' => 'Private Key de Decidir Prisma(Opcional)',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => NULL
        ]);

    }

    public function down()
    {
        echo "m170905_184925_config_params_decidir_prisma cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
