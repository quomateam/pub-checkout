<?php

use yii\db\Migration;

/**
 * Class m180530_142705_create_table_mp_customer
 */
class m180530_142705_create_table_mp_seller extends Migration
{

    public function init() {
        $this->db = 'db_checkout';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('mp_seller', [
            'mp_seller_id' => $this->primaryKey(),
            'model_id' => $this->integer(),
            'model_class' => $this->string(),
            'access_token' => $this->string(),
            'refresh_token' => $this->string(),
            'expiration_time' => $this->integer(),
            'app_id' => $this->string(),
            'site_id' => $this->integer()
        ]);

        $this->createIndex('mp_seller_model_id_indx','mp_seller','model_id');
        $this->addForeignKey('mp_seller_site', 'mp_seller','site_id','site','site_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('mp_seller');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180530_142705_create_table_mp_customer cannot be reverted.\n";

        return false;
    }
    */
}
