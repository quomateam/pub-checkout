<?php

use yii\db\Migration;

class m170724_134956_add_payment_token extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("ALTER TABLE `$db`.`web_payment` 
            ADD COLUMN `token` VARCHAR(50) NULL DEFAULT NULL");

    }

    public function down()
    {
        echo "m170724_134956_add_payment_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
