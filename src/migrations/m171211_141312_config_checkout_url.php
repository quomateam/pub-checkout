<?php

use yii\db\Migration;

class m171211_141312_config_checkout_url extends Migration
{
    public function init() {
        $this->db = 'db_config';
        parent::init();
    }

    public function up()
    {

        $category = \quoma\modules\config\models\Category::find()->where(['name' => 'Ecommerce'])->one();

        $this->insert('item', [
            'attr' => 'checkout_url',
            'type' => 'textInput',
            'label' => 'Url donde esta alojado el modulo checkout',
            'description' => '',
            'multiple' => 0,
            'category_id' => $category->category_id,
            'superadmin' => 1,
            'default' => 'http://site2.com/checkout_online'
        ]);


    }

    public function down()
    {
        echo "m171211_141312_config_checkout_url cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
