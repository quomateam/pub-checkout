<?php

use yii\db\Migration;

class m170103_140606_add_datetime_payment extends Migration
{
    public function up()
    {
        
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->execute("ALTER TABLE `$db`.`web_payment` 
        ADD COLUMN `create_date` DATE NULL DEFAULT NULL,
        ADD COLUMN `create_time` TIME NULL DEFAULT NULL AFTER `create_date`,
        ADD COLUMN `create_datetime` INT(11) NULL DEFAULT NULL AFTER `create_time`,
        ADD COLUMN `update_date` DATE NULL DEFAULT NULL AFTER `create_datetime`,
        ADD COLUMN `update_time` TIME NULL DEFAULT NULL AFTER `update_date`,
        ADD COLUMN `update_datetime` INT(11) NULL DEFAULT NULL AFTER `update_time`;");
    }

    public function down()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->dropColumn("$db.web_payment", "create_date");
        $this->dropColumn("$db.web_payment", "create_time");
        $this->dropColumn("$db.web_payment", "create_datetime");
        $this->dropColumn("$db.web_payment", "update_date");
        $this->dropColumn("$db.web_payment", "update_time");
        $this->dropColumn("$db.web_payment", "update_datetime");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
