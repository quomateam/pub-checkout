<?php

use yii\db\Migration;

class m170905_192459_web_payment_add_bin_param extends Migration
{
    public function safeUp()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->addColumn("`$db`.`web_payment`", 'bin', $this->string(6) );


    }

    public function down()
    {
        echo "m170905_192459_web_payment_add_bin_param cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
