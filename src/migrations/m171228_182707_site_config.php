<?php

use yii\db\Migration;

class m171228_182707_site_config extends Migration
{
    public function safeUp()
    {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->createTable("$db.site_config_category", [
            'site_config_category_id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'slug' => $this->string(45)->notNull(),
            'backend' => $this->boolean(),
            'site_id' => $this->integer()
        ]);
        
        $this->addForeignKey('fk_site_config_category_site_id', "$db.site_config_category", 'site_id', "$db.site", 'site_id');
        
        $this->createTable("$db.site_config_item", [
            'site_config_item_id' => $this->primaryKey(),
            'attr' => $this->string(45)->notNull(),
            'type' => $this->string(45),
            'default' => $this->string(255),
            'label' => $this->string(140),
            'description' => $this->string(255),
            'multiple' => $this->boolean(),
            'site_config_category_id' => $this->integer()->notNull(),
            'backend' => $this->boolean(),
            'site_id' => $this->integer()
        ]);
        
        $this->addForeignKey('fk_site_config_item_site_id', "$db.site_config_item", 'site_id', "$db.site", 'site_id');
        $this->addForeignKey('fk_site_config_category', "$db.site_config_item", 'site_config_category_id', "$db.site_config_category", 'site_config_category_id');
        
        $this->createTable("$db.site_config", [
            'site_config_id' => $this->primaryKey(),
            'value' => $this->text(),
            'site_config_item_id' => $this->integer()->notNull(),
        ]);
        
        $this->addForeignKey('fk_config_item_idx', "$db.site_config", 'site_config_item_id', "$db.site_config_item", 'site_config_item_id');
        
        $this->createTable("$db.site_config_rule", [
            'site_config_rule_id' => $this->primaryKey(),
            'message' => $this->string(255),
            'max' => $this->double(),
            'min' => $this->double(),
            'pattern' => $this->string(255),
            'format' => $this->string(45),
            'targetAttribute' => $this->string(45),
            'targetClass' => $this->string(255),
            'site_config_item_id' => $this->integer()->notNull(),
            'validator' => $this->string(45)->notNull()
        ]);
        
        $this->addForeignKey('fk_rule_item1_idx', "$db.site_config_rule", 'site_config_item_id', "$db.site_config_item", 'site_config_item_id');
    }

    public function safeDown()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->dropForeignKey('fk_rule_item1_idx', "$db.site_config_rule");
        $this->dropTable("$db.site_config_rule");
        $this->dropForeignKey('fk_config_item_idx', "$db.site_config");
        $this->dropTable("$db.site_config");
        $this->dropForeignKey('fk_site_config_category', "$db.site_config_item");
        $this->dropTable("$db.site_config_item");
        $this->dropTable("$db.site_config_category");
        
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
