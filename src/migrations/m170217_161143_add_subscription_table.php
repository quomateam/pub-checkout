<?php

use yii\db\Migration;

class m170217_161143_add_subscription_table extends Migration
{
    public function up()
    {
        
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("CREATE TABLE IF NOT EXISTS `$db`.`subscription` (
            `subscription_id` INT(11) NOT NULL AUTO_INCREMENT,
            `external_id` VARCHAR(100) NULL DEFAULT NULL,
            `payer_id` VARCHAR(45) NULL DEFAULT NULL,
            `status` VARCHAR(45) NULL DEFAULT NULL,
            `reason` VARCHAR(45) NULL DEFAULT NULL,
            `reference` VARCHAR(45) NULL DEFAULT NULL,
            `date_created` VARCHAR(45) NULL DEFAULT NULL,
            `last_modified` VARCHAR(45) NULL DEFAULT NULL,
            `frequency` VARCHAR(45) NULL DEFAULT NULL,
            `frequency_type` VARCHAR(45) NULL DEFAULT NULL,
            `transaction_amount` FLOAT(11) NULL DEFAULT NULL,
            `currency_id` VARCHAR(5) NULL DEFAULT NULL,
            `start_date` VARCHAR(45) NULL DEFAULT NULL,
            `end_date` VARCHAR(45) NULL DEFAULT NULL,
            PRIMARY KEY (`subscription_id`))
          ENGINE = InnoDB
          DEFAULT CHARACTER SET = utf8
          ");
        
        $this->execute("ALTER TABLE `$db`.`web_payment` 
        ADD COLUMN `subscription_id` INT(11) NULL DEFAULT NULL,
        ADD INDEX `fk_web_payment_subscription1_idx` (`subscription_id` ASC)");
        
        $this->execute("ALTER TABLE `$db`.`web_payment` 
          ADD CONSTRAINT `fk_web_payment_subscription1`
          FOREIGN KEY (`subscription_id`)
          REFERENCES `$db`.`subscription` (`subscription_id`)
          ON DELETE NO ACTION");

    }

    public function down()
    {
        echo "m170217_161143_add_subscription_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
