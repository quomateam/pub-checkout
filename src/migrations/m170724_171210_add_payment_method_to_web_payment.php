<?php

use yii\db\Migration;

class m170724_171210_add_payment_method_to_web_payment extends Migration
{
    public function safeUp()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("ALTER TABLE `$db`.`web_payment` 
            ADD COLUMN `payment_method_id` INT(11) NULL DEFAULT NULL");
        
        $this->execute("ALTER TABLE `$db`.`web_payment` ADD INDEX `fk_web_payment_payment_method1_idx` (`payment_method_id` ASC)");
        
        $this->execute("ALTER TABLE `$db`.`web_payment` 
            ADD CONSTRAINT `fk_web_payment_payment_method1`
            FOREIGN KEY (`payment_method_id`)
            REFERENCES `$db`.`payment_method` (`payment_method_id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION");
    }

    public function down()
    {
        echo "m170724_171210_add_payment_method_to_web_payment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
