<?php

use yii\db\Migration;
use quoma\checkout\models\Platform;

class m161214_000003_checkout_add_mercado_pago_payment_methods extends Migration {

    public function up() {
        
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $platform = Platform::find()->where(['name' => 'Mercado Pago'])->one();

        if (!$platform) {

            $this->insert("$db.platform", [
                'main_class' => 'quoma/checkout/MercadoPago/MercadoPago', 
                'status' => 'enabled',
                'name' => 'Mercado Pago'
                ]);
            
            $platform = Platform::find()->where(['name' => 'Mercado Pago'])->one();
        }

        $mp = new \MP("TEST-3105433861271210-091909-46aa12b4e74b556622dd8a1cac04463c__LA_LD__-187418877");

        $payment_methods = $mp->get("/v1/payment_methods");

        if ($payment_methods['status'] == '200') {
            foreach ($payment_methods['response'] as $key => $method) {

                $this->insert("$db.payment_method", [
                    'name' => $method['name'],
                    'code' => $method['id'],
                    'payment_method_type' => $method['payment_type_id'],
                    'platform_id' => $platform->platform_id,
                    'accreditation_time' => $method['accreditation_time']
                ]);
            }
        }
    }

    public function down() {
        echo "m161213_193013_checkout_add_mercado_pago_payment_methods cannot be reverted.\n";

        return true;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
