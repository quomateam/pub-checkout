<?php

use yii\db\Migration;

class m171026_202817_add_platform_payment_id extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->addColumn("$db.web_payment",'platform_payment_id', $this->string(255));
        $this->addColumn("$db.web_receipt",'platform_payment_id', $this->string(255));

    }

    public function down()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->dropColumn("$db.web_payment",'platform_payment_id');
        $this->dropColumn("$db.web_receipt",'platform_payment_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
