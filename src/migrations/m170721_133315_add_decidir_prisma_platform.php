<?php

use yii\db\Migration;

class m170721_133315_add_decidir_prisma_platform extends Migration
{
    public function safeUp()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->insert("$db.platform", ['main_class' => 'quoma\checkout\platforms\DecidirPrisma\DecidirPrisma', 'status' => 'enabled', 'name' => 'Decidir Prisma']);
        
        $platform_id = $this->db->getLastInsertID();
        
        $credit_card_type = quoma\checkout\models\PaymentMethodType::find()->where(['key' => 'credit_card'])->one();
        
        //TARJETAS DE CRÉDITO
        $this->insert("$db.payment_method", [
            'name' => 'Visa',
            'code' => 1,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'MasterCard',
            'code' => 15,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'American Express',
            'code' => 65,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'Diners Club',
            'code' => 8,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
      
        
        $this->insert("$db.payment_method", [
            'name' => 'Cabal',
            'code' => 27,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'Tarjeta Shopping',
            'code' => 23,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'Tarjeta Naranja',
            'code' => 24,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        $this->insert("$db.payment_method", [
            'name' => 'Tarjeta Nevada',
            'code' => 39,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        $this->insert("$db.payment_method", [
            'name' => 'Nativa',
            'code' => 42,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        $this->insert("$db.payment_method", [
            'name' => 'Tarjeta Más',
            'code' => 43,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'Tarjeta Carrefour / Cetelem',
            'code' => 44,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'Tarjeta Club Día',
            'code' => 56,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'Tarjeta La Anónima',
            'code' => 61,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'ArgenCard',
            'code' => 30,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        $this->insert("$db.payment_method", [
            'name' => 'Credimás',
            'code' => 38,
            'payment_method_type_id' => $credit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
        
        //TARJETAS DE DÉBITO
        
        $debit_card_type = quoma\checkout\models\PaymentMethodType::find()->where(['key' => 'debit_card'])->one();
        
        $this->insert("$db.payment_method", [
            'name' => 'Visa Débito',
            'code' => 31,
            'payment_method_type_id' => $debit_card_type->payment_method_type_id,
            'platform_id' => $platform_id,
            'accreditation_time' => NULL
        ]);
    }

    public function down()
    {
        echo "m170721_133315_add_decidir_prisma_platform cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
