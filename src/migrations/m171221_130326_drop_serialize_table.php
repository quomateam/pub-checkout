<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `serialize`.
 */
class m171221_130326_drop_serialize_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        $this->dropTable("$db.serialize");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('serialize', [
            'serialize_id' => $this->primaryKey(),
            'payment_id' => $this->integer(),
            'data' => $this->text()
        ]);
    }
}
