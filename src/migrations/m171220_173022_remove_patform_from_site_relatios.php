<?php

use yii\db\Migration;

class m171220_173022_remove_patform_from_site_relatios extends Migration
{
    public function up()
    {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->dropForeignKey('fk_site_has_installment_platform1',"$db.site_has_installment");
        $this->dropColumn("$db.site_has_installment", 'platform_id');
        $this->dropForeignKey('fk_site_has_payment_method_platform1',"$db.site_has_payment_method");
        $this->dropColumn("$db.site_has_payment_method", 'platform_id');

    }

    public function down()
    {
        echo "m171220_173022_remove_patform_from_site_relatios cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
