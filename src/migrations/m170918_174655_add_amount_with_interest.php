<?php

use yii\db\Migration;

class m170918_174655_add_amount_with_interest extends Migration
{
    public function up()
    {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->addColumn("$db.web_payment", 'interest', $this->double(2));
        $this->addColumn("$db.web_payment", 'interest_amount', $this->double(2));
        $this->addColumn("$db.web_payment", 'gross_amount', $this->double(2));
        $this->addColumn("$db.web_payment", 'net_amount', $this->double(2));

        $this->alterColumn("$db.web_payment", 'amount', $this->double(2));

    }

    public function down()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->dropColumn("$db.web_payment", 'interest');
        $this->dropColumn("$db.web_payment", 'interest_amount');
        $this->dropColumn("$db.web_payment", 'gross_amount');
        $this->dropColumn("$db.web_payment", 'net_amount');

        $this->alterColumn("$db.web_payment", 'amount', $this->float());

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
