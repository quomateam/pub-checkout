<?php

use yii\db\Migration;

/**
 * Class m180605_173609_add_mercado_pago_payment_data
 */
class m180605_173609_add_mercado_pago_payment_data extends Migration
{

    public function init() {
        $this->db = 'db_checkout';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('web_payment', 'net_received_amount', $this->double());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180605_173609_add_mercado_pago_payment_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180605_173609_add_mercado_pago_payment_data cannot be reverted.\n";

        return false;
    }
    */
}
