<?php

use yii\db\Migration;
use \quoma\modules\config\models\Item;

/**
 * Class m171228_174957_remove_deprecated_config_items
 */
class m171228_174957_remove_deprecated_config_items extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $db_config = \quoma\core\helpers\DbHelper::getDbName('db_config');

        $attrs = Item::find()->where(['in','attr' , [
            'has_interest', 'mercado_pago_client_id','mercado_pago_secret','mercado_pago_sandbox','mercado_pago_back_url','only_secure_connection',
            'checkout_url','company_enabled','public_key_decidir_prisma','private_key_decidir_prisma']])->all();

        if($attrs){
            foreach ($attrs as $attr){
                $this->delete("$db_config.config" , ['item_id' => $attr->item_id]);
                $this->delete("$db_config.item",['item_id' => $attr->item_id]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->addColumn("$db.site_has_installment", 'interest', $this->float());

        $this->update("$db.site_has_installment", ['interest' => 0]);

        $categoryId = \quoma\modules\config\models\Category::find()->where(['name' => 'Ecommerce'])->one()->category_id;

        $db_config = \quoma\core\helpers\DbHelper::getDbName('db_config');

        $this->insert("$db_config.item", [
            'attr' => 'has_interest',
            'type' => 'checkbox',
            'label' => '¿El sitio tendrá interés por pagos en cuotas?',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => 0
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171228_174957_remove_deprecated_config_items cannot be reverted.\n";

        return false;
    }
    */
}
