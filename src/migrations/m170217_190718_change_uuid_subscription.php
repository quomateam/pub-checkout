<?php

use yii\db\Migration;

class m170217_190718_change_uuid_subscription extends Migration
{
    public function up()
    {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("ALTER TABLE `$db`.`subscription` 
        CHANGE COLUMN `uuid` `uuid` VARCHAR(100) NULL DEFAULT NULL ");
    }

    public function down()
    {
        echo "m170217_190718_change_uuid_subscription cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
