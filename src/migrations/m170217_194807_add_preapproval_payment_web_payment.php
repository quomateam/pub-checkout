<?php

use yii\db\Migration;

class m170217_194807_add_preapproval_payment_web_payment extends Migration {

    public function up() {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->execute("ALTER TABLE `$db`.`web_payment` 
        ADD COLUMN `type` VARCHAR(45) NULL DEFAULT NULL,
        ADD COLUMN `preapproval_payment` INT(11) NULL DEFAULT 0 AFTER `type`");
        

    }

    public function down() {
        echo "m170217_194807_add_preapproval_payment_web_payment cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
