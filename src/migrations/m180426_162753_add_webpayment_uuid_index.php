<?php

use yii\db\Migration;

/**
 * Class m180426_162753_add_cart_uuid
 */
class m180426_162753_add_webpayment_uuid_index extends Migration
{

    public function init() {
        $this->db = 'db_checkout';
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('webpayment_uuid_idx','web_payment','uuid');
        $this->createIndex('webpayment_request_uuid_idx','web_payment','request_uuid');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180426_162753_add_cart_uuid cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180426_162753_add_cart_uuid cannot be reverted.\n";

        return false;
    }
    */
}
