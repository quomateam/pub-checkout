<?php

use yii\db\Migration;

/**
 * Class m180531_150755_add_market_place_attrs_in_web_payment
 */
class m180531_150755_add_market_place_attrs_in_web_payment extends Migration
{
    public function init() {
        $this->db = 'db_checkout';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('web_payment', 'is_market_place', $this->boolean()->defaultValue(false));
        $this->addColumn('web_payment', 'marketplace_fee', $this->double());
        $this->addColumn('web_payment', 'mp_seller_id', $this->integer()->null());

        $this->addForeignKey('fk_web_payment_mp_seller','web_payment', 'mp_seller_id','mp_seller','mp_seller_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180531_150755_add_market_place_attrs_in_web_payment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180531_150755_add_market_place_attrs_in_web_payment cannot be reverted.\n";

        return false;
    }
    */
}
