<?php

use yii\db\Migration;

/**
 * Class m181009_214642_cybersource_data
 */
class m181009_214642_cybersource_data extends Migration
{

    public function init(){
        $this->db= 'db_checkout';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            "CREATE TABLE IF NOT EXISTS `cybersource_data` (
                  `cybersource_data_id` INT NOT NULL AUTO_INCREMENT,
                  `first_name` VARCHAR(255) NOT NULL,
                  `last_name` VARCHAR(255) NOT NULL,
                  `customer_id` VARCHAR(45) NOT NULL,
                  `address` VARCHAR(255) NOT NULL,
                  `email` VARCHAR(55) NOT NULL,
                  `city` VARCHAR(50) NOT NULL,
                  `postal_code` VARCHAR(45) NOT NULL,
                  `state` VARCHAR(45) NOT NULL,
                  `currency` VARCHAR(45) NOT NULL,
                  `amount` DOUBLE NOT NULL,
                  `days_in_site` INT NULL,
                  `is_guest` TINYINT(1) NULL,
                  `transactions` INT NULL,
                  `web_payment_id` INT(11) NOT NULL,
                  `site_id` INT(11) NOT NULL,
                  PRIMARY KEY (`cybersource_data_id`),
                  INDEX `fk_cybersource_data_web_payment1_idx` (`web_payment_id` ASC, `site_id` ASC),
                  CONSTRAINT `fk_cybersource_data_web_payment1`
                    FOREIGN KEY (`web_payment_id` , `site_id`)
                    REFERENCES `web_payment` (`web_payment_id` , `site_id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB");

        $this->execute(
            "CREATE TABLE IF NOT EXISTS `cybersource_item_data` (
                  `cybersource_item_data_id` INT NOT NULL AUTO_INCREMENT,
                  `code` VARCHAR(45) NOT NULL,
                  `name` VARCHAR(255) NOT NULL,
                  `description` TEXT NOT NULL,
                  `sku` VARCHAR(45) NOT NULL,
                  `total_amount` DOUBLE NOT NULL,
                  `qty` INT NOT NULL,
                  `unit_price` DOUBLE NOT NULL,
                  `cybersource_data_id` INT NOT NULL,
                  PRIMARY KEY (`cybersource_item_data_id`),
                  INDEX `fk_cybersource_item_data_cybersource_data1_idx` (`cybersource_data_id` ASC),
                  CONSTRAINT `fk_cybersource_item_data_cybersource_data1`
                    FOREIGN KEY (`cybersource_data_id`)
                    REFERENCES `cybersource_data` (`cybersource_data_id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cibersource_item_data');
        $this->dropTable('cibersource_data');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181009_214642_cybersource_data cannot be reverted.\n";

        return false;
    }
    */
}
