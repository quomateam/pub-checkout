<?php

use yii\db\Migration;

/**
 * Class m180528_173430_drop_checkout_extras_databases
 */
class m180528_173430_drop_deprecated_extras_databases extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        try {
            $this->execute("DROP DATABASE " . \quoma\core\helpers\DbHelper::getDbName('db_checkout_payment'));
        } catch (Exception $e) {

        }

        try {
            $this->execute("DROP DATABASE " . \quoma\core\helpers\DbHelper::getDbName('db_checkout_site_access'));
        } catch (Exception $e) {

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180528_173430_drop_checkout_extras_databases cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180528_173430_drop_checkout_extras_databases cannot be reverted.\n";

        return false;
    }
    */
}
