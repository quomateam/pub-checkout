<?php

use yii\db\Migration;

class m170915_192345_add_interest_to_installments extends Migration
{
    public function up()
    {

        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->addColumn("$db.site_has_installment", 'interest', $this->float());

        $this->update("$db.site_has_installment", ['interest' => 0]);

        $categoryId = \quoma\modules\config\models\Category::find()->where(['name' => 'Ecommerce'])->one()->category_id;

        $db_config = \quoma\core\helpers\DbHelper::getDbName('db_config');

        $this->insert("$db_config.item", [
            'attr' => 'has_interest',
            'type' => 'checkbox',
            'label' => '¿El sitio tendrá interés por pagos en cuotas?',
            'description' => '',
            'multiple' => 0,
            'category_id' => $categoryId,
            'superadmin' => 1,
            'default' => 0
        ]);

    }

    public function down()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->dropColumn("$db.site_has_installment",'interest');

        $this->dropColumn("$db.item",'has_interest');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
