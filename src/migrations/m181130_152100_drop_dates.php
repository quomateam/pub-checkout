<?php

use yii\db\Migration;

/**
 * Class m181130_152100_drop_dates
 */
class m181130_152100_drop_dates extends Migration
{

    public function init()
    {
        $this->db= 'db_checkout';
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('web_payment', 'create_date');
        $this->dropColumn('web_payment', 'create_time');
        $this->dropColumn('web_payment', 'update_date');
        $this->dropColumn('web_payment', 'update_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181130_152100_drop_dates cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181130_152100_drop_dates cannot be reverted.\n";

        return false;
    }
    */
}
