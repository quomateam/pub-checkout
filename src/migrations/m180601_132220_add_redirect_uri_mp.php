<?php

use yii\db\Migration;

/**
 * Class m180601_132220_add_redirect_uri_mp
 */
class m180601_132220_add_redirect_uri_mp extends Migration
{
    public function init() {
        $this->db = 'db_checkout';
        parent::init();
    }


    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('mp_seller', 'redirect_uri', $this->string());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180601_132220_add_redirect_uri_mp cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180601_132220_add_redirect_uri_mp cannot be reverted.\n";

        return false;
    }
    */
}
