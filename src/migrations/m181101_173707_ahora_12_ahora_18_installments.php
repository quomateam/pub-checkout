<?php

use yii\db\Migration;

/**
 * Class m181101_173707_ahora_12_ahora_18_installments
 */
class m181101_173707_ahora_12_ahora_18_installments extends Migration
{
    public function init(){
        $this->db= 'db_checkout';
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('installment', 'label', 'VARCHAR(45) NULL');

        $installments= $this->db->createCommand('SELECT * FROM installment')->queryAll();

        foreach ($installments as $installment){
            $this->update('installment', ['label' => $installment['key'] . ' Cuotas'], ['installment_id' => $installment['installment_id']]);
        }

        $this->insert('installment', [
            'key' => 12,
            'qty' => 12,
            'label' => 'Ahora 12'
        ]);

        $this->insert('installment', [
            'key' => 18,
            'qty' => 18,
            'label' => 'Ahora 18',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('installment', ['key' => 'Ahora 12']);
        $this->delete('installment', ['key' => 'Ahora 18']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181101_173707_ahora_12_ahora_18_installments cannot be reverted.\n";

        return false;
    }
    */
}
