<?php

use yii\db\Migration;

class m171212_134603_config_company_enabled extends Migration
{
    public function init() {
        $this->db = 'db_config';
        parent::init();
    }

    public function up()
    {

        $category = \quoma\modules\config\models\Category::find()->where(['name' => 'Ecommerce'])->one();

        $this->insert('item', [
            'attr' => 'company_enabled',
            'type' => 'checkbox',
            'label' => ' ¿Tendrá una compañía por defecto?',
            'description' => '',
            'multiple' => 0,
            'category_id' => $category->category_id,
            'superadmin' => 1,
            'default' => false
        ]);


    }
    public function down()
    {
        echo "m171212_134603_config_company_enabled cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
