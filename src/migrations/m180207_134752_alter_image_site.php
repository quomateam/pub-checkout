<?php

use yii\db\Migration;

/**
 * Class m180207_134752_alter_image_site
 */
class m180207_134752_alter_image_site extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $db_checkout = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->alterColumn("$db_checkout.site", 'image', $this->string(1000));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $db_checkout = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $this->alterColumn("$db_checkout.site", 'image', $this->string(255));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180207_134752_alter_image_site cannot be reverted.\n";

        return false;
    }
    */
}
