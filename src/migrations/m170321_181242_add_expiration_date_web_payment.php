<?php

use yii\db\Migration;

class m170321_181242_add_expiration_date_web_payment extends Migration
{
    public function up()
    {
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
        
        $this->execute("ALTER TABLE `$db`.`web_payment` 
            ADD COLUMN `expires` TINYINT(1) NULL DEFAULT 0,
            ADD COLUMN `expiration_date_from` VARCHAR(45) NULL DEFAULT NULL AFTER `expires`,
            ADD COLUMN `expiration_date_to` VARCHAR(45) NULL DEFAULT NULL AFTER `expiration_date_from`");

    }

    public function down()
    {
      $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');
      
      $this->dropColumn("$db.web_payment", 'expires');
      $this->dropColumn("$db.web_payment", 'expiration_date_from');
      $this->dropColumn("$db.web_payment", 'expiration_date_to');
      
      return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
