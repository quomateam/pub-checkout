<?php

use yii\db\Migration;

/**
 * Class m181011_182345_phone_and_birthday_cybersource_data
 */
class m181011_182345_phone_and_birthday_cybersource_data extends Migration
{
    public function init()
    {
        $this->db= 'db_checkout';
        parent::init(); // TODO: Change the autogenerated stub
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cybersource_data', 'phone', 'VARCHAR(45) NULL');
        $this->addColumn('cybersource_data', 'birthday', 'INT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('cybersource_data', 'phone');
        $this->dropColumn('cybersource_data', 'birthday');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181011_182345_phone_and_birthday_cybersource_data cannot be reverted.\n";

        return false;
    }
    */
}
