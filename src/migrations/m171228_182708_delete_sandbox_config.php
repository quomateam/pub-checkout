<?php

use yii\db\Migration;

/**
 * Class m171228_182708_delete_sandbox_config
 */
class m171228_182708_delete_sandbox_config extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $db_checkout = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $attrs = \quoma\checkout\modules\siteConfig\models\SiteConfigItem::find()->where(['in','attr' , [
            'checkout_mode_testing']])->all();

        if($attrs){
            foreach ($attrs as $attr){
                $this->delete("$db_checkout.site_config" , ['site_config_item_id' => $attr->site_config_item_id]);
                $this->delete("$db_checkout.site_config_item",['site_config_item_id' => $attr->site_config_item_id]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171228_182708_delete_sandbox_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171228_182708_delete_sandbox_config cannot be reverted.\n";

        return false;
    }
    */
}
