<?php

namespace quoma\checkout\commands;

use yii\console\Controller;
use quoma\checkout\models\PaymentMethod;
use quoma\checkout\models\Platform;
use quoma\checkout\platforms\MercadoPago\MercadoPago;
use quoma\checkout\models\Site;

/**
 * Description of MercadoPagoController
 *
 * @author Gabriela
 */
class MercadoPagoController extends Controller {

    public function actionImportPaymentMethods() {

        $platform = Platform::find()->where(['name' => 'Mercado Pago'])->one();

        if (!$platform) {
            echo 'No existe la plataforma con nombre "Mercado Pago"';
            return false;
        }

        $mp = new \MP("TEST-3105433861271210-091909-46aa12b4e74b556622dd8a1cac04463c__LA_LD__-187418877");
        $db = \quoma\core\helpers\DbHelper::getDbName('db_checkout');

        $payment_methods = $mp->get("/v1/payment_methods");

        if ($payment_methods['status'] == '200') {
            foreach ($payment_methods['response'] as $key => $method) {

                $m = PaymentMethod::findOne(['code' => $method['id'], 'platform_id' => $platform->platform_id]);
                
                $payment_method_type = \quoma\checkout\models\PaymentMethodType::findOne(['key' => $method['payment_type_id']]);
               
                if(!$payment_method_type){
                    \Yii::$app->db->createCommand()->insert("$db.payment_method_type", [
                        'name' => $method['payment_type_id'],
                        'key' => $method['payment_type_id'],
                    ])
                    ->execute();
                }
                
                if ($m) {
                    \Yii::$app->db->createCommand()->update("$db.payment_method", [
                                'name' => $method['name'],
                                'code' => $method['id'],
                                'payment_method_type_id' => $payment_method_type->payment_method_type_id,
                                'accreditation_time' => $method['accreditation_time']
                                    ], "payment_method_id=$m->payment_method_id")
                            ->execute();
                } else {
                    \Yii::$app->db->createCommand()->insert("$db.payment_method", [
                                'name' => $method['name'],
                                'code' => $method['id'],
                                'payment_method_type_id' => $payment_method_type->payment_method_type_id,
                                'platform_id' => $platform->platform_id,
                                'accreditation_time' => $method['accreditation_time']
                            ])
                            ->execute();
                }
            }
        }
    }


}
