<?php

namespace quoma\checkout\platforms\Decidir;

class DecidirModule extends \yii\base\Module
{
    public $controllerNamespace = 'quoma\checkout\platforms\Decidir\controllers';

    public function init()
    {
        parent::init();

        \Yii::setAlias('@plarform', __DIR__);
    }
}
