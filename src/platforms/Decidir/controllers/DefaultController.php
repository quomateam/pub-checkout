<?php

namespace quoma\checkout\platforms\Decidir\controllers;

use quoma\checkout\modules\siteConfig\models\SiteConfig;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use quoma\checkout\platforms\Decidir\Decidir;
use quoma\checkout\models\WebPayment;
use quoma\modules\config\models\Config;

class DefaultController extends Controller {

    public function init() {
        parent::init();
//Agregado porq si no puedo enviarle post
        $this->enableCsrfValidation = false;
    }

    public function actionInit($payment) {

        $this->layout = '//main_no_menu';

        $decidir = new Decidir();

        $payment = WebPayment::find()->where(['uuid' => $payment])->one();

        if (!$payment)
            throw new \yii\web\HttpException(400, Yii::t('yii', 'Your request is invalid.'));

        //chequeo si no ha superado el tiempo disponible para la operación
        $isOnTimeout = $payment->isOnTimeout();

        if ($isOnTimeout) {
            return $this->render('_error', [
                        'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                        'site' => $payment->site->name,
                        'operation' => $payment->uuid
            ]);
        }

        if (YII_ENV == 'test' or SiteConfig::get('checkout_mode_testing', $payment->site_id)) {
            $post_url = Url::to(['/test/mock-decidir']);
        } else {
            $post_url = 'https://sps.decidir.com/sps-ar/Validar';
        }

        $site = \quoma\checkout\models\Site::findOne($payment->site_id);

        $payments_methods = $site->paymentMethods;
        $installments = $site->installments;

        //TODO: traer el nro de comercio desde la siteConfig
//        $data = \quoma\checkout\models\Serialize::getDataByPayment($payment->web_payment_id);

        $decidir->config['NROCOMERCIO'] = $data['commerce_number'];


        return $this->render('init', [
                    'decidir' => $decidir,
                    'payment' => $payment,
                    'post_url' => $post_url,
                    'payments_methods' => $payments_methods,
                    'installments' => $installments,
                    'time_out' => $site->platform_timeout ,
        ]);
    }

    /**
     * Chequea el tiempo que el cliente se demoró en la vista de cobros para no enviar pagos que puedn superar el tiempo máximo de operación
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCheckTime() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $json = [];

        if ($post = \Yii::$app->request->post()) {

            $web_payment = WebPayment::findOne($post['payment_id']);

            if (!$web_payment) {
                $json['status'] = 'error';
                $json['message'] = 'El pago no existe!';
                return $json;
            }

            //chequeo si no ha superado el tiempo disponible para la operación
            $isOnTimeout = $web_payment->isOnTimeout();

            if ($isOnTimeout) {

                $this->notifyPaymentTimeOut($web_payment);
                $json['status'] = 'error';
                $json['html'] = $this->renderPartial('_error', [
                    'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                    'site' => $web_payment->site->name,
                    'operation' => $web_payment->uuid
                ]);
                return $json;
            }

            $json['status'] = 'success';
            $json['message'] = 'OK';


            return $json;
        } else {
            throw new \yii\web\HttpException(400, 'The requested page does not exist.');
        }
    }

    private function NotifyPaymentTimeOut($payment) {

        $payment->changeStatus('timeout');

        if (isset($payment->request_uuid))
            $nro_operation = $payment->request_uuid;
        else
            throw new \yii\web\HttpException(400, \Yii::t('yii', 'Your request is invalid.'));

        $status = $payment->status;

        if(in_array($status, ['canceled', 'timeout'])){
            $status = 'error';
        }

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $url = str_replace('www.', '', $payment->result_url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $nro_operation, 'status' => $status]);

        if (SiteConfig::get('checkout_mode_testing', $payment->site_id)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        
        //execute post
        $result = curl_exec($ch);

        if ($result === false) {
            echo "Error: " . curl_error($ch);
            echo "\n";
        }
        
        //close connection
        curl_close($ch);

//        \Yii::$app->response->format = 'json';
        return [
            'status' => 'ok',
            'message' => '',
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionResult() {

        if (!\Yii::$app->request->isPost) {
            throw new \yii\web\HttpException(400, \Yii::t('yii', 'Your request is invalid.'));
        }

        $model = new \quoma\checkout\platforms\Decidir\ResultModel();
        $model->setAttributes($_POST);

        $status = 'none';

        //Estado
        if ($model->resultado == 'APROBADA') {
            $status = 'approved';
        } elseif ($model->resultado == 'RECHAZADA') {
            $status = 'unapproved';
        }

        $response = [
            'status' => $status,
            'amount' => (float) (str_replace(',', '.', $model->monto)),
            'currency' => $model->moneda,
            'autorization_code' => $model->codautorizacion,
            'installments' => $model->cuotas,
            'owner' => $model->titular,
            'card' => $model->tarjeta,
            'email' => $model->emailcomprador,
            'datetime' => date('Y-m-d H:i:s', strtotime($model->fechahora)),
            'payment' => $model->noperacion,
            'messages' => ''
        ];

        //TODO: el negocio no debe conocer al controlador
        $payment = $this->loadPayment($model->noperacion);

        $payment->receipt($response);

        if (isset($payment->request_uuid))
            $nro_operation = $payment->request_uuid;
        else
            throw new \yii\web\HttpException(400, \Yii::t('yii', 'Your request is invalid.'));

        $status = $payment->status;
        
        if(in_array($status, ['canceled', 'timeout'])){
            $status = 'error';
        }
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $url = str_replace('www.', '', $payment->result_url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $nro_operation, 'status' => $status, 'response' => serialize($response)]);

        if (SiteConfig::get('checkout_mode_testing', $payment->site_id)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        
        //execute post
        $result = curl_exec($ch);

         if ($result === false) {
            echo "Error: " . curl_error($ch);
            echo "\n";
        }
        
        //close connection
        curl_close($ch);

        \Yii::$app->response->format = 'json';
        return [
            'status' => 'ok',
            'message' => '',
        ];
    }

    /**
     * Si encuentra el pago asociado al uuid, lo devuelve. Caso contrario
     * dispara una excepcion.
     * @param string $uuid
     * @return WebPayment
     * @throws CHttpException
     */
    public function loadPayment($uuid) {

        $model = WebPayment::find()->where(['uuid' => $uuid])->one();

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
        return $model;
    }

    public function actionBack($operation) {

        if (isset($_GET['operation'])) {
            $operation = $_GET['operation'];

            //Buscamos el pago asociado
            $payment = WebPayment::find()->where(['uuid' => $operation])->one();

            if ($payment) {

                if (isset($payment->request_uuid))
                    $operation = $payment->request_uuid;
                else
                    throw new \yii\web\HttpException(500, 'The operation does not exist.');

                $status = $payment->status;

                if(in_array($status, ['canceled', 'timeout'])){
                    $status = 'error';
                }
        
                //redirecciono 
                $return_url = $payment->return_url;
                if (strpos($return_url, '?') === false)
                    $url = "$return_url?data=$operation&status=$status";
                else
                    $url = "$return_url&data=$operation&status=$status";

                return Yii::$app->response->redirect($url);
            }else {
                throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
            }
        }
    }

}
