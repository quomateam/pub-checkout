<?php

return [
    'categories' => [
        ['name' => 'General', 'slug' => 'general'],
        ['name' => 'Ecommerce', 'slug' => 'ecommerce'],
        ['name' => 'Gestión de Stock', 'slug' => 'stock_management'],
    ],
    'items' => [
        [
            'attr' => 'checkout_mode_testing',
            'label' => 'Modo Testing',
            'category' => 'checkout-online',
            'type' => 'checkbox',
            'default' => false,
            'backend' => true,
        ],
        [
            'attr' => 'commerce_nro',
            'label' => 'Decidir - Número de comercio',
            'category' => 'ecommerce',
            'type' => 'text',
            'default' => '',
            'backend' => false,
        ],
        [
            'attr' => 'timeout_confim_payment',
            'label' => 'Tiempo máximo de espera en pantalla de confirmación de datos del pago(init)',
            'category' => 'checkout-online',
            'type' => 'text',
            'default' => 5,
            'backend' => true,
        ],
    ],
];
