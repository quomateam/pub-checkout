
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Payment */

$this->title = quoma\checkout\CheckoutModule::t('Method of Payment');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payments'), 'url' => ['#']];
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-12 header padding-top-double">
        <div class="col-xs-7 text-right">
            <img style="height: 120px" src="<?= $payment->site->image ?>" class="img-responsive inline-block">
        </div>
        <div class="col-xs-5 text-center" id="total-time">
            <h1><?= $time_out; ?> <span class="glyphicon glyphicon-time font-size-sm"></span></h1>
            <h5>Dispone <?= $time_out; ?> de minutos para completar su compra. </h5>
        </div>
    </div>
</div>



<div class="checkout-default-index">

    <h3>Seleccione un <?= quoma\checkout\CheckoutModule::t('Method of Payment') ?></h3>
    <div class="panel-body">
        <p class="">
            <?php
            //TODO: implementar i18n a nivel plataforma
            echo Yii::t('app', 'Monto Total');
            ?>: $ <?php echo Yii::$app->formatter->asCurrency($payment->pendingAmount, 'ARG'); ?> 
        </p>

        <form action="<?= $post_url ?>" method="post" id="init-form">

            <input type="HIDDEN" name="NROCOMERCIO" value="<?php echo $decidir->config['NROCOMERCIO']; ?>" size=8>
            <input type="HIDDEN" name="NROOPERACION" value="<?php echo $payment->uuid; ?>" size=<?php echo strlen($payment->uuid); ?>>

            <label>Medio de pago</label>

            <div class="form-group">
                <?php echo Html::dropDownList('MEDIODEPAGO', null, \yii\helpers\ArrayHelper::map($payments_methods, 'code', 'name'), ['class' => "form-control", 'id' => 'MEDIODEPAGO']) ?>
                <input type="HIDDEN" name="MONTO" value="<?php echo (int) (100 * round($payment->pendingAmount, 2)); ?>" size=12>
            </div>

            <label>Cuotas</label>
            <div class="form-group">
                <?php echo Html::dropDownList('CUOTAS', null, \yii\helpers\ArrayHelper::map($installments, 'key', 'qty'), ['class' => "form-control", 'id' => 'CUOTAS']) ?>
                <input type="HIDDEN" name="URLDINAMICA" value="<?php echo $decidir->getCallbackUrl(); ?>" size=<?php echo strlen($decidir->getCallbackUrl()); ?>>
            </div>

            <!--            <div class="large-12 text-center">
                            <button type="button" class="button btn btn-primary send-form">Confirmar</button>	
                        </div>	-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-xs-7 text-right">
                        <button type="button" class="button btn btn-primary send-form">Continuar</button>	
                    </div>
                    <div class="col-xs-5 text-left">
                        <h5>Dispone <strong><span id="count-down" class="text-warning"></span></strong> de minutos para continuar. </h5>
                    </div>
                </div>
            </div>
        </form>
        <?php if (!YII_ENV_PROD) { ?>
            <div id="request_uuid"><?php echo $payment->request_uuid; ?></div>
            <div id="cobros_url"><?php echo yii\helpers\Url::to(['/'], true); ?></div>
        <?php } ?>
    </div>
</div>


<script>
    var Form = new function (event) {

        //cheque si no superó el tiempo establecido antes de enviar el pago a Decidir
        this.send = function (event) {

            $('.send-form').on('click', function () {

                checkForm();

            });
        };

        function checkForm(){
            
            var data = new Object;
            data.payment_id = <?= $payment->web_payment_id ?>;
            data.payment_method_key = $('#MEDIODEPAGO :selected').val();

            console.log(data.payment_id);
            $.ajax({
                url: '<?= yii\helpers\Url::toRoute(['default/check-time']); ?>',
                data: data,
                dataType: 'json',
                type: 'post'
            }).done(function (json) {
                if (json.status === 'success') {
                    $('#init-form').submit();
                } else {
                    $('.checkout-default-index').html(json.html);
                    $('#total-time').html("");
                }
            });
        }
        ;

        var c = <?= $payment->site->platform_timeout - 1?>;
        var t;
        var timer_is_on = 0;

        this.startTime = function () {
            startCount();
        };

        function timedCount() {
            document.getElementById("count-down").innerHTML = c;

            if (c === 0) {
                console.log('c=0');
               
                stopCount();
                 checkForm();
            }
            c = c - 1;
            t = setTimeout(function () {
                timedCount()
            }, 60000);
        }

        function startCount() {
            if (!timer_is_on) {
                timer_is_on = 1;
                timedCount();
            }
        }

        function stopCount() {
            clearTimeout(t);
            timer_is_on = 0;
        }

    };


</script>

<?php
$this->registerJs('Form.send();');
$this->registerJs("Form.startTime();");

