<?php

namespace quoma\checkout\platforms\Decidir;

use Yii;

/**
 * Description of ResultModel
 *
 * @author martin
 */
class ResultModel extends \yii\base\Model{
    
    public $resultado;
    public $fechahora;
    public $monto;
    public $moneda;
    public $codautorizacion;
    public $cuotas;
    public $tarjeta;
    public $emailcomprador;
    public $noperacion;
    public $titular;
    
    public function rules(){
        
        return [
            [['resultado', 'fechahora', 'monto', 'moneda', 'codautorizacion', 'cuotas', 'tarjeta', 'emailcomprador', 'noperacion', 'titular'], 'safe']
        ];
        
    }
    
    
}
