<?php

namespace quoma\checkout\platforms\Decidir;

use quoma\checkout\modules\siteConfig\models\SiteConfig;
use Yii;
use quoma\modules\config\models\Config;
use yii\web\HttpException;

/**
 * Punto de acceso a la plataforma Decidir
 * 
 * @author gabriela
 */
class Decidir extends \quoma\checkout\components\Platform implements \quoma\checkout\components\CheckoutInterface {

    /**
     * Relativo a platforms.Decidir
     * @var type 
     */
    protected $assetsPathAlias = null;
    private $_baseUrl;
    public $config;
  

    public function __construct($owner = null) {

        $this->config = include 'config.php';
                        
        parent::__construct($owner);
    }

    public function pay($payment) {
        
        return Yii::$app->response->redirect(
                Yii::$app->urlManager->createUrl(['checkout_online/Decidir/default/init', 'payment' => $payment->uuid])
        );
    }

    public function renderForm($payment) {

        $this->render('init', array('payment' => $payment));
    }

    public function getUrl($payment) {

        return $this->_baseUrl;
    }

    public function getCallbackUrl() {
       return \Yii::$app->urlManager->createAbsoluteUrl('') . 'checkout_online/Decidir/default/result';
    }
    
    /**
     * Cancela un pago en Decidir
     * TODO: Cancelar un pago en Decidir por medio del WebService
     * @param type $web_payment
     */
    public function cancelPayment($web_payment){
        throw new HttpException(500, 'Not implemented.');
    }
    

    public function searchPayment()
    {
        // TODO: Implement searchPayment() method.
    }

    public function checkWebPayment()
    {
        //TODO
    }
}
