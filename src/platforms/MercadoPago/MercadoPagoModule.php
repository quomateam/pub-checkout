<?php

namespace quoma\checkout\platforms\MercadoPago;

class MercadoPagoModule extends \yii\base\Module
{
    public $controllerNamespace = 'quoma\checkout\platforms\MercadoPago\controllers';

    public function init()
    {
        parent::init();
        \Yii::setAlias('@platform', __DIR__);
    }
}
