<?php

namespace quoma\checkout\platforms\MercadoPago;

use Yii;

/**
 * Description of ResultModel
 *
 * @author martin
 */
class ResultModel extends \yii\base\Model {

    public $id;
    public $site_id;
    public $date_created;
    public $date_approved;
    public $operation_type;
    public $external_reference;
    public $transaction_amount;
    public $total_paid_amount;
    public $currency_id;
    public $installments;
    public $status;
    public $status_detail;
    public $released;
    public $payment_type;
    public $nickname; //Payer Nickname
    public $first_name; //Payer First name
    public $last_name; //Payer Last name
    public $email; //payer@email.com
    public $net_received_amount;
    public $marketplace_fee;

    public function rules() {

        return [
            [['id', 'site_id', 'date_created', 'date_approved', 'operation_type', 'external_reference', 'transaction_amount',
            'total_paid_amount', 'status', 'status_detail', 'released', 'payment_type', 'currency_id', 'installments', 'net_received_amount', 'marketplace_fee'], 'safe']
        ];
    }

    public function checkData($response){
        if($this->first_name == '' && $response['cardholder']['name'] != ''){
            $this->first_name = $response['cardholder']['name'];
        }
    }
}