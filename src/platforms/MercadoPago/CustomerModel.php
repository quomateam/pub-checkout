<?php

namespace quoma\checkout\platforms\MercadoPago;

use Yii;

/**
 * Description of ResultModel
 *
 * @author martin
 */
class CustomerModel extends \yii\base\Model {

   
    public $id; //PAYER_ID,
    public $nickname; //Payer Nickname
    public $first_name; //Payer First name
    public $last_name; //Payer Last name
    public $email; //payer@email.com

    public function rules() {

        return [
            [['id', 'nickname', 'first_name', 'last_name', 'email'], 'safe']
        ];
    }

}
