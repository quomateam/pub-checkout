<?php
/**
 * Created by PhpStorm.
 * User: gabriela
 * Date: 31/05/18
 * Time: 13:35
 */

namespace  quoma\checkout\platforms\MercadoPago\components\widgets;

use quoma\checkout\platforms\MercadoPago\components\behaviors\MarketPlaceBehavior;
use yii\helpers\Html;
use yii\helpers\Url;

class LinkingButton extends \yii\base\Widget
{
    public $model_id;
    public $model;
    public $checkout_token;
    public $redirect_uri;
    public $options;

    public function init()
    {
        parent::init();

        if($this->options === null){
            $this->options = [
                'class' => 'btn btn-info',
                //'target' => '_blank'
            ];
        }
    }

    public function run()
    {
        if(MarketPlaceBehavior::wasVinculated($this->model_id, $this->model)){
            return \yii\helpers\Html::a(\Yii::t('app','Unlinking To Market Place'),
                [
                    '/checkout_online/MercadoPago/mp-seller/un-linking-account',
                ],
                array_merge($this->options, ['data' => [
                    'confirm' => \Yii::t('app','Are you sure you want to unlinking account? If you continue your customers will not be able to buy your products.'),
                    'method' => 'get',
                    'params' => [
                        'id' => $this->model_id,
                        'model' => $this->model,
                    ]
                ],])
            );
        }else{
            return \yii\helpers\Html::a(\Yii::t('app','Linking To Market Place'),
                [
                    '/checkout_online/MercadoPago/mp-seller/linking-account',
                    'id' => $this->model_id,
                    'model' => $this->model,
                    't' => $this->checkout_token,
                    'back_uri' => $this->redirect_uri
                ],
                $this->options
            );
        }

    }

}