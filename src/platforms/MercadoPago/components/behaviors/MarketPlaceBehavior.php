<?php
namespace  quoma\checkout\platforms\MercadoPago\components\behaviors;
/**
 * Created by PhpStorm.
 * User: gabriela
 * Date: 30/05/18
 * Time: 11:14
 */

use \quoma\checkout\models\MpSeller;
use \quoma\checkout\models\Site;
use  \quoma\checkout\modules\siteConfig\models\SiteConfig;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class MarketPlaceBehavior extends \yii\base\Behavior
{
    public $modelClass = null;
    public $mp_seller = null;
    public $token = null;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'initMpSeller',
        ];
    }


    /**
     * @return bool
     */
    public function getAccessToken(){
        \Yii::info( $this->mp_seller->mp_seller_id, 'mp_seller-LOG');
        //si no esta vencido
        if($this->mp_seller->expiration_time > time()) {
            \Yii::info( $this->mp_seller->access_token, 'MPSELLER');

            if($this->mp_seller->access_token == ''){
                throw new Exception(400, 'Access Token not set.');
            }
            return $this->mp_seller->access_token;
        }

        if($this->refreshToken()){
            if($this->mp_seller->access_token == ''){
                throw new Exception(400, 'Access Token not set.');
            }
            return $this->mp_seller->access_token;
        }

        //TODO: Devolver error

    }

    public function refreshToken(){

        //sino renuevo las credenciales
        $secret = SiteConfig::get('mercado_pago_secret', $this->mp_seller->site_id);
        $app_id = SiteConfig::get('market_place_app_id',$this->mp_seller->site_id);

        //obtengo las crendeciales del vendedor
        $result = $this->getOauthToken([
            'client_id' => $app_id,
            'client_secret' => $secret,
            'grant_type' => 'client_credentials',
            'refresh_token' => $this->mp_seller->refresh_token,
        ]);
        \Yii::info( $result, 'REFRESHTOKEN-RESULT');

        if (isset($result->access_token)) {
            return $this->mp_seller->updateAttributes([
                'app_id' => $app_id,
                'access_token' => $result->access_token,
                'refresh_token' => $result->refresh_token,
                'expiration_time' => $result->expires_in + time()
            ]);
        }
        return false;
    }


    public function createAccessToken($code){

        //si no esta vencido
        if($this->mp_seller->expiration_time > time()) {
            return $this->mp_seller->access_token;
        }

        //sino renuevo las credenciales
        $secret = SiteConfig::get('mercado_pago_secret', $this->mp_seller->site_id);
        $app_id = SiteConfig::get('market_place_app_id',$this->mp_seller->site_id);
        $redirect_uri = $this->getRedirectUri();

        //obtengo las crendeciales del vendedor
        $result = $this->getOauthToken([
            'client_id' => $app_id,
            'client_secret' => $secret,
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $redirect_uri
        ]);

        if (isset($result->access_token) &&
            $this->mp_seller->updateAttributes([
                'app_id' => $app_id,
                'access_token' => $result->access_token,
                'refresh_token' => $result->refresh_token,
                'expiration_time' => time() + $result->expires_in
            ]))
        {
            return $this->mp_seller->access_token;
        }

        return false;

    }

    private function getOauthToken($data){

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $url = 'https://api.mercadopago.com/oauth/token';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);

        if ($result === false) {
            $error_message = curl_error($ch);
            \Yii::error($error_message, 'CHECKOUT_MP_getOauthToken_Error');
            echo "Error: " . $error_message;
            echo "\n";
        }

        //close connection
        curl_close($ch);

        \Yii::info($result, 'CHECKOUT_MP_getOauthToken_Result');

        return json_decode($result);
    }

    public function getMpSellerID(){
        return $this->mp_seller->mp_seller_id;
    }

    public function getMpSeller(){
        return $this->mp_seller;
    }

    /**
     * @param $app_id   APP_ID Identificador de aplicación de Market Place de Mercado Pago
     * @param $redirect_uri   Redirect URI donde serán redireccionados los vendedores para poder ser vinculados correctamente
     */
    public function getUrlLinkingAccounts($site_token, $back_uri){

        $site = Site::find()->where(
            "server_name='$site_token'"
        )->one();

        if(!$site){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->mp_seller->updateAttributes(['redirect_uri' => $back_uri]);

        $app_id = SiteConfig::get('market_place_app_id',$site->site_id);

        $redirect_uri = $this->getRedirectUri();

        return "https://auth.mercadopago.com.ar/authorization?client_id=$app_id&response_type=code&platform_id=mp&redirect_uri=$redirect_uri";
    }

    public function getRedirectUri(){
        $url = \Yii::$app->urlManager->createAbsoluteUrl('', 'https');
        $url .= 'checkout_online/MercadoPago/mp-seller/linking-accounts-back?seller_id=';
        $url .= $this->mp_seller->mp_seller_id;
        \Yii::trace($url);
        return $url;
    }

    public function initMpSeller($event){
        $class = $this->modelClass;
        $pk = $class::primaryKey()[0];

        $mp_seller = MpSeller::findOne(['model_id' => $this->owner->$pk,'model_class' => $class]);


        if(!$mp_seller){

            $site = Site::findOne(['server_name' => $this->token]);

            if(!$site){
                throw new NotFoundHttpException('Site not found. Please check your site server name config.');
            }

            $app_id = SiteConfig::get('market_place_app_id',$site->site_id);

            //inicializar el Vendedor
            $mp_seller = new MpSeller();
            $mp_seller->model_class = $this->getModelClass();
            $mp_seller->model_id = $this->owner->$pk;
            $mp_seller->app_id = $app_id;
            $mp_seller->site_id = $site->site_id;
            $mp_seller->save();
        }

        $this->mp_seller = $mp_seller;

    }

    public static function wasVinculated($model_id, $model){
        $mp_seller = MpSeller::findOne(['model_id' => $model_id,'model_class' => $model]);

        if(!$mp_seller){
            return false;
        }

        if(!in_array($mp_seller->access_token, ['',NULL])){
            return true;
        }

        return false;
    }

    public function getModelClass(){

        if($this->modelClass){
            return $this->modelClass;
        }else{
            return $this->owner->className();
        }
    }

}