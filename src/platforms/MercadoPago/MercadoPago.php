<?php

namespace quoma\checkout\platforms\MercadoPago;

use quoma\checkout\models\MpSeller;
use quoma\checkout\models\PaymentMethod;
use quoma\checkout\modules\siteConfig\models\SiteConfig;
use Yii;
use yii\helpers\StringHelper;
use quoma\checkout\models\SiteData;
use quoma\modules\config\models\Config;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MercadoPago
 *
 * @author Gabriela
 */
class MercadoPago extends \quoma\checkout\components\Platform implements \quoma\checkout\components\CheckoutInterface {

    private $mp;
    private $access_token;
    public $config;
    public $client_id;
    public $secret;



    public function renderForm($payment) {
        
    }

    public static function getPlatform() {
        return \quoma\checkout\models\Platform::find()->where(['name' => 'Mercado Pago'])->one();
    }

    /**
     * Retorna una instancia de mercadopago para hacer los request al mismo.
     *
     * @return \MP
     */
    public static function getMercadoPago($payment) {

        if($payment->is_market_place){

            $mp_seller = MpSeller::findOne($payment->mp_seller_id);
            $model = $mp_seller->loadModel();
            $access_token = $model->getAccessToken();
            Yii::info($access_token, 'AccessToken');
            if($access_token == NULL){
                Yii::info($access_token, 'Access Token not found.');
                throw new \yii\web\HttpException(400, Yii::t('yii', 'Access Token not found.'));
            }
            $mp = new \MP($access_token);
            Yii::info($mp, 'MP');
        }else{

            //busco los datos que envió el sitio
            $client_id = SiteConfig::get('mercado_pago_client_id', $payment->site_id);
            $secret = SiteConfig::get('mercado_pago_secret', $payment->site_id);

            $mp = new \MP($client_id, $secret);

        }

        $mp->sandbox_mode(SiteConfig::get('mercado_pago_sandbox', $payment->site_id));

        return $mp;
    }

    /**
     * Pago
     *
     * @param $payment
     * @return array|void
     * @throws \yii\web\HttpException
     */
    public function pay($payment) {

        $site = $payment->getSite()->with(['paymentMethods', 'installments'])->one();

        //chequeo si no ha superado el tiempo disponible para la operación
        if ($payment->isOnTimeout()) {
            return [
                'status' => 'error',
                'message' => 'Lo sentimos! Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                'site' => $site->name,
                'operation' => $payment->uuid
            ];
        }

        $preference_data = $this->getPreferenceData($payment);

        try {
            $preference = $this
                ->getMercadoPago($payment)
                ->create_preference($preference_data);

            $url = $preference['response'][(SiteConfig::get('mercado_pago_sandbox', $site->site_id) ? "sandbox_" : "") . 'init_point'];

        } catch (\Exception $ex) {
            Yii::error('ERROR-MERCADOPAGO Payment broker error: '. $ex->getMessage(), 'ERROR-MERCADOPAGO ');

            error_log('ERROR-MERCADOPAGO Payment broker error: '. $ex->getMessage());
            throw new \yii\web\HttpException(400, Yii::t('yii', 'Payment broker error.'));
        }

        if (!$payment->isOnTimeout()) {
            return [
                'status' => 'redirect',
                'url' => $url
            ];
        }

        return [
            'status' => 'error',
            'message' => 'Lo sentimos! No se pudo procesar el pago.',
            'site' => $site->name,
            'operation' => $payment->uuid
        ];
    }

    /**
     * Arma los datos necesarios para la Preferencia de Pago
     * @param $payment
     * @return array
     */
    public function getPreferenceData($payment){

        $items[] = [
            "title" => $payment->concept,
            "quantity" => 1,
            //TODO: default o recibir por parametro en init
            "currency_id" => "ARS",
            "unit_price" => (float)$payment->amount,
            "picture_url" => $payment->site->image,
        ];

        $excluded_payment_methods = $this->getExcludedPaymentMethods($payment->site);
        $payment_methods = [
            "excluded_payment_methods" => $excluded_payment_methods,
            "excluded_payment_types" => [],
        ];

        //installments no es una relacion
        if ($payment->installments) {
            $payment_methods['default_installments'] = $payment_methods['installments'] = (int)$payment->installments;
        } else {
            $payment_methods['installments'] = (int)$payment->site->findInstallment();
        }

        $preference_data = [
            'id' => $payment->uuid,
            'external_reference' => $payment->uuid,
            'items' => $items,
            'additional_info' => $payment->concept,
            'payer' => [
                'name' => '',
                'surname' => '',
                'email' => '',
                'phone' => [
                    'number' => ''
                ],
                'address' => [
                    'street_name' => '',
                    'zip_code' => ''
                ]
            ],
            'auto_return' => 'approved',
            'payment_methods' => $payment_methods,
            'back_urls' => [
                "success" => $this->getCallbackUrl(),
                "failure" => $this->getCallbackUrl(),
                "pending" => $this->getCallbackUrl()
            ],
        ];

        if($payment->expires){
            $expiration_date = $payment->getExpirationDateTo();
            if($expiration_date != NULL){
                $preference_data['expires'] = ($payment->expires == 1) ? true : false;
                $preference_data['expiration_date_to'] = $expiration_date;
            }
        }

        //Marketplace
        if($payment->is_market_place){
            if($payment->marketplace_fee > 0) {
                $marketplace_fee =  ($payment->marketplace_fee*$payment->amount)/100;
            }else {
                $fee = SiteConfig::get('marketplace_fee', $payment->site_id);
                if(!in_array($fee, ['', NULL, 0])){
                    $marketplace_fee = ($fee*$payment->amount)/100;
                }
            }

            if(isset($marketplace_fee) && $marketplace_fee>0){
                $payment->updateAttributes(['marketplace_fee' => $marketplace_fee]);
                $preference_data['marketplace_fee'] = (double)$marketplace_fee;
            }
        }

        //Url callback de notificacion de pago
        if($payment->result_url){
            $preference_data['notification_url'] = $payment->result_url;
        }else{
            $preference_data['notification_url'] = \Yii::$app->urlManager->createAbsoluteUrl(['checkout_online/MercadoPago/default/result','site_id' => $payment->site_id, 'uuid' => $payment->uuid]);
        }

        Yii::info($preference_data, 'PREFERENCE-DATA-LOG');
        return $preference_data;
    }

    /**
     * Callback url por defecto para mercado pago
     *
     * @return string
     */
    public function getCallbackUrl() {
        return \Yii::$app->urlManager->createAbsoluteUrl(['checkout_online/MercadoPago/default/back']);
    }

    /**
     * Chequea el estado de un pago.
     * @param $web_payment
     * @return array
     */
    public function checkWebPayment($web_payment){

        try {
            //Buscamos el pago en Mercado Pago
            $result = MercadoPago::searchPayments($web_payment, ["external_reference" => $web_payment->uuid], 0, 10);
        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => 'No se pudo encontrar el Pago en Mercado Pago',
                'exception' => $exc->getTraceAsString()
            ];
        }

        if ($result['status'] == 'error') {
            $web_payment->updateAttributes([
                'status' => 'error',
                'message' => StringHelper::truncate($result['message'], 250, '...'),
            ]);
            return [
                'status' => 'error',
                'message' => $result['message'],
            ];
        }

        //Si el estado es "warning", el pago aún no se ha encontrado en mercado pago. Se debe esperar más tiempo
        if ($result['status'] == 'warning') {

            if(isset($result['message'])){
                $web_payment->updateAttributes([
                    'message' => StringHelper::truncate($result['message'], 250, '...'),
                ]);
            }

            return [
                'status' => 'warning',
                'message' => $result['message'] ?? 'No message.',
            ];
        }

        $mp_payment = $result['result'];

        //si hay mas de un pago asociado al reference
        if (count($mp_payment) > 1) {
            $paid = 0;
            foreach ($mp_payment as $key => $payment) {

                if ($payment['collection']['status'] == 'approved') {
                    $paid = 1;
                }
            }
        }

        if ($mp_payment[0]['collection']['status'] == 'rejected'){
            $web_payment->changeStatus('rejected');
        }

        //si esta pagado en mercado pago le cambio el estado a pagado al pago del modulo
        if (($mp_payment[0]['collection']['status'] == 'approved' || (isset($paid) && $paid))) {
            $web_payment->changeStatus('paid');
            //Monto real que recibe el vendedor. SOLO CUANDO USAMOS EL MARKETPLACE
            if (isset($mp_payment[0]['collection']['transaction_details'])){
                $web_payment->updateAttributes(['net_amount' => $mp_payment[0]['collection']['transaction_details']['net_received_amount']]);
            }
        }

        return [
            'status' => 'success',
        ];

    }

    /**
     * Busca un pago en Mercado Pago segùn los filtros enviados
     * @param $site_id
     * @param $filters
     * @param $offset
     * @param $limit
     * @return array
     */
    public static function searchPayments($web_payment, $filters, $offset, $limit) {

        $mp = \quoma\checkout\platforms\MercadoPago\MercadoPago::getMercadoPago($web_payment);

        if (!$mp) {
            return [
                'status' => 'error',
                'message' => 'No existe la plataforma con nombre "Mercado Pago" para el sitio '
            ];
        }

        try {
            //Buscamos el pago en Mercado Pago
            $search_result = $mp->search_payment($filters, $offset, $limit);
        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => $exc->getTraceAsString()
            ];
        }

        if ($search_result['status'] == 200 && count($search_result['response']['results']) < 1){
            return [
                'status' => 'warning',
                'message' => 'No se encontró el pago en Mercado Pago'
            ];
        }
        if ($search_result['status'] != 200 || !isset($search_result['response']['results'][0]['collection'])) {
            return [
                'status' => 'error',
                'message' => 'No se recibió respuesta del pago de Mercado Pago'
            ];
        }

        return [
            'status' => 'success',
            'result' => $search_result['response']['results']
        ];
    }

    /**
     * Inicia la cancelaciòn de  un pago en Mercado Pago
     * @param $web_payment
     * @return array
     */
    public function cancelPayment($web_payment) {

        $this->checkWebPayment($web_payment);
        $web_payment->refresh();

        if($web_payment->status != 'pending'){
            return [
                'status' => 'error',
                'message' => 'El pago no se encuentra en estado pendiente por lo tanto no puede ser cancelado.',
            ];
        }

        try {
            //Buscamos el pago en Mercado Pago
            $mp_payment = MercadoPago::searchPayments($web_payment, ["external_reference" => $web_payment->uuid], 0, 10);
        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => 'No se pudo encontrar el Pago en Mercado Pago',
                'exception' => $exc->getTraceAsString()
            ];
        }

        if ($mp_payment['status'] == 'error') {
            return [
                'status' => 'error',
                'message' => $mp_payment['message'] ?? 'Error con la plataforma Mercado Pago.',
            ];
        }

        try {

            //si aun no se ha aprobado cancelo el pago, puede existir más de un pago en MP para el mismo external reference
            return MercadoPago::cancelExternalPayments($web_payment, $mp_payment['result']);

        } catch (Exception $exc) {
            return [
                'status' => 'error',
                'message' => 'No se pudo cancelar el Pago en Mercado Pago.',
                'exception' => $exc->getTraceAsString()
            ];
        }

    }

    /**
     * Cancela los pagos en Mercado Pago
     * @param $site_id
     * @param $payments
     * @return array
     */
    private static function cancelExternalPayments($web_payment, $payments) {

        $result = [];
        $ok = 0;

        $mp = \quoma\checkout\platforms\MercadoPago\MercadoPago::getMercadoPago($web_payment);

        //cancelo todos los pagos con estado cancelable
        foreach ($payments as $key => $payment) {

            if (in_array($payment['collection']['status'], ['pending', 'in_process', 'rejected'])) {
                //si aun no se ha aprobado cancelo el pago,
                $result = $mp->cancel_payment($payment['collection']['id']);

                if ($result['status'] == 200) {
                    $ok++;
                }
            }
        }

        if($ok > 0){

            $web_payment->updateAttributes(['status' => 'canceled']);

            return [
                'status' => 'success',
                'message' => "$ok pagos cancelados de ".count($payments),
            ];
        }else{
            return [
                'status' => 'error',
                'message' => 'No se pudo cancelar el pago.',
            ];
        }

    }

    /**
     *Devuelve los medios de pagos que se deben excluir
     * @param type $platform_id
     * @return type
     */
    public function getExcludedPaymentMethods($site) {
        $payment_methods = [];
        $pms = $site->paymentMethods;
        if ($pms) {
            foreach ($pms as $method) {
                $payment_methods[] = $method->payment_method_id;
            }
        }

        $platform = MercadoPago::getPlatform();
        $excluded_code = PaymentMethod::find()
            ->select('payment_method.code as id')
            ->joinWith('siteHasPaymentMethods')
            ->where(['not in', 'payment_method.payment_method_id', $payment_methods])
            ->andWhere("payment_method.platform_id=$platform->platform_id")
            ->createCommand()->queryAll();

        return $excluded_code;
    }

}
