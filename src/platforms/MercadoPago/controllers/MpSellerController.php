<?php
/**
 * Created by PhpStorm.
 * User: gabriela
 * Date: 30/05/18
 * Time: 11:55
 */

namespace quoma\checkout\platforms\MercadoPago\controllers;


use quoma\checkout\models\MpSeller;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class MpSellerController extends Controller
{

    public function actionLinkingAccount($id, $model, $t, $back_uri){

        $model = $model::findOne($id);

        $url = $model->getUrlLinkingAccounts( $t , $back_uri);
        return $this->redirect($url);
    }

    /**
     * Enlaza la cuenta de un vendedor con el market place
     * @param $code  AUTHORIZATION_CODE código de autorización para operar en Mercado Pago en nombre del vendedor
     */
    public function actionLinkingAccountsBack($seller_id , $code)
    {

        if($seller_id && $code){
            //busco el vendedor
            $seller = $this->findModel($seller_id);
            $class_model = $seller->model_class;

            $model =  $class_model::findOne($seller->model_id);

            if($model->createAccessToken($code)){
                if($seller->redirect_uri != ''){
                    if (strpos($seller->redirect_uri, '?') === false){
                        $redirect_url= $seller->redirect_uri.'?'."type=linking&status=success";
                    }else{
                        $redirect_url= $seller->redirect_uri."&type=linking&status=success";
                    }
                    return $this->redirect($redirect_url);
                }

                return $this->render('result',[ 'status' => 'success','message' => 'Cuenta vinculada con éxito!']);
            }else{
                if($seller->redirect_uri != ''){
                    if (strpos($seller->redirect_uri, '?') === false){
                        $redirect_url= $seller->redirect_uri.'?'."type=linking&status=error";
                    }else{
                        $redirect_url= $seller->redirect_uri."&type=linking&status=error";
                    }
                    return $this->redirect($redirect_url);
                }

            }

        }

        return $this->render('result', ['status' => 'error','message' => 'Algo salió mal. No pudimos vincular tu cuenta, por favor intentalo nuevamente.']);

    }

    public function actionUnLinkingAccount($id, $model){

        $model = $model::findOne($id);

        $mp_seller = $model->getMpSeller();

        if($mp_seller->updateAttributes(['access_token' => NULL, 'refresh_token' => NULL, 'expiration_time' => NULL])){
            if($mp_seller->redirect_uri != ''){
                if (strpos($mp_seller->redirect_uri, '?') === false){
                    $redirect_url= $mp_seller->redirect_uri.'?'."type=unlinking&status=success";
                }else{
                    $redirect_url= $mp_seller->redirect_uri."&type=unlinking&status=success";
                }
                return $this->redirect($redirect_url);
            }
            return $this->render('result',[ 'status' => 'success','message' => 'Cuenta vinculada con éxito!']);
        }else{
            if($mp_seller->redirect_uri != ''){
                if (strpos($mp_seller->redirect_uri, '?') === false){
                    $redirect_url= $mp_seller->redirect_uri.'?'."type=unlinking&status=error";
                }else{
                    $redirect_url= $mp_seller->redirect_uri."&type=unlinking&status=error";
                }
                return $this->redirect($redirect_url);
            }

        }

        return $this->render('result', ['status' => 'error','message' => 'Algo salió mal. No pudimos desvincular tu cuenta, por favor intentalo nuevamente.']);

    }


    /**
     * Finds the MpSeller model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MpSeller the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MpSeller::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}