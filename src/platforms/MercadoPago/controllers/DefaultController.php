<?php

namespace quoma\checkout\platforms\MercadoPago\controllers;

use quoma\checkout\modules\siteConfig\models\SiteConfig;
use Yii;
use yii\web\Controller;
use quoma\checkout\platforms\MercadoPago\MercadoPago;
use quoma\checkout\models\WebPayment;
use quoma\checkout\models\Platform;
use quoma\checkout\models\SiteData;
use quoma\checkout\models\Site;
use quoma\checkout\models\WebPaymentData;
use quoma\modules\config\models\Config;

class DefaultController extends Controller {

    public function init() {
        parent::init();
        //Agregado porq si no puedo enviarle post
    }

    /**
    * @inheritdoc
    */
    public function beforeAction($action)
    {
        if ($action->id == 'result') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }


    /**
     * Chequea el tiempo que el cliente se demoró en la vista de cobros para no enviar pagos que puedn superar el tiempo máximo de operación
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCheckTime() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $json = [];

        if ($post = \Yii::$app->request->post()) {

            $web_payment = WebPayment::findOne($post['payment_id']);

            if (!$web_payment) {
                $json['status'] = 'error';
                $json['message'] = 'El pago no existe!';
                return $json;
            }

            //chequeo si no ha superado el tiempo disponible para la operación
            if ($web_payment->isOnTimeout()) {
                $web_payment->changeStatus('timeout');
                $this->notifyPaymentTimeOut($web_payment);
                $json['status'] = 'error';
                $json['html'] = $this->renderPartial('_error', [
                    'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                    'site' => $web_payment->site->name,
                    'operation' => $web_payment->uuid
                ]);
                return $json;
            }

            $json['status'] = 'success';
            $json['message'] = 'OK';

            return $json;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function NotifyPaymentTimeOut($payment) {

        if (isset($payment->request_uuid))
            $nro_operation = $payment->request_uuid;
        else
            throw new \yii\web\HttpException(400, \Yii::t('yii', 'Your request is invalid.'));

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $url = str_replace('www.', '', $payment->result_url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $nro_operation, 'status' => $payment->status]);

        if (SiteConfig::get('checkout_mode_testing', $payment->site_id)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        
        //execute post
        $result = curl_exec($ch);
        
        if ($result === false) {
            echo "Error: " . curl_error($ch);
            echo "\n";
        }
        //close connection
        curl_close($ch);
        
//        \Yii::$app->response->format = 'json';
        return [
            'status' => 'ok',
            'message' => '',
        ];
    }
    
    public function actionResult() {

        $oRequest = array_merge($_GET, $_POST);

        try {
            $site_id = $oRequest["site_id"];
            $topic = $oRequest["topic"];
            $uuid = (isset($oRequest["uuid"])) ? $oRequest["uuid"] : NULL;
            $payment_id = $oRequest["id"];
            $merchant_order_info = null;

            $platform = MercadoPago::getPlatform();
            $site = Site::findOne($site_id);

            if (!isset($oRequest["id"]) || !isset($platform) || !isset($site)) {
                error_log('ERROR-MERCADOPAGO: ID, platform or site not defined');
            } else {

                $client_id = SiteConfig::get('mercado_pago_client_id', $site_id);
                $secret = SiteConfig::get('mercado_pago_secret', $site_id);

                $mp = new \MP($client_id, $secret);

                if($uuid){
                    $web_payment = WebPayment::findOne(['uuid' => $uuid]);
                    $mp = MercadoPago::getMercadoPago($web_payment);
                }

                //obtengo la información del pago
                $payment_info = $mp->get("/collections/notifications/$payment_id");

                if ($payment_info["response"]["collection"]["merchant_order_id"] != '') {
                    $merchant_order_info = $mp->get("/merchant_orders/" . $payment_info["response"]["collection"]["merchant_order_id"]);

                    if ($merchant_order_info == null) {
                        error_log('ERROR-MERCADOPAGO: Error obtaining the merchant_order');
                    }

                    if ($merchant_order_info["status"] != 200) {
                        error_log('ERROR-MERCADOPAGO: Error obtaining the merchant_order status');
                    }

                    $model = new \quoma\checkout\platforms\MercadoPago\ResultModel();
                    $model->setAttributes($payment_info["response"]["collection"], false);

                    $customer = new \quoma\checkout\platforms\MercadoPago\ResultModel();
                    $customer->setAttributes($payment_info["response"]["collection"]['payer'], false);
                    $customer->checkData($payment_info["response"]["collection"]);

                    $response = [
                        'id' => $model->id,
                        'status' => $model->status,
                        'amount' => (float)(str_replace(',', '.', $model->transaction_amount)),
                        'currency' => $model->currency_id,
                        'autorization_code' => '',
                        'installments' => $model->installments,
                        'owner' => $customer->first_name . ' ' . $customer->last_name,
                        'card' => '',
                        'email' => $customer->email,
                        'datetime' => date('Y-m-d H:i:s', strtotime($model->date_approved)),
                        'payment' => $model->external_reference,
                        'messages' => ''
                    ];


                    $payment = $this->loadPayment($model->external_reference);
                    $payment->updateAttributes(['installments' => $model->installments, 'net_received_amount' => $model->net_received_amount, 'payer_email' => $customer->email,'marketplace_fee' => $model->marketplace_fee]);
                    $payment->receipt($response);
                    $nro_operation = $payment->request_uuid;

                    $result = WebPayment::NotifyToSite($payment, ['data' => $nro_operation, 'remote_uuid' => $payment->uuid, 'status' => $payment->status, 'response' => serialize($response), 'type' => $topic, 'operation_type' => $payment_info['response']['collection']['operation_type'], 'platform_payment_id' => $model->id]);

                }
            }

        }catch(\Exception $ex) {
            error_log('ERROR-MERCADOPAGO: '. $ex->getMessage());
        }
        Yii::$app->response->statusCode = 200;
        return 'ok';
    }

    /**
     * Si encuentra el pago asociado al uuid, lo devuelve. Caso contrario
     * dispara una excepcion.
     * @param string $uuid
     * @return WebPayment
     * @throws CHttpException
     */
    public function loadPayment($uuid) {

        $model = WebPayment::find()->where(['uuid' => $uuid])->one();

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
        return $model;
    }

    /**
     * Si encuentra el pago asociado al uuid, lo devuelve. Caso contrario
     * dispara una excepcion.
     * @param string $uuid
     * @return WebPayment
     * @throws CHttpException
     */
    public function loadPaymentPreaproval($uuid, $external_id,$site) {

        $model = WebPayment::find()->where(['uuid' => $uuid])->one();
       
        if($model){
            //busco  el id del pago en mp
            $web_payment_data = WebPaymentData::find()->where(['external_payment_id' => $external_id])->one();
            
            $group_payments  = WebPayment::find()->where(['request_uuid' => $model->request_uuid])->one();
            $count  = WebPayment::find()->where(['request_uuid' => $model->request_uuid])->count();
            
            if($web_payment_data 
                    || ($count == 1 && !WebPaymentData::find()->where(['web_payment_id' => $group_payments->web_payment_id])->one())){
                $model = WebPayment::findOne($model->web_payment_id);
                return $model;
            }
        }
       
        if(!isset($web_payment_data) || $model === null){
            $new_model = new \quoma\checkout\models\WebPayment();
            $new_model->load($model);
            $new_model->link('site', $site);
            $new_model->platform_id = $model->platform_id;
            $new_model->return_url = $model->return_url;
            $new_model->result_url = $model->result_url;
            $new_model->request_uuid = $model->request_uuid;
            $new_model->installments = $model->installments;
            $new_model->payer_email = $model->payer_email;
            $new_model->frequency = $model->frequency;
            $new_model->frequency_type = $model->frequency_type;
            $new_model->reason = $model->reason;
            $new_model->preapproval_payment = $model->preapproval_payment;
            $new_model->amount = $model->amount;
            $new_model->type = $model->type;

            if (!$new_model->save()) {
                throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
            }
        }
        return $new_model;
    }

    public function actionBack() {
        $is_subscription = false;
        
        if (isset($_GET['external_reference'])) {
            $operation = $_GET['external_reference'];
        } elseif (isset($_GET['preapproval_id'])) {
            $is_subscription = true;
            $payment_info = WebPayment::getPaymentInfo($_GET["site_id"], $_GET['preapproval_id']);
            $operation = WebPayment::getPaymentUuid($payment_info['response']['external_reference']);
        }

        if(!isset($operation) || (isset($operation) && $operation == '')){
            throw new \yii\web\HttpException(404, 'Operation not found.');
        }

        //Buscamos el pago asociado
        $payment = WebPayment::find()->where(['uuid' => $operation])->one();

        if ($payment) {

            if (isset($payment->request_uuid))
                $operation = $payment->request_uuid;
            else
                throw new \yii\web\HttpException(500, 'The operation does not exist.');

            //redirecciono 
            $return_url = $payment->return_url;
            $status = ($is_subscription)?$payment_info['response']['status']:$payment->status;
            if (strpos($return_url, '?') === false)
                $url = "$return_url?data=$operation&status=$status";
            else
                $url = "$return_url&data=$operation&status=$status";

            return Yii::$app->response->redirect($url);
        }else {
            throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
        }
    }

}
