<?php
namespace quoma\checkout\platforms\Manual;

class Manual extends \quoma\checkout\components\Platform implements \quoma\checkout\components\CheckoutInterface
{

    /**
     * Devuelve la plataforma
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getPlatform() {
        return \quoma\checkout\models\Platform::find()->where(['name' => 'Manual'])->one();
    }

    public function pay($payment){
    }

    public function renderForm($payment){
    }

    public function getCallbackUrl(){
    }

    public function cancelPayment($payment){

        $payment->updateAttributes(['status' => 'canceled']);

        return [
            'status' => 'success',
        ];
    }

    public function checkWebPayment($payment){
    }
}