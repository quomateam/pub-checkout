var FormPay = new function (event) {

    this.url;
    this.web_payment_id;
    this.web_payment_uuid;
    this.public_key_decidir_prisma;
    this.decidir_url;
    this.timeout_confim_payment;
    this.c;
    this.cybersource;
    this.device_id;


    this.init = function(url, web_payment_id, web_payment_uuid, public_key_decidir_prisma, decidir_url, timeout_confim_payment, cs, device_id) {

        FormPay.url = url;
        FormPay.web_payment_id = web_payment_id;
        FormPay.web_payment_uuid = web_payment_uuid;
        FormPay.public_key_decidir_prisma = public_key_decidir_prisma;
        FormPay.decidir_url = decidir_url;
        FormPay.timeout_confim_payment = timeout_confim_payment;
        FormPay.c = timeout_confim_payment;
        FormPay.cybersource= cs;
        FormPay.device_id= device_id;
        
        $('[data-decidir="card_holder_doc_number"]').on('keypress', function (e) {
            if (e.keyCode < 48 || e.keyCode > 57) {
                e.preventDefault();
            }
        })

    }

    //cheque si no superó el tiempo establecido antes de enviar el pago a Decidir
    this.send = function (event) {

        $('.send-form').on('click', function (e) {
            e.preventDefault();
            document.getElementById("btn-send-form").disabled = true;
            checkTime();

        });
    };


    //Chequea si se ha superado el tiempo
    function checkTime(){

        var data = new Object;
        data.payment_id = FormPay.web_payment_id;
        data.payment_method_key = $('#payment_method_id :selected').val();

        $.ajax({
            url: FormPay.url + 'default/check-time',
            data: data,
            dataType: 'json',
            type: 'post'
        }).done(function (json) {
            if (json.status === 'success') {

                getToken();
                document.getElementById("btn-send-form").disabled = false;

            } else {
                document.getElementById("btn-send-form").disabled = false;
                $('.checkout-default-index').html(json.html);
                $('#total-time').html("");
                $('#counter').html("");

            }
        });
    }

    //Realiza la request para crear el token de la transaccion, si usa cybersource en el form debe esta el device_unique_id
    function getToken() {
        $('#init-form .text-danger').remove();
        /**var decidir = getDecidirPrisma();
         console.log(decidir);
         var form = document.querySelector('#init-form');
         console.log('Datos del formulario: '+ $(form));
         decidir.createToken(form, sdkResponseHandler); //formulario y callback**/
        var data = {
            card_number: $('[data-decidir="card_number"]').val(),
            card_expiration_month: $('[data-decidir="card_expiration_month"]').val(),
            card_expiration_year: $('[data-decidir="card_expiration_year"]').val(),
            security_code: $('[data-decidir="security_code"]').val(),
            card_holder_name: $('[data-decidir="card_holder_name"]').val(),
            card_holder_identification: {
                type: $('[data-decidir="card_holder_doc_type"]').val(),
                number: $('[data-decidir="card_holder_doc_number"]').val()
            }

        };

        if (FormPay.cybersource) {
            data.fraud_detection = {
                device_unique_identifier: $('[data-decidir="device_unique_identifier"]').val()
            }
        }

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": FormPay.decidir_url + "/tokens",
            "method": "POST",
            "headers": {
                "apikey": FormPay.public_key_decidir_prisma,
                "content-type": "application/json",
                "cache-control": "no-cache",
                "dataType": 'json'
            },
            "processData": false,
            "data": JSON.stringify(data)

        };
        console.log(settings.data);
        $.ajax(settings).done(function (response, textStatus, jqXHR) {
            console.log(response);
            sdkResponseHandler(jqXHR.status, response);
        }).fail(function (jqXHR, textStatus, ex) {
            console.log(jqXHR);
            sdkResponseHandler(jqXHR.status, jqXHR.responseJSON);
        });
    }

    function getDecidirPrisma() {
        const publicApiKey = FormPay.public_key_decidir_prisma;

        const url = FormPay.decidir_url;
        const decidir = new Decidir(url, FormPay.cybersource);
        decidir.device_unique_identifier= FormPay.device_id;
        decidir.setPublishableKey(publicApiKey);
        decidir.setTimeout(0); //se configura sin timeout

        return decidir;
    }

    //funcion para manejar la respuesta
    function sdkResponseHandler(status, response) {
        if (status !== 200 && status !== 201) {
            $("#error-message").show();
            console.log(response);
            if (response.validation_errors) {
                $.each(response.validation_errors, function(i, e) {
                    var input = $('[data-decidir="'+e.param+'"]');
                    $(input).after('<span class="text-danger" style="size: 8pt">'+$(input).data('error_msj')+'</span>')
                });
            }

        } else {
            console.log(response);
            registerResponse(response);

        }
    }



    function registerResponse(response){
        $('<form id="response-data"></form>').appendTo('body');
        var form = $('#response-data');

        form.append(jQuery('<input>', {
            name: 'WebPayment[payment_method_id]',
            value: $('#payment_method_id').val(),
            type: 'hidden'
        }));

        form.append(jQuery('<input>', {
            name: 'WebPayment[installments]',
            value: $('#installments').val(),
            type: 'hidden'
        }));


        form.append(jQuery('<input>', {
            name: 'WebPayment[token]',
            value: response.id,
            type: 'hidden'
        }));

        form.append(jQuery('<input>', {
            name: 'WebPayment[bin]',
            value: response.bin,
            type: 'hidden'
        }));

        form.append(jQuery('<input>', {
            name: 'WebPayment[uuid]',
            value: FormPay.web_payment_uuid,
            type: 'hidden'
        }));

        if (FormPay.cybersource){
            form.append(jQuery('<input>', {
                name: 'WebPayment[cybersource_data][address]',
                value: $('#cs_address').val(),
                type: 'hidden'
            }));

            form.append(jQuery('<input>', {
                name: 'WebPayment[cybersource_data][city]',
                value: $('#cs_city').val(),
                type: 'hidden'
            }));

            form.append(jQuery('<input>', {
                name: 'WebPayment[cybersource_data][postal_code]',
                value: $('#cs_postal_code').val(),
                type: 'hidden'
            }));

            form.append(jQuery('<input>', {
                name: 'WebPayment[cybersource_data][state]',
                value: $('#cs_state').val(),
                type: 'hidden'
            }));

            form.append(jQuery('<input>', {
                name: 'WebPayment[cybersource_data][phone]',
                value: $('#cs_phone').val(),
                type: 'hidden'
            }));
        }


        $.ajax({
            url: FormPay.url +'default/register-response',
            data: form.serialize(),
            dataType: 'json',
            type: 'post'
        }).done(function (json) {
            if (json.status === 'success') {
                $('.checkout-default-index').css('display','none');
                $('#payment-confirm').css('display','');
                $('#payment-confirm').html(json.html);
                $('#count-operation').html('pagar');
            }
        });
    }


    this.confirm = function(event){

        $('#pay-confirm').on('click', function(event){
            $('#payment-confirm').addClass('loading');

            event.preventDefault();

            pay();
        });
    }

    //pay(response);
    function pay(){

        $.ajax({
            url: FormPay.url +'default/pay',
            data:{
                uuid: FormPay.web_payment_uuid
            },
            dataType: 'json',
            type: 'post'
        }).done(function (json) {
            if (json.status === 'error') {
                console.log(json.errors)
                window.location.href= json.url;
                /**
                $("#error-message").css("display", '');
                $('.checkout-default-index').css('display','');
                $('#payment-confirm').removeClass('loading');
                $('#payment-confirm').css('display','none');
                 **/
            }
        });

    }

    var t;
    var timer_is_on = 0;

    this.startTime = function () {

        startCount();
    };

    function timedCount() {

        document.getElementById("count-down").innerHTML = FormPay.c;

        if (FormPay.c <= 0) {
            stopCount();
            confirmPaymentCheckTime();
        }
        FormPay.c = FormPay.c - 1;
        t = setTimeout(function () {
            timedCount()
        }, 60000);
    }

    function startCount() {
        if (!timer_is_on) {
            timer_is_on = 1;
            timedCount();
        }
    }

    function stopCount() {
        clearTimeout(t);
        timer_is_on = 0;
    }

    //Chequea si se ha superado el tiempo
    function confirmPaymentCheckTime(){

        var data = new Object;
        data.payment_id = FormPay.web_payment_id;
        data.payment_method_key = $('#payment_method_id :selected').val();

        $.ajax({
            url: FormPay.url +'default/check-time',
            data: data,
            dataType: 'json',
            type: 'post'
        }).done(function (json) {
            if (json.status === 'success') {
                

            } else {
                $('.checkout-default-index').html(json.html);
                $('#total-time').html("");
                $('#counter').html("");

            }
        });
    }


}