<?php
/**
 * Created by PhpStorm.
 * User: gabriela
 * Date: 12/04/18
 * Time: 10:37
 */

namespace quoma\checkout\platforms\DecidirPrisma\assets;


use quoma\checkout\assets\CheckoutAssets;
use yii\web\AssetBundle;

class DecidirPrismaAssets extends AssetBundle
{

    public $sourcePath= '@vendor/quoma/checkout-module/src/platforms/DecidirPrisma/assets';

    public $js= [
        'js/form.js',
    ];

    public $depends= [
        'quoma\checkout\assets\CheckoutAssets',
    ];


}