<?php

namespace quoma\checkout\platforms\DecidirPrisma\models;

use quoma\checkout\models\WebPayment;
use Yii;

/**
 * This is the model class for table "cybersource_data".
 *
 * @property int $cybersource_data_id
 * @property string $first_name
 * @property string $last_name
 * @property string $customer_id
 * @property string $address
 * @property string $email
 * @property string $city
 * @property string $postal_code
 * @property string $state
 * @property string $currency
 * @property double $amount
 * @property int $days_in_site
 * @property int $is_guest
 * @property int $transactions
 * @property int $web_payment_id
 * @property int $site_id
 * @property string $phone
 * @property int $birthday
 *
 * @property WebPayment $webPayment
 * @property CybersourceItemData[] $cybersourceItemDatas
 */
class CybersourceData extends \yii\db\ActiveRecord
{

    public $items;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cybersource_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'customer_id', 'currency', 'amount', 'web_payment_id', 'site_id'], 'required'],
            [['amount'], 'number'],
            [['days_in_site', 'is_guest', 'transactions', 'web_payment_id', 'site_id', 'birthday'], 'integer'],
            [['first_name', 'last_name', 'address'], 'string', 'max' => 255],
            [['customer_id', 'postal_code', 'state', 'currency', 'phone'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 55],
            [['city'], 'string', 'max' => 50],
            [['items'], 'safe'],
            [['web_payment_id', 'site_id'], 'exist', 'skipOnError' => true, 'targetClass' => WebPayment::className(), 'targetAttribute' => ['web_payment_id' => 'web_payment_id', 'site_id' => 'site_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cybersource_data_id' => Yii::t('app', 'Cybersource Data ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'address' => Yii::t('app', 'Address'),
            'email' => Yii::t('app', 'Email'),
            'city' => Yii::t('app', 'City'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'state' => Yii::t('app', 'State'),
            'currency' => Yii::t('app', 'Currency'),
            'amount' => Yii::t('app', 'Amount'),
            'days_in_site' => Yii::t('app', 'Days In Site'),
            'is_guest' => Yii::t('app', 'Is Guest'),
            'transactions' => Yii::t('app', 'Transactions'),
            'web_payment_id' => Yii::t('app', 'Web Payment ID'),
            'site_id' => Yii::t('app', 'Site ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebPayment()
    {
        return $this->hasOne(WebPayment::className(), ['web_payment_id' => 'web_payment_id', 'site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCybersourceItemDatas()
    {
        return $this->hasMany(CybersourceItemData::className(), ['cybersource_data_id' => 'cybersource_data_id']);
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert && !empty($this->items) && is_array($this->items)){
            foreach ($this->items as $item){
                $csDataItem= new CybersourceItemData([
                    'code' => $item['code'],
                    'name' => $item['name'],
                    'description' => $item['description'],
                    'sku' => $item['sku'],
                    'total_amount' => $item['total_amount'],
                    'qty' => $item['qty'],
                    'unit_price' => $item['unit_price'],
                    'cybersource_data_id' => $this->cybersource_data_id
                ]);

                if (!$csDataItem->save()){
                    Yii::info('Cybersource Item not saved', 'Cybersource');
                    Yii::info($csDataItem->getErrors(), 'Cybersource');
                }
            }
        }
    }
}
