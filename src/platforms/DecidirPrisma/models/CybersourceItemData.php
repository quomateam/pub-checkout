<?php

namespace quoma\checkout\platforms\DecidirPrisma\models;

use Yii;

/**
 * This is the model class for table "cybersource_item_data".
 *
 * @property int $cybersource_item_data_id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $sku
 * @property double $total_amount
 * @property int $qty
 * @property double $unit_price
 * @property int $cybersource_data_id
 *
 * @property CybersourceData $cybersourceData
 */
class CybersourceItemData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cybersource_item_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name', 'sku', 'total_amount', 'qty', 'unit_price', 'cybersource_data_id'], 'required'],
            [['description'], 'string'],
            [['total_amount', 'unit_price'], 'number'],
            [['qty', 'cybersource_data_id'], 'integer'],
            [['code', 'sku'], 'string', 'max' => 45],
            [['name'], 'string', 'max' => 255],
            [['cybersource_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => CybersourceData::className(), 'targetAttribute' => ['cybersource_data_id' => 'cybersource_data_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cybersource_item_data_id' => Yii::t('app', 'Cybersource Item Data ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'sku' => Yii::t('app', 'Sku'),
            'total_amount' => Yii::t('app', 'Total Amount'),
            'qty' => Yii::t('app', 'Qty'),
            'unit_price' => Yii::t('app', 'Unit Price'),
            'cybersource_data_id' => Yii::t('app', 'Cybersource Data ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCybersourceData()
    {
        return $this->hasOne(CybersourceData::className(), ['cybersource_data_id' => 'cybersource_data_id']);
    }
}
