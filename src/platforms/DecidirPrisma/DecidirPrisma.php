<?php

namespace quoma\checkout\platforms\DecidirPrisma;

use Decidir\Cybersource\Retail;
use quoma\checkout\components\CheckoutInterface;
use quoma\checkout\models\WebPayment;
use quoma\checkout\modules\siteConfig\models\SiteConfig;
use Yii;
use quoma\modules\config\models\Config;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Punto de acceso a la plataforma Decidir
 * 
 * @author gabriela
 */
class DecidirPrisma extends \quoma\checkout\components\Platform implements CheckoutInterface{


    public static function getConnector($site){
        $keys_data = array('public_key' => SiteConfig::get('public_key_decidir_prisma', $site->site_id),
            'private_key' => SiteConfig::get('private_key_decidir_prisma', $site->site_id));

        if (YII_ENV == 'test' or SiteConfig::get('checkout_mode_testing', $site->site_id)) {
            $ambient = "test";
        } else {
            $ambient = "prod";
        }

        return new \Decidir\Connector($keys_data, $ambient);
    }

    public function pay($payment){
        Yii::$app->getResponse()->redirect(['checkout_online/DecidirPrisma/default/init','payment' => $payment->uuid]);
    }

    public function processPayment($payment) {

        $site = $payment->getSite()->with(['paymentMethods', 'installments'])->one();

        $connector = DecidirPrisma::getConnector($site);

        if (SiteConfig::get('cybersource', $site->site_id)){
            $merchant_id= SiteConfig::get('decidir_merchant_id', $site->site_id);
            $unique_id= uniqid();
            $cybersource= new Retail($payment->getCybersourceArray(), $payment->getCybersourceItemArray());
            Yii::info($cybersource->getData());

            $cybersource->setDeviceUniqueId('device_unique_identifier', $payment->web_payment_id);

            Yii::info($cybersource->getData());


            $connector->payment()->setCybersource($cybersource->getData());

        }

        $data = array(
            "site_transaction_id" => $payment->uuid,
            "token" => $payment->token,
            "payment_method_id" => (int)$payment->payment_method_id,
            "bin" => $payment->bin,
            "amount" => round($payment->net_amount, 2),
            "currency" => "ARS",
            "installments" => (int)$payment->installments,
            "description" => "",
            "payment_type" => "single",
            "sub_payments" => array(),

        );

        Yii::debug($data);
        try {
            $response = $connector->payment()->ExecutePayment($data);
            Yii::info(print_r($response, 1));
            $status = $response->getStatus();
            if(($status == 'rejected')){
                $detail = $response->getStatus_details();
            }
            $payment->updateAttributes(['platform_payment_id' => $response->getId()]);

            $r_data = [
                'id' => $response->getId(),
                'status' => $status,
                'amount' =>  (float) (str_replace(',', '.', $response->getAmount())) ,
                'currency' => $response->getCurrency(),
                'autorization_code' =>  $response->getInstallments(),
                'installments' =>  $response->getInstallments(),
                'owner' => '',
                'card' => $response->getCard_brand(),
                'email' => '',
                'datetime' => date('Y-m-d H:i:s', strtotime($response->getDate())),
                'payment' => $response->getSite_id(),
                'messages' => ($status == 'rejected') ? $detail->error['type'].' '. $detail->error['reason']['description'] .' Código de error: '.$detail->error['reason']['id'] : ''
            ];

            $payment->receipt($r_data);

            if(in_array($status, ['canceled', 'timeout'])){
                $status = 'error';
            }

            //open connection
            $ch = curl_init();

            //set the url, number of POST vars, POST data
            $url = str_replace('www.', '', $payment->result_url);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $payment->request_uuid,'remote_uuid' => $payment->uuid, 'status' => $payment->status, 'response' => serialize($response)]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if (SiteConfig::get('checkout_mode_testing', $site->site_id)) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            }

            //execute post
            $result = curl_exec($ch);

            \Yii::$app->response->format = 'json';

            if ($result === false) {
                /**echo "Error: " . curl_error($ch);
                echo "\n";**/
                return [
                    'status' => 'error',
                    'errors' => curl_error($ch),
                ];
            }

            //close connection
            curl_close($ch);


            return [
                'status' => 'success',
                'message' => '',
            ];

        } catch (\Exception $e) {
            Yii::debug($e->getMessage() . ' '. $e->getTraceAsString());
            return [
                'status' => 'error',
                'errors' => $e,
            ];
        }

    }

    //TODO: wtf??!!
    public function checkWebPayment($payment){
        return [
            'status' => 'success'
        ];
    }

    public function renderForm($payment) {

        $this->render('init', array('payment' => $payment));
    }

    public function getCallbackUrl() {
        if(\Yii::$app->params['only_secure_connection']){
            return \Yii::$app->urlManager->createAbsoluteUrl('','https') . 'checkout_online/Decidir/default/result';
        }else{
            return \Yii::$app->urlManager->createAbsoluteUrl('') . 'checkout_online/Decidir/default/result';
        }
    }
    
    /**
     * Cancela un pago en Decidir
     * TODO: Cancelar un pago en Decidir por medio del WebService
     * @param type $web_payment
     */
    public function cancelPayment($web_payment){
        if ($web_payment->status === 'paid'){
            throw new HttpException(500, 'Not implemented.');
        }

        $web_payment->updateAttributes(['status' => 'canceled']);
        return ['status' => 'success'];
    }


    public function searchPayment()
    {
        // TODO: Implement searchPayment() method.
    }
}
