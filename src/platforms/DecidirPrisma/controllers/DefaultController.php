<?php

namespace quoma\checkout\platforms\DecidirPrisma\controllers;

use function GuzzleHttp\debug_resource;
use quoma\checkout\CheckoutModule;
use quoma\checkout\modules\siteConfig\models\SiteConfig;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use quoma\checkout\platforms\DecidirPrisma\DecidirPrisma;
use quoma\checkout\models\WebPayment;
use \quoma\modules\config\models\Config;

class DefaultController extends Controller {

    public function init() {
        parent::init();
//Agregado porq si no puedo enviarle post
        $this->enableCsrfValidation = false;
    }

    public function actionInit($payment) {

        $payment = WebPayment::find()->where(['uuid' => $payment])->one();

        if (!$payment)
            throw new \yii\web\HttpException(400, Yii::t('yii', 'Your request is invalid.'));


        $decidir = new DecidirPrisma();

        //chequeo si no ha superado el tiempo disponible para la operación
        $isOnTimeout = $payment->isOnTimeout();

        if ($isOnTimeout) {
            return $this->render('_error', [
                        'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                        'site' => $payment->site->name,
                        'operation' => $payment->uuid
            ]);
        }

        if(in_array($payment->status, ['error','canceled'])){
            return $this->render('_error', [
                'message' => 'Lo sentimos! <br> El pago ya no se puede continuar. Por favor vuelva a intentarlo.',
                'site' => $payment->site->name,
                'operation' => $payment->uuid
            ]);
        }
        

        $site = \quoma\checkout\models\Site::findOne($payment->site_id);

        $payments_methods = $site->paymentMethods;
        $installments = $site->installments;

        return $this->render('init', [
                    'decidir' => $decidir,
                    'payment' => $payment,
                    'site' => $site,
                    'payments_methods' => $payments_methods,
                    'installments' => $installments,
                    'time_out' => $site->platform_timeout,
        ]);
    }

    public function actionRegisterResponse() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {

            $data = Yii::$app->request->post();

            $payment = WebPayment::find()->where(['uuid' => $data['WebPayment']['uuid']])->one();
            $payment->load(Yii::$app->request->post());

            if ($payment->load(Yii::$app->request->post()) && $payment->save()) {
                $site = $payment->site;
                $installment = $site->getInstallments()->where(['qty' => $payment->installments])->one();
                $interest = $site->getInterest($installment->installment_id, $payment->platform_id);
                $amount = $payment->pendingAmount;

                $payment->interest = $interest;
                $payment->interest_amount = ($interest * $amount) /100;
                $payment->gross_amount = $amount;
                $payment->net_amount = $amount + $payment->interest_amount;
                $payment->updateAttributes(['interest','gross_amount','net_amount','interest_amount']);
                return [
                    'status' => 'success',
                    'html' => $this->renderAjax('_payment-confirm', ['payment' => $payment])
                ];


            }else{
                $payment->getErrors();
            }
        }


    }

    public function actionPay() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (Yii::$app->request->isPost) {

            $data = Yii::$app->request->post();
            $payment = WebPayment::find()->where(['uuid' => $data['uuid']])->one();

            //chequeo si no ha superado el tiempo disponible para la operación
            $isOnTimeout = $payment->isOnTimeout();

            if ($isOnTimeout) {
                return $this->render('_error', [
                    'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                ]);
            }


            $decidir = new DecidirPrisma();

            if($payment->status == 'invalid'){
                $payment = $payment->clonePayment();
            }

            if(in_array($payment->status, ['error','canceled','invalid'])){
                return $this->render('_error', [
                    'message' => 'Lo sentimos! <br> El pago ya no se puede continuar. Por favor vuelva a intentarlo.',
                    'site' => $payment->site->name,
                    'operation' => $payment->uuid
                ]);
            }

            $result = $decidir->processPayment($payment);

            //si no existieron errores de validaciòn
            if($result['status'] == 'success'){

                return Yii::$app->response->redirect($payment->return_url.'?data='.$payment->request_uuid.'&status='.$payment->status);
            }else{
                $payment->updateAttributes(['status' => 'invalid']);
                Yii::info($result, 'Decidir');
                return [
                    'status' => 'error',
                    'errors' => $result['errors'],
                    'url' => $payment->return_url.'?data='.$payment->request_uuid.'&status='.$payment->status
                ];
            }
        }
    }


    /**
     * Chequea el tiempo que el cliente se demoró en la vista de cobros para no enviar pagos que puedn superar el tiempo máximo de operación
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCheckTime() {
       
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $json = [];
       
        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            $web_payment = WebPayment::findOne($post['payment_id']);
         
            if (!$web_payment) {
                $json['status'] = 'error';
                $json['message'] = 'El pago no existe!';
                return $json;
            }

            //chequeo si no ha superado el tiempo disponible para la operación
            $isOnTimeout = $web_payment->isOnTimeout();

            if ($isOnTimeout) {

                $this->notifyPaymentTimeOut($web_payment);
                $json['status'] = 'error';
                $json['html'] = $this->renderPartial('_error', [
                    'message' => 'Lo sentimos! <br> Se ha agotado el tiempo disponible para realizar la operación. Por favor vuelva a intentarlo.',
                    'site' => $web_payment->site->name,
                    'operation' => $web_payment->uuid
                ]);
                return $json;
            }

            $json['status'] = 'success';
            $json['message'] = 'OK';


            return $json;
        } else {
            throw new \yii\web\HttpException(400, 'The requested page does not exist.');
        }
    }


    private function NotifyPaymentTimeOut($payment) {

        $payment->changeStatus('timeout');

        if (isset($payment->request_uuid))
            $nro_operation = $payment->request_uuid;
        else
            throw new \yii\web\HttpException(400, \Yii::t('yii', 'Your request is invalid.'));

        $status = $payment->status;
      
        if (in_array($status, ['canceled', 'timeout'])) {
            $status = 'error';
        }

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $url = str_replace('www.', '', $payment->result_url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $nro_operation, 'status' => $status,'remote_uuid' => $payment->uuid]);

        if (SiteConfig::get('checkout_mode_testing', $payment->site_id)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        //execute post
        $result = curl_exec($ch);

        if ($result === false) {
            echo "Error: " . curl_error($ch);
            echo "\n";
        }

        //close connection
        curl_close($ch);

//        \Yii::$app->response->format = 'json';
        return [
            'status' => 'ok',
            'message' => '',
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Si encuentra el pago asociado al uuid, lo devuelve. Caso contrario
     * dispara una excepcion.
     * @param string $uuid
     * @return WebPayment
     * @throws CHttpException
     */
    public function loadPayment($uuid) {

        $model = WebPayment::find()->where(['uuid' => $uuid])->one();

        if ($model === null)
            throw new \yii\web\HttpException(404, 'The requested payment does not exist.');
        return $model;
    }

}
