<?php

return [
    'categories' => [
        ['name' => 'Checkout Online', 'slug' => 'checkout-online'],
    ],
    'items' => [
        [
            'attr' => 'checkout_mode_testing',
            'label' => 'Modo Testing',
            'category' => 'checkout-online',
            'type' => 'checkbox',
            'default' => false,
            'backend' => true,
        ],
        [
            'attr' => 'public_key_decidir_prisma',
            'label' => 'Decidir Prisma - Clave pública',
            'category' => 'checkout-online',
            'type' => 'text',
            'default' => '',
            'backend' => false,
        ],
        [
            'attr' => 'private_key_decidir_prisma',
            'label' => 'Decidir Prisma - Clave privada',
            'category' => 'checkout-online',
            'type' => 'text',
            'default' => '',
            'backend' => false,
        ],
        [
            'attr' => 'cybersource',
            'label' => 'Activar Cyber Source',
            'category' => 'checkout-online',
            'type' => 'checkbox',
            'default' => false,
            'backend' => true,
        ],
        [
            'attr' => 'decidir_merchant_id',
            'label' => 'Merchant ID - Decidir',
            'category' => 'checkout-online',
            'type' => 'text',
            'default' => 'decidir_agregador',
            'backend' => false,
        ],
    ],
];
