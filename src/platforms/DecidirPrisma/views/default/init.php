<script src="https://live.decidir.com/static/v2/decidir.js"></script>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \quoma\checkout\modules\siteConfig\models\SiteConfig;

/* @var $this yii\web\View */

$this->title = 'Pagar';
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payments'), 'url' => ['#']];
//$this->params['breadcrumbs'][] = $this->title;

\quoma\checkout\platforms\DecidirPrisma\assets\DecidirPrismaAssets::register($this);

?>


<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2  col-xs-12">
    <div class="row">
        <div class="col-lg-12 header padding-top-double">
            <div class="col-xs-12 text-center" id="total-time">
                <h1><?= $time_out; ?> <span class="glyphicon glyphicon-time font-size-sm"></span></h1>
                <h5>Restan <strong><span id="count-down" class="text-warning"></span></strong> minutos para completar su compra. </h5>
            </div>
        </div>
    </div>



    <div class="checkout-default-index pay-form-decidir">

        <h3>Ingrese los datos de su tarjeta</h3>
        <div class="panel-body">
            <h4 class="">
                <?php
                echo Yii::t('app', 'Monto Total');
                ?>: $ <?php echo Yii::$app->formatter->asCurrency($payment->pendingAmount, 'ARG'); ?>
            </h4>

            <form method="post" id="init-form">


                <div id="error-message"  style="display: none;">

                    <div class="alert alert-danger alert-dismissable">
                        Por favor revise los datos ingresados.
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <label>Medio de pago</label>

                        <div class="form-group">
                            <?= Html::activeDropDownList($payment,'payment_method_id',\yii\helpers\ArrayHelper::map($payments_methods, 'code', 'name'), ['class' => "form-control", 'id' => 'payment_method_id']) ?>
                        </div>

                    </div>
                    <div class="col-md-6 col-xs-12">
                        <label>Cuotas</label>
                        <div class="form-group">
                            <?= Html::activeDropDownList($payment,'installments', \yii\helpers\ArrayHelper::map($installments, 'installment_id',
                                function($data) use ($payment){

                                    $site = $payment->site;
                                    $install_amount = $site->calculateAmountInstallment($data->installment_id, $payment->pendingAmount);

                                    return $data->label . ' - '. $data->qty." de ".Yii::$app->formatter->asCurrency(round($install_amount, 2 , PHP_ROUND_HALF_EVEN));
                                }),
                                ['class' => "form-control", 'id' => 'installments']) ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="card_number">Numero de tarjeta:</label>
                            <input type="text" data-decidir="card_number"  placeholder="XXXXXXXXXXXXXXXX"  class="form-control" name="Card[card_number]" data-error_msj="Verifique su nro de Tarjeta"/>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="card_holder_name">Nombre del titular:</label>
                            <input type="text" data-decidir="card_holder_name"  placeholder="TITULAR" class="form-control" name="Card[card_holder_name]" data-error_msj="Campo obligatorio. El nombre debe ser tal cual aparece en la tarjeta"/>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="card_expiration_month">Fecha de vencimiento MM/AA:</label>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">

                                    <input type="text"  data-decidir="card_expiration_month"  placeholder="MM"  class="form-control" name="Card[card_expiration_month]" data-error_msj="Campo Obligatorio. Debe poseer solamente 2 dígitos"/>

                                </div>

                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <input type="text"  data-decidir="card_expiration_year" placeholder="AA"  class="form-control"name="Card[card_expiration_year]" data-error_msj="Campo Obligatorio. Debe poseer solamente 2 dígitos"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="security_code">Codigo de seguridad:</label>
                            <input type="password"  data-decidir="security_code"  placeholder="XXX"  class="form-control" name="Card[security_code]" data-error_msj="Campo Obligatorio. Verifique código" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="card_holder_doc_type">Tipo de documento:</label>
                            <select data-decidir="card_holder_doc_type" class="form-control" name="Card[card_holder_doc_type]">
                                <option value="dni">DNI </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">

                        <div class="form-group">
                            <label for="card_holder_doc_number">Numero de documento:</label>
                            <input type="text" data-decidir="card_holder_doc_number"  placeholder="XXXXXXXXXX"  class="form-control" name="Card[card_holder_doc_number]" data-error_msj="Campo Obligatorio. Verifique su DNI"/>
                        </div>
                    </div>
                </div>
                <?php if (SiteConfig::get('cybersource', $payment->site_id)):?>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label for="address">Domicilio:</label>
                            <input type="text"  placeholder="Falsa 123"  class="form-control" id="cs_address"/>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="address">Localidad:</label>
                            <input type="text"  placeholder="Godoy Cruz"  class="form-control" id="cs_city"/>

                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="address">Código Postal:</label>
                            <input type="text"  placeholder="5501"  class="form-control" id="cs_postal_code"/>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12">

                        <div class="form-group">
                            <label for="state">Provincia:</label>
                            <select data-decidir="card_holder_doc_type" class="form-control" id="cs_state"">
                                <option value="C">CABA</option>
                                <option value="B">Buenos Aires</option>
                                <option value="K">Catamarca</option>
                                <option value="H">Chaco</option>
                                <option value="U">Chubut</option>
                                <option value="X">Córdoba</option>
                                <option value="E">Entre Ríos</option>
                                <option value="Y">Formosa</option>
                                <option value="L">Jujuy</option>
                                <option value="F">La Pampa</option>
                                <option value="M">Mendoza</option>
                                <option value="N">Misiones</option>
                                <option value="Q">Neuquén</option>
                                <option value="R">Río Negro</option>
                                <option value="A">Salta</option>
                                <option value="J">San Juan</option>
                                <option value="D">San Luis</option>
                                <option value="Z">Santa Cruz</option>
                                <option value="S">Santa Fe</option>
                                <option value="G">Santiago del Estero</option>
                                <option value="V">Tierra del Fuego</option>
                                <option value="T">Tucumán</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-12">
                        <div class="form-group">
                            <label for="address">Teléfono:</label>
                            <input type="text"  placeholder="5501"  class="form-control" id="cs_phone"/>

                        </div>
                    </div>
                </div>
                <?php endif;?>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="button" id="btn-send-form" class="button btn btn-primary send-form">Continuar</button>
                    </div>
                </div>

                <?php $unique= $payment->web_payment_id; ?>

                <input type="hidden" data-decidir="device_unique_identifier" value="<?php echo $unique?>">

                <p style="background:url(https://h.online-metrix.net/fp/clear.png?org_id=1snn5n9w&amp;session_id=<?php echo SiteConfig::get('decidir_merchant_id', $payment->site_id)?><?php echo $unique?>&amp;m=1)"></p>
                <img src="https://h.online-metrix.net/fp/clear.png?org_id=1snn5n9w&session_id=<?php echo SiteConfig::get('decidir_merchant_id', $payment->site_id)?><?php echo $unique?>&m=2" alt="" >

                <script src="https://h.online-metrix.net/fp/check.js?org_id=1snn5n9w&session_id=<?php echo SiteConfig::get('decidir_merchant_id', $payment->site_id)?><?php echo $unique?>" type="text/javascript"></script>

                <object type="application/x-shockwave-flash" data="https://h.online-metrix.net/fp/fp.swf?org_id=1snn5n9w&session_id=<?php echo SiteConfig::get('decidir_merchant_id', $payment->site_id)?><?php echo $unique?>" width="1" height="1" id="thm_fp">
                    <param name="movie" value="https://h.online-metrix.net/fp/fp.swf?org_id=1snn5n9w&session_id=<?php echo SiteConfig::get('decidir_merchant_id', $payment->site_id)?><?php echo $unique?>" />
                </object>
            </form>
            <?php if (!YII_ENV_PROD) { ?>
                <div id="request_uuid"><?php echo $payment->request_uuid; ?></div>
                <div id="cobros_url"><?php echo yii\helpers\Url::to(['/'], true); ?></div>
            <?php } ?>
        </div>
    </div>

</div>

<div id="payment-confirm">

</div>

<?php
$url = \yii\helpers\Url::home('').'checkout_online/DecidirPrisma/';
$web_payment_id = $payment->web_payment_id ;
$web_payment_uuid = $payment->uuid ;
$public_key_decidir_prisma = SiteConfig::get('public_key_decidir_prisma', $site->site_id);
$decidir_url = (YII_ENV == 'test' or SiteConfig::get('checkout_mode_testing', $site->site_id))? "https://developers.decidir.com/api/v2" :"https://live.decidir.com/api/v2";
$timeout_confim_payment = ($time_out-1);

$this->registerJs("FormPay.init('$url','$web_payment_id','$web_payment_uuid','$public_key_decidir_prisma','$decidir_url','$timeout_confim_payment','".SiteConfig::get('cybersource', $payment->site_id)."','".$unique."')");

$this->registerJs('FormPay.send();');
$this->registerJs("FormPay.startTime();");
?>

