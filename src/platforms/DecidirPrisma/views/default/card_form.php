<script src="https://live.decidir.com/static/v1/decidir.js"></script>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Payment */

$this->title = quoma\checkout\CheckoutModule::t('Method of Payment');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payments'), 'url' => ['#']];
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="checkout-default-index">

    <h3>Seleccione un <?= quoma\checkout\CheckoutModule::t('Method of Payment') ?></h3>
    <div class="panel-body">
        <form method="post" id="formulario" >
            <div class="form-group">
                <label for="card_number">Numero de tarjeta:</label>
                <input type="text" data-decidir="card_number"  placeholder="XXXXXXXXXXXXXXXX" value="4507990000004905"/>
            </div>
            
            <div class="form-group">
                <label for="security_code">site:</label>
                <input type="text"  data-decidir="security_code"  placeholder="XXX" value="755" />
            </div>

            <div class="form-group">
                <label for="card_expiration_month">Mes de vencimiento:</label>
                <input type="text"  data-decidir="card_expiration_month"  placeholder="MM" value="08"/>
            </div>

            <div class="form-group">
                <label for="card_expiration_year">Año de vencimiento:</label>
                <input type="text"  data-decidir="card_expiration_year" placeholder="AA" value="18"/>
            </div>

            <div class="form-group">
                <label for="card_holder_name">Nombre del titular:</label>
                <input type="text" data-decidir="card_holder_name"  placeholder="TITULAR" value="Tarjeta Visa"/>
            </div>

            <div class="form-group">
                <label for="card_holder_doc_type">Tipo de documento:</label>
                <select data-decidir="card_holder_doc_type">
                    <option value="dni">DNI</option>
                </select>
            </div>

            <div class="form-group">
                <label for="card_holder_doc_type">Numero de documento:</label>
                <input type="text"data-decidir="card_holder_doc_number"  placeholder="XXXXXXXXXX" value="27859328"/>
            </div>

            <button id='send' type="button"/>Generar Token
        </form>
    </div>
</div>


<script>
    var DecidirPrisma = new function () {

        this.init = function () {
            
        };

        this.send = function (e) {
            $('#send').on('click', function (e) {
                console.log('estoy en send');
                e.preventDefault();
                
                sendForm();
            });
        };

        //funcion de invocacion con sdk
        function sendForm() {
            var decidir = getDecidirPrisma();
            console.log('estoy en sendForm');
            var form = document.querySelector('#formulario');
            
            decidir.createToken(form, sdkResponseHandler); //formulario y callback
//            return false;
        }


        function getDecidirPrisma() {
           
            const publicApiKey = "f667f689571541a8addf873dc9200679";
            const urlSandbox = "https://developers.decidir.com/api/v1";
            const decidir = new Decidir(urlSandbox,true);
            decidir.setPublishableKey(publicApiKey);
            decidir.setTimeout(0); //se configura sin timeout
            return decidir;
        }

        //funcion para manejar la respuesta
        function sdkResponseHandler(status, response) {
            console.log('estoy en sdkResponseHandler');
            console.log(status);
            console.log(response);
            if (status !== 200 && status !== 201) {
                //Manejo de error donde response = {error: [{error:"tipo de error", param:"parametro con error"},...]}
                //...codigo...

            } else {
                //Manejo de respuesta donde response = {token: "99ab0740-4ef9-4b38-bdf9-c4c963459b22"}
                //..codigo...
                $.ajax({
                    url: '<?= yii\helpers\Url::toRoute(['default/init-pay']); ?>',
                    data: {
                        token : response.id,
                        payment_id:<?= $payment->web_payment_id?>
                    },
                    dataType: 'json',
                    type: 'post'
                }).done(function (json) {
                    if (json.status === 'success') {
                        $('#init-form').submit();
                    } else {
                        $('.checkout-default-index').html(json.html);
                        $('#total-time').html("");
                    }
                });
            }
        }
    };


</script>

<?php $this->registerJs('DecidirPrisma.send();') ?>

