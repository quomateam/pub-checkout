<div class="container checkout-default-index">

    <div class="row pay-form-decidir go-back-pay">
        <div class="col-xs-12">
            <h3>Revise los datos del Pago</h3>
            <!-- <a href="">Modificar</a> -->
        </div>
    </div>
    <hr>
    <div class="row pay-form-decidir">
        <div class="col-sm-6 col-xs-12 text-center">
            <h4> <?= $payment->installments?> cuotas de <?= Yii::$app->formatter->asCurrency(round($payment->net_amount/$payment->installments,2, PHP_ROUND_HALF_EVEN))?> c/u</h4>

        </div>
        <div class="col-sm-6 col-xs-12 text-center">
            <h4>Monto Total: <?= Yii::$app->formatter->asCurrency($payment->net_amount)?></h4>
        </div>
    </div>
    <hr>
    <div class="row pay-form-decidir margin-up-sm">
        <div class="col-sm-6 col-xs-12 acept-terms">
            <p>Al pagar, afirma que es mayor de edad y acepta los <a  target="_blank" href="<?= yii\helpers\Url::to(['/site/page', 'view' => 'terms']) ?>">Términos y Condiciones</a></p>
        </div>
        <div class="col-sm-6 col-xs-12 text-right">
            <button id="pay-confirm" type="button" class="button btn btn-primary">Pagar</button>
        </div>
        <br>

    </div>

</div>


<!--<div class="row">-->
<!--    <div class="col-xs-12 text-center">            -->
<!--        <h5>Dispone <strong><span id="count-down" class="text-warning"></span></strong> de minutos para pagar. </h5>-->
<!--    </div>-->
<!--</div>-->


<?php
$this->registerJs("FormPay.confirm();");