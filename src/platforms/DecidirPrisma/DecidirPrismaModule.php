<?php

namespace quoma\checkout\platforms\DecidirPrisma;

class DecidirPrismaModule extends \yii\base\Module
{
    public $controllerNamespace = 'quoma\checkout\platforms\DecidirPrisma\controllers';

    public function init()
    {
        parent::init();

        \Yii::setAlias('@platform', __DIR__);
    }
}
