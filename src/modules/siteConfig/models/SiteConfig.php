<?php

namespace quoma\checkout\modules\siteConfig\models;

use quoma\checkout\models\Site;
use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $config_id
 * @property integer $site_config_item_id
 * @property string $value
 *
 * @property SiteConfigItem $item
 */
class SiteConfig extends \quoma\core\db\ActiveRecord
{

    public $_site;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_config';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if (empty($this->item)) {
            return [];
        }

        $rules = [];
        foreach ($this->item->rules as $rule) {
            $rules[] = $rule->getLine();
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_config_id' => Yii::t('app', 'Config ID'),
            'site_config_item_id' => Yii::t('app', 'Item ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(SiteConfigItem::className(), ['site_config_item_id' => 'site_config_item_id']);
    }


    /**
     * Devuelve un objeto Config para un item de configuracion dado por $attr
     * @param string $attr
     * @return SiteConfig
     * @throws \yii\web\HttpException
     */
    public static function getConfig($attr, $site_id) {

        $item = static::findItem($attr, $site_id);
        
        if($item === null){
            $site = Site::findOne($site_id);
            static::initialize($site);

            //Luego de inicializar, si no encontramos el item, es porque no esta declarado
            $item = SiteConfigItem::find()->where(['attr' => $attr,'site_id' => $site_id])->one();

            if($item === null){
                throw new \yii\web\HttpException(500, 'Config item not found. Please, check the init file.');
            }
        }

        if ($item->multiple) {
            $config = self::find()->where(['site_config_item_id' => $item->site_config_item_id])->all();
        } else {
            $config = self::find()->where(['site_config_item_id' => $item->site_config_item_id])->one();
        }

        if (empty($config)) {
            $config = new self;
            $config->site_config_item_id = $item->site_config_item_id;
            $config->value = $item->default;
            $config->save();

            if ($item->multiple) {
                $config = [$config];
            }
        }

        return $config;
    }
    
    public static function get($attr, $site_id) {

        $config = self::getConfig($attr, $site_id);

        if (is_array($config)) {
            return \yii\helpers\ArrayHelper::map($config, 'site_config_id', 'value');
        }

        return $config->value;
    }

    public static function setValue($attr, $value, $site_id, $validate = true) {

        $item = static::findItem($attr, $site_id);
        if($item === null){
            throw new \yii\web\HttpException(404, Yii::t('app','Configuration item not found: {attr}', ['attr' => $attr]));
        }

        $config = self::find()->where(['site_config_item_id' => $item->site_config_item_id])->one();

        if (empty($config)) {
            $config = new SiteConfig;
            $config->site_config_item_id = $item->site_config_item_id;
            $config->site_id = $site_id;
        }

        $config->value = $value;

        $config->save($validate);

        return $config;
    }

    public function getAttr() {
        return $this->item->attr;
    }

    public function getLabel() {
        return $this->item->label;
    }

    public function getType() {
        return $this->item->type;
    }

    public function getDescription() {
        return $this->item->description;
    }
    
    protected static function findItem($attr, $site_id)
    {
        return SiteConfigItem::find()->where(['attr' => $attr, 'site_id' => $site_id])->one();
    }


    public static function initialize($site)
    {
        $file = $site->platform->platformPath.DIRECTORY_SEPARATOR.'init'.DIRECTORY_SEPARATOR.'config.php';

        if(file_exists($file)){

            $data = require($file);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                //Categorias
                if($data['categories']){
                    $categoriesCount = static::initializeCategories($site, $data['categories']);
                }
                //Items de configuracion
                if($data['items']){
                    $itemsCount = static::initializeItems($site, $data['items']);
                }
                $transaction->commit();

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }


        }

        return true;
    }

    /**
     * Inicializa las categorias
     * @param array $categories
     * @return int
     */
    protected static function initializeCategories($site, $categories)
    {
        $count = 0;

        foreach($categories as $category){

            //Validamos datos
            static::parseCategory($category);

            if(!SiteConfigCategory::find()->where(['slug' => $category['slug'], 'site_id' => $site->site_id])->exists()){
                $newCategory = new SiteConfigCategory();
                $newCategory->load($category,'');

                $newCategory->site_id = $site->site_id;

                if($newCategory->save()){
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * Inicializa las categorias
     * @param array $categories
     * @return int
     */
    protected static function initializeItems($site, $items)
    {
        $count = 0;

        foreach($items as $item){

            //Validamos datos
            static::parseItem($item);

            $category = SiteConfigCategory::find()->where(['slug' => $item['category'], 'site_id' => $site->site_id])->one();

            if($category === null){
                throw new \yii\web\HttpException(500, 'Category "'.$item['category'].'" not found in item '.$item['attr'].'. Please, check config-init file.');
            }

            if(!SiteConfigItem::find()->where(['attr' => $item['attr'], 'site_config_category_id' => $category->site_config_category_id])->exists()){

                //El valor debe ser convertido a string para almacenar
                $item['default'] = (string)$item['default'];

                $newItem = new SiteConfigItem();
                $newItem->load($item,'');

                $newItem->site_config_category_id = $category->site_config_category_id;
                $newItem->site_id = $site->site_id;

                if($newItem->save()){
                    $count++;

                    //Reglas de validacion
                    if(isset($item['rules']) && is_array($item['rules'])){
                        static::initializeRules($newItem, $item['rules']);
                    }
                }else{
                    var_dump($newItem->getErrors());die;
                }
            }
        }

        return $count;
    }

    /**
     * Inicializa las reglas de validacion para un item
     * @param array $categories
     * @return int
     */
    protected static function initializeRules($item, $rules)
    {
        $count = 0;

        foreach($rules as $rule){

            //Validamos datos
            static::parseRules($rule);

            foreach($rules as $rule){
                $newRule = new SiteConfigRule;
                $newRule->load($rule,'');
                $newRule->site_config_item_id = $item->site_config_item_id;
                if($newRule->save()){
                    $count++;
                }
            }

        }

        return $count;
    }


    /**
     * Valida las categorias
     * @param array $data
     * @return boolean
     * @throws \yii\web\HttpException
     */
    public static function parseCategory($data)
    {
        $check = static::parseArray($data, [
            //Req:
            'name',
            'slug'
        ], [
            //Valid:
            'name',
            'slug',
            'backend'
        ]);

        if($check === 0){
            return true;
        }

        //Identificador para el array de inicializacion de categoria
        $id = isset($data['slug']) ? $data['slug'] : print_r($data, true);

        if($check === 1){
            throw new \yii\web\HttpException(500, "Bad configuration. Please, check required fields for category '$id' in config-init file.");
        }

        if($check === 2){
            throw new \yii\web\HttpException(500, "Bad configuration. Please, check category '$id' in config-init file.");
        }
    }

    /**
     * Valida las categorias
     * @param array $data
     * @return boolean
     * @throws \yii\web\HttpException
     */
    public static function parseItem($data)
    {
        $check = static::parseArray($data, [
            //Req:
            'label',
            'attr',
            'type',
            'default',
            'category'
        ], [
            //Valid:
            'label',
            'attr',
            'type',
            'default',
            'category',
            'backend',
            'description',
            'rules'
        ]);

        if($check === 0){
            return true;
        }

        //Identificador para el array de inicializacion de categoria
        $id = isset($data['attr']) ? $data['attr'] : print_r($data, true);

        if($check === 1){
            throw new \yii\web\HttpException(500, "Bad configuration. Please, check required fields for item $id in config-init file.");
        }

        if($check === 2){
            throw new \yii\web\HttpException(500, "Bad configuration. Please, check item $id in config-init file.");
        }
    }

    /**
     * Valida las categorias
     * @param array $data
     * @return boolean
     * @throws \yii\web\HttpException
     */
    public static function parseRules($data)
    {
        if(!is_array($data)){
            throw new \yii\web\HttpException(500, "Bad configuration. Please, check that all rules are arrays in init/config file.");
        }

        $check = static::parseArray($data, [
            //Req:
            'validator',
        ], [
            //Valid:
            'message',
            'max',
            'min',
            'pattern',
            'format',
            'validator'
        ]);

        //Identificador para el array de inicializacion de categoria
        $id = isset($data['attr']) ? $data['attr'] : print_r($data, true);

        if($check === 1){
            throw new \yii\web\HttpException(500, "Bad configuration. Please, check required fields for rule $id in init/ file.");
        }

        if($check === 2){
            throw new \yii\web\HttpException(500, "Bad configuration. Please, check rule $id in init/config file.");
        }

        //Verificamos que el validador exista
        $validators = SiteConfigRule::getValidatorsList();
        if(!in_array($data['validator'], $validators)){
            throw new \yii\web\HttpException(500, "Bad configuration. Please, check validator field for rule $id in init/config file.");
        }

        return true;
    }

    /**
     * Devuelve 1 si faltan datos requeridos; devuelve 2 si hay datos con nombre
     * incorrecto. Devuelve 0 si todo esta bien.
     * @param array $data
     * @param array $required
     * @param array $validKeys
     * @return int
     */
    protected static function parseArray($data, $required, $validKeys)
    {

        //Validamos datos requeridos
        if(count(array_intersect_key(array_flip($required), $data)) !== count($required)) {
            return 1;
        }

        //Validamos todos los keys
        foreach($data as $key => $value){
            if(!in_array($key, $validKeys)){
                return 2;
            }
        }

        return 0;
    }

    public static function getSiteConfig($attr, $site_id) {

        $item = SiteConfigItem::find()->where(['attr' => $attr, 'site_id' => $site_id])->one();

        if ($item === null) {
            throw new \yii\web\HttpException(404, 'Configuration item not found: ' . $attr);
        }

        if ($item->multiple) {
            $config = self::find()->where(['site_config_item_id' => $item->site_config_item_id])->all();
        } else {
            $config = self::find()->where(['site_config_item_id' => $item->site_config_item_id])->one();
        }

        if (empty($config)) {
            $config = new self;
            $config->site_config_item_id = $item->site_config_item_id;
            $config->value = $item->default;
            $config->save();

            if ($item->multiple) {
                $config = [$config];
            }
        }

        return $config;
    }

    public function getSite(){
        return $this->item->site;
    }

}
