<?php

namespace quoma\checkout\modules\siteConfig\models;

use Yii;
use quoma\checkout\modules\siteConfig\SiteConfigModule;

/**
 * This is the model class for table "category".
 *
 * @property integer $site_config_category_id
 * @property string $name
 * @property string $status
 * @property boolean $backend
 * @property string $slug
 *
 * @property SiteConfigItem[] $items
 */
class SiteConfigCategory extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_config_category';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','slug'], 'required'],
            [['name','slug'], 'string', 'max' => 45],
            [['backend'], 'boolean'],
            [['backend'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_config_category_id' => Yii::t('app', 'Category ID'),
            'name' => Yii::t('app', 'Name'),
            'backend' => Yii::t('app', 'Superadmin only'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(SiteConfigItem::className(), ['site_config_category_id' => 'site_config_category_id']);
    }

    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: Items.
     */
    protected function unlinkWeakRelations(){
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Vuelve los valores de todas las configuraciones a sus valores por defecto.
     * Si el usuario no es backend, solo modifica las configuraciones que no tengan
     * el requisito de usuario backend.
     */
    public function resetValues()
    {

        $items = $this->getItems();

        if(Yii::$app->id != 'app-backend'){
            $items->andWhere(['backend' => 0]);
        }

        foreach($items->all() as $item){
            SiteConfig::setValue($item->attr, $item->default, false);
        }

    }

}
