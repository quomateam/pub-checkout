<?php

namespace quoma\checkout\modules\siteConfig\models;

use quoma\checkout\models\Site;
use Yii;
use quoma\checkout\modules\siteConfig\SiteConfigModule;

/**
 * This is the model class for table "item".
 *
 * @property integer $site_config_item_id
 * @property string $attr
 * @property string $type
 * @property string $default
 * @property string $label
 * @property string $description
 * @property integer $multiple
 * @property integer $site_config_category_id
 *
 * @property SiteConfig[] $configs
 * @property SiteConfigCategory $categoryCategory
 */
class SiteConfigItem extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_config_item';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }
    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attr', 'site_config_category_id'], 'required'],
            [['multiple', 'site_config_category_id'], 'integer'],
            [['label'], 'string', 'max' => 140],
            [['attr', 'type'], 'string', 'max' => 45],
            [['default', 'description'], 'string', 'max' => 255],
            [['attr'], 'unique', 'targetAttribute' => 'site_id'],
            [['backend'], 'boolean'],
            [['backend','multiple'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_config_item_id' => Yii::t('app', 'Item ID'),
            'attr' => Yii::t('app', 'Attr'),
            'type' => Yii::t('app', 'Type'),
            'default' => Yii::t('app', 'Default'),
            'label' => Yii::t('app', 'Label'),
            'description' => Yii::t('app', 'Description'),
            'multiple' => Yii::t('app', 'Multiple'),
            'site_config_category_id' => Yii::t('app', 'SiteConfigCategory'),
            'backend' => Yii::t('app', 'Superadmin only'),
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfigs()
    {
        return $this->hasMany(SiteConfig::className(), ['site_config_item_id' => 'site_config_item_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRules()
    {
        return $this->hasMany(SiteConfigRule::className(), ['site_config_item_id' => 'site_config_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(SiteConfigCategory::className(), ['site_config_category_id' => 'site_config_category_id']);
    }
             
    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }

    public function getSite(){
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: Configs, CategoryCategory.
     */
    protected function unlinkWeakRelations(){
        
        $this->unlinkAll('configs', true);
        
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }
    
    public static function types()
    {
        return [
            'textInput' => 'Text',
            'textarea' => 'Textarea',
            'checkbox' => 'Checkbox',
            'passwordInput' => 'Password'
        ];
    }
    
    public static function getItems($category)
    {
        
        $query = SiteConfigItem::find();
        
        $query->where(['site_config_category_id' => $category->site_config_category_id]);

        return $query->all();
        
    }


}
