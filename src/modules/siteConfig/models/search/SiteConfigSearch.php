<?php

namespace quoma\checkout\modules\SiteConfig\models\search;

use quoma\checkout\modules\siteConfig\models\SiteConfig;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ConfigSearch represents the model behind the search form about `common\modules\SiteConfig\models\SiteConfig`.
 */
class SiteConfigSearch extends SiteConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['config_id', 'item_id'], 'integer'],
            [['value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteConfig::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'config_id' => $this->config_id,
            'item_id' => $this->site_config_item_id,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}
