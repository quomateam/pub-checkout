<?php

namespace quoma\checkout\modules\siteConfig\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\checkout\modules\siteConfig\models\search\SiteConfigCategory;

/**
 * CategorySearch represents the model behind the search form about `common\modules\siteConfig\models\SiteConfigCategory`.
 */
class SiteConfigCategorySearch extends SiteConfigCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_config_category_id'], 'integer'],
            [['name', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SiteConfigCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'site_config_category_id' => $this->site_config_category_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
