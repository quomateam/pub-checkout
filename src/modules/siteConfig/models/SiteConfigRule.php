<?php

namespace quoma\checkout\modules\siteConfig\models;

use Yii;

/**
 * This is the model class for table "rule".
 *
 * @property integer $site_config_rule_id
 * @property string $message
 * @property double $max
 * @property double $min
 * @property string $pattern
 * @property string $format
 * @property string $targetAttribute
 * @property string $targetClass
 * @property integer $site_config_item_id
 * @property string $validator
 *
 * @property SiteConfigItem $item
 */
class SiteConfigRule extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_config_rule';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }
    
    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_config_item_id', 'validator'], 'required'],
            [['site_config_item_id'], 'integer'],
            [['max', 'min'], 'number'],
            [['message', 'pattern', 'targetClass'], 'string', 'max' => 255],
            [['format', 'targetAttribute', 'validator'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_config_rule_id' => Yii::t('app', 'Rule ID'),
            'message' => Yii::t('app', 'Message'),
            'max' => Yii::t('app', 'Max value'),
            'min' => Yii::t('app', 'Min value'),
            'pattern' => Yii::t('app', 'Pattern'),
            'format' => Yii::t('app', 'Format'),
            'targetAttribute' => Yii::t('app', 'Target Attribute'),
            'targetClass' => Yii::t('app', 'Target Class'),
            'site_config_item_id' => Yii::t('app', 'Item ID'),
            'item' => Yii::t('app', 'Item'),
            'validator' => Yii::t('app', 'Validator'),
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(SiteConfigItem::className(), ['site_config_item_id' => 'site_config_item_id']);
    }
         
    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: SiteConfigItem.
     */
    protected function unlinkWeakRelations(){
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }
    
    public static function getValidatorsList() 
    {
        
        $builtInValidators = \yii\validators\Validator::$builtInValidators;
        
        $validators = array_diff_key($builtInValidators, array_flip([
            'default',
            'safe',
            'filter',
            'file',
            'image',
            'captcha',
            'compare',
            'each',
            'in'
        ]));
        
        $keys = array_keys($validators);
        return array_combine($keys, $keys);
        
    }
    
    public static function getAvaibleAttributes()
    {
        $avaibleAttrs = [
            'max',
            'min',
            'pattern',
            'format',
            'targetAttribute',
            'targetClass',
        ];
        
        return $avaibleAttrs;
    }
    
    public static function getValidatorAttributes($validator)
    {
        $builtInValidators = \yii\validators\Validator::$builtInValidators;
        
        $avaibleAttrs = static::getAvaibleAttributes();
        
        $attrs = [];
        
        if (isset($builtInValidators[$validator])) {
            
            $type = $builtInValidators[$validator];
            
            if(is_array($type)){
                $type = $type['class'];
            }
            
            $reflex = new \ReflectionClass($type);
            
            foreach($reflex->getProperties() as $property){
                if(in_array($property->name, $avaibleAttrs)){
                    $attrs[] = $property->name;
                }
            }            
            
        }
        
        return $attrs;
        
    }

    public function getLine()
    {
        
        $rule = ['value', $this->validator];
        
        $avaibleAttrs = static::getAvaibleAttributes();
        
        foreach ($avaibleAttrs as $attr){
            if(!empty($this->$attr)){
                $rule[$attr] = $this->$attr;
            }
        }
        
        return $rule;
        
    }
    
}
