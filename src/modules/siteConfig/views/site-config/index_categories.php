<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\modules\config\ConfigModule;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\config\models\search\ConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Configuration');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="website-config-index box-body">


    
        <?php $form = ActiveForm::begin(); ?>
            <?php 
            foreach($models as $category => $model):?>

                <div class="page-header">
                    <h1><?= Html::encode($this->title) ?>: <?= $category?></h1>
                </div>

                <?= $this->render('_input', ['model' => $model]);?>

            <?php endforeach; ?>

            <hr/>

            <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>


        <?php ActiveForm::end(); ?>
        
    </div>
    
</div>
