<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\modules\config\ConfigModule;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\config\models\search\ConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Configuration');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="website-config-index box-body">

        <div class="page-header">
            <h1><?= Html::encode($this->title) ?>: <?= $category->name ?></h1>
        </div>
    
        <?php $form = ActiveForm::begin(); ?>
            <?php

            foreach($models as $model){
                echo $this->render('_input', ['model' => $model]);
            }
            ?>

            <hr/>

            <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>

<!--            Html::a(Yii::t('app', 'Reset to default'), ['reset', 'category' => $category->site_config_category_id], ['data' => [-->
<!--                'confirm' => Yii::t('app', 'Are you sure you want to reset all?'),-->
<!--                'method' => 'post',-->
<!--            ], 'class' => 'btn btn-warning' ]) -->

        <?php ActiveForm::end(); ?>
        
    </div>
    
</div>
