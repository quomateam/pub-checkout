<?php
use yii\helpers\Html;
use common\modules\config\ConfigModule;
?>

<div class="form-group<?php if($model->hasErrors()) echo ' has-error' ?>">
    <?php if($model->type == 'checkbox'): ?>
        <?= Html::checkbox($model->attr, $model->value, ['uncheck' => 0, 'label' => $model->label]); ?>
    <?php else: ?>
        <?= Html::label($model->label, $model->attr, ['class' => 'control-label']); ?>
        <?= Html::input($model->type, $model->attr, $model->value, ['class' => 'form-control']); ?>
    <?php endif; ?>
    <?php if($model->description): ?>
    
        <div class="help-block"><?= $model->description ?></div>
    
    <?php endif; ?>
    <?= Html::error($model, 'value', ['class' => 'help-block']); ?>
</div>