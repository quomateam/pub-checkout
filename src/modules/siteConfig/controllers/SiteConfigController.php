<?php

namespace  quoma\checkout\modules\siteConfig\controllers;

use quoma\checkout\CheckoutModule;
use quoma\checkout\models\Site;
use Yii;
use quoma\checkout\modules\siteConfig\models\SiteConfig;
use yii\web\Controller;;
use quoma\checkout\modules\siteConfig\models\SiteConfigCategory;
use quoma\checkout\modules\siteConfig\models\SiteConfigItem;

/**
 * ConfigController implements the CRUD actions for SiteConfig model.
 */
class SiteConfigController extends Controller
{
    public function behaviors()
    {
        return parent::behaviors();
    }


    /**
     * Lists all SiteConfig models.
     * @return mixed
     */
    public function actionIndex($id,$category = 'checkout-online')
    {
        $site = Site::findOne($id);
        SiteConfig::initialize($site);

        $category = SiteConfigCategory::find()->where(['slug' => $category, 'site_id' => $id])->one();
        if ($category === null) {
            throw new \yii\web\HttpException(404, 'Category not found.');
        }

        if ($data = Yii::$app->request->post()) {

            $configs = [];

            //Si ocurre algun error, esto es true
            $errors = false;

            //Quitamos los datos que no se deben guardar (no hay peligro)
            unset($data['_csrf']);
            unset($data['_csrf-backend']);
            unset($data['_csrf-frontend']);
            foreach ($data as $attr => $value) {
                $config = SiteConfig::setValue($attr, $value, $site->site_id);
                //Vemos si hay errores
                if ($config->hasErrors()) {
                    $errors = true;
                    Yii::$app->session->setFlash(
                        'error',
                        Yii::t('app', 'The value for "{label}" has not been saved. Error: {error}',
                            [
                                'label' => $config->label,
                                'error' => implode('. ', $config->getErrors('value'))
                            ]
                        )
                    );
                }
                $configs[] = $config;
            }

            if ($errors == false) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Your changes has been saved.'));
            }

            return $this->redirect(['/'.Yii::$app->controller->module->module->id.'/site/view',
                'id' => $site->site_id,
            ]);
        }

        $configs = [];
        $items = SiteConfigItem::getItems($category);
        foreach($items as $item){
            $configs[] = SiteConfig::getConfig($item->attr, $site->site_id);
        }

        return $this->render('index', [
            'models' => $configs,
            'category' => $category,
        ]);
    }
    
    /**
     * Lists all SiteConfig models.
     * @return mixed
     */
//    public function actionReset($category)
//    {
//        $category =SiteConfigCategory::find()->where(['site_config_category_id' => $category])->one();
//        if($category === null){
//            throw new \yii\web\HttpException(404, 'Category not found.');
//        }
//
//        $category->resetValues();
//
//        \Yii::$app->getSession()->setFlash('success', Yii::t('app', 'All settings has been resetted to their default values.'));
//
//        $this->redirect(['index', 'category' => $category->site_config_category_id]);
//    }

}
