<?php

namespace quoma\checkout\modules\api;

/**
 * api module definition class
 */
class ApiModule extends \yii\base\Module
{

    /**
     * @inheritdoc
     */
    public function init()
    {

        $this->modules =  [
            'v1' => [
                'class' => '\quoma\checkout\modules\api\v1\V1Module',
            ]
        ];

        return parent::init();

        // custom initialization code goes here
    }
}
