<?php

namespace quoma\checkout\modules\api\v1;

use yii\web\Response;

/**
 * v1 module definition class
 */
class V1Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'quoma\checkout\modules\api\v1\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if(!\Yii::$app->request->isConsoleRequest){
            \Yii::$app->response->format = Response::FORMAT_JSON;
        }

    }
}
