<?php
namespace quoma\checkout\modules\api\v1\models;
/**
 * Created by PhpStorm.
 * User: gabriela
 * Date: 27/04/18
 * Time: 09:42
 */

class WebPayment extends \quoma\checkout\models\WebPayment
{
    public static function searchPayment($uuid){

        $web_payment = WebPayment::find()->where(['uuid' => $uuid])->one();

        if (!$web_payment) {
            return [
                'status' => 'warning',
                'payment' => NULL,
                'message' => 'No existen pagos para el uuid pasado.'
            ];
        }
        //si el pago ya esta pagado
        if($web_payment->status == 'paid'){
            return [
                'status' => 'success',
                'payment' => $web_payment,
            ];
        }
        //cargo la plataforma
        $platform = $web_payment->site->loadPlatform();
        $result = $platform->checkWebPayment($web_payment);
        //chequeo el estado del pago con la plataforma
        if($result['status'] == 'success'){
            return [
                'status' => 'success',
                'payment' => $web_payment,
            ];
        }
        //No se encontró el pago en la plataforma.
        if($result['status'] == 'warning'){
            return [
                'status' => 'warning',
                'payment' => NULL
            ];
        }

        //status:error,message:mensaje-de-error
        return $result;
    }

    public static function cancelWebPayment($uuid)
    {
        $web_payment = WebPayment::find()->where(['uuid' => $uuid])->one();

        if (!$web_payment) {
            return [
                'status' => 'error',
                'message' => 'No existen pagos para el uuid pasado.'
            ];
        }

        //cargo la plataforma

        $result = $web_payment->cancelPayment();

        if($result) {
            return [
                'status' => 'success',
                'result' => $web_payment,
            ];
        }

        return [
            'status' => $web_payment->status,
            'result' => $web_payment,
        ];
    }

}