<?php
/**
 * Created by PhpStorm.
 * User: gabriela
 * Date: 04/01/18
 * Time: 14:23
 */

namespace quoma\checkout\modules\api\v1\controllers;

use quoma\checkout\modules\api\v1\models\WebPayment;

class WebPaymentController extends \yii\rest\Controller
{

    /**
     * Devuelve el pago correspondiente al request_uuid.
     * En caso de que el estado del pago no sea paid, consulta el estado del pago a Mercado Pago.
     * @return array
     */
    public function actionSearchPayment(){
        $result = [];

        if(!\Yii::$app->request->post('external_uuid')){
            return [
                'status' => 'error',
                'message' => 'Error en los datos enviados'
            ];
        }
        \Yii::info(\Yii::$app->request->post('external_uuid'), 'uuid-log');

        $web_payments = WebPayment::find()->where(['request_uuid' => \Yii::$app->request->post('external_uuid')])->all();

        if(!$web_payments){
            return [
                'status' => 'error',
                'message' => 'No se encontraron pagos'
            ];
        }

        foreach ($web_payments as $web_payment){
            $result[] = WebPayment::searchPayment($web_payment->uuid);
        }

        return  $result;

    }

    /**
     * Cancela un pago en el modulo checkout
     * @return array
     */
    public function actionCancelPayment(){

        $result = [];


        if(!\Yii::$app->request->post('external_uuid')){
            return [
                'status' => 'error',
                'message' => 'Error en los datos enviados'
            ];
        }

        $web_payments = WebPayment::find()->where(['request_uuid' => \Yii::$app->request->post('external_uuid')])->all();

        if(!$web_payments){
            return [
                'status' => 'error',
                'message' => 'No se encontraron pagos'
            ];
        }

        foreach ($web_payments as $web_payment){
            $result[] =  WebPayment::cancelWebPayment($web_payment->uuid);
        }

        return  $result;
    }

}