<?php

namespace quoma\checkout;

use quoma\core\menu\Menu;
use quoma\core\module\QuomaModule;

class CheckoutModule extends QuomaModule {

    public $controllerNamespace = 'quoma\checkout\controllers';

    public $defaultControllerBehaviors= null;

    public function init() {
        
        parent::init();

        $this->registerTranslations();


        $this->modules = [
            'Decidir' => [
                // you should consider using a shorter namespace here!
                'class' => 'quoma\checkout\platforms\Decidir\DecidirModule',
            ],
            'MercadoPago' => [
                // you should consider using a shorter namespace here!
                'class' => 'quoma\checkout\platforms\MercadoPago\MercadoPagoModule',
            ],
            'DecidirPrisma' => [
                // you should consider using a shorter namespace here!
                'class' => 'quoma\checkout\platforms\DecidirPrisma\DecidirPrismaModule',
            ],
            'siteConfig' => [
                // you should consider using a shorter namespace here!
                'class' => 'quoma\checkout\modules\siteConfig\SiteConfigModule',
            ],
            'api' => [
                // you should consider using a shorter namespace here!
                'class' => 'quoma\checkout\modules\api\ApiModule',
            ],

        ];
        \Yii::setAlias('@quoma', dirname(dirname(__DIR__)) . '/vendor/quoma');
        \Yii::setAlias('@checkoutOnline', __DIR__);
    }
    

    public function getMenu(Menu $menu, $module_name = 'checkout_online')
    {
        $_menu = (new Menu(Menu::MENU_TYPE_ROOT))
            ->setName('checkout_online')
            ->setLabel(self::t('Checkout Online'))
            ->setSubItems([
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Sites'))->setUrl(["/$module_name/site/index"]),
                (new Menu(Menu::MENU_TYPE_DIVIDER)),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Platforms'))->setUrl(["/$module_name/platform/index"]),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Payment Methods'))->setUrl(["/$module_name/payment-method/index"]),
                (new Menu(Menu::MENU_TYPE_ITEM))->setLabel(self::t('Installments'))->setUrl(["/$module_name/installment/index"]),
            ])
        ;
        $menu->addItem($_menu, Menu::MENU_POSITION_LAST);
        return $_menu;
    }

    public function getDependencies()
    {
        return [
        ];
    }

}
