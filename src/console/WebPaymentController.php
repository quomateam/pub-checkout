<?php
namespace quoma\checkout\console;

use quoma\checkout\models\Site;
use quoma\checkout\models\WebPayment;
use quoma\modules\config\models\Config;
use yii\console\Controller;

/**
 * Created by PhpStorm.
 * User: gabriela
 * Date: 26/06/18
 * Time: 15:04
 */

class WebPaymentController extends Controller
{

    /**
     * Verifica el estado de los pagos contra las plataformas correspondientes
     *
     * @throws \yii\web\HttpException
     */
    public function actionCheckPayments(){
        echo "Running...";
        echo "\n";

        $currentDate = time();

        $site = Site::find()->where(['server_name' => Config::getValue('checkout_token')])->one();

        //Compras con mas de N minutos y sin pagar, deben deshabilitarse
        $firstTime = $currentDate - (60 * $site->platform_timeout);

        //Solo revisamos compras de las ultimas 48hs:
        $fromTime = $currentDate - (48 * 60 * 60);

        echo "CurrentTime=" . date('H:i:s', $currentDate);
        echo "\n";
        echo "FirstTime=" . date('H:i:s', $firstTime);
        echo "\n";

        $query = WebPayment::find()
            ->where(['in', 'status', ['pending', 'init']])
            ->andWhere(['<=','create_datetime',$firstTime])
            ->andWhere(['>=','create_datetime',$fromTime]);

        echo "Ventas encontradas: " . $query->count();
        echo "\n";

        //Por cada venta no marcada como pagada, verificamos
        foreach ($query->each() as $payment) {
            echo "Venta:" . $payment->web_payment_id;
            echo "\n";
            echo "Fecha de alta de pago = " . date('d-m-Y H:i:s', $payment->create_datetime);
            echo "\n";

            $payment->checkPaymentStatus();
        }

        echo "Fin";
        echo "\n\n";
    }


}