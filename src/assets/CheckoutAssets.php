<?php

namespace quoma\checkout\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CheckoutAssets extends AssetBundle
{
    public $sourcePath = '@vendor/quoma/pub-checkout-module/src/assets/files';
    public $css = [
    ];
    public $js = [
        'js/checkout.js'
    ];
    public $depends = [
    ];
}
