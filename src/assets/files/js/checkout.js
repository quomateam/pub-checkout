/**
 * 
 */
var Checkout = new function(){
    
    var self = this;
    
    //Base url, checkout server
    var baseUrl;
    //Checkout site id

    //Init data
    this.init = function(bUrl){
        baseUrl = bUrl;
    }
    
    /**************
     *  Events
     **************/
    //Before send ajax
    this.onBeforeSend = function(){
        $('.loading-el').addClass('loading');
    }
    
    //Ajax fail
    this.onFail = function(){
        console.log('Error');
    }
    
    //Response status error
    this.onError = function(response){
        console.log('Error');
        console.log(response);
    }
    
    //Always
    this.onAlways = function(){
        $('.loading-el').removeClass('loading');
    }
    
    //Response status success (custom checkout)
    this.onSuccess = function(response){
        console.log(response);
    }
    
    /**
     * 
     * @param object data
     * @returns mixed
     */
    this.pay = function(payment){

        var defaults = {
            //Payment
            id: null,
            token: null,
            start_date: null,
            end_date: null,
            amount: 0.0,
            concept: null,
            installments: 1,
            currency_id: 'ARS',
            return_url: window.location.href,
            expires:0,
            picture_url:null
            //payment_method_id
        };
        
        var data = $.extend({}, defaults, payment);

        $.ajax({
            url: baseUrl + '/default/init-payment',
            type: 'post',
            data: data,
            headers: {
                token:data.token
            },
            beforeSend: this.onBeforeSend()
        }).done(function(response){
            switch (response.status){
                case 'redirect':
                    window.location.replace(response.url);
                    break;
                case 'success':
                    self.onSuccess(response);
                    break;
                case 'error':
                    self.onError(response);
                    break;
            }
        }).always(function(){
            self.onAlways();
        }).fail(function(){
            self.onFail();
        });
    }
}
