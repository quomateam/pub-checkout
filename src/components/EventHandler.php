<?php

namespace quoma\checkout\components;

use Yii;


/**
 * Maneja los eventos en los pagos 
 */
class EventHandler extends \yii\base\Component {

    /**
     * Evento para mandar a facturar si asi lo desea,
     * @param CEvent $event
     */
    public function onPaid($event) {
         
    }

    public function onError($event) {
        //Flash msg:
        Yii::$app->getSession()->setFlash('msg-pay', 'Ocurrió algún error con el pago.');
    }

    public function onCanceled($event) {
    }

    public function onRejected($event) {
        
    }
     public function onRefunded($event) {
        
    }
     public function onInMediation($event) {
        
    }
     public function onChargedBack($event) {
        
    }
    
    
    public function onPending($event) {
        
    }
}

?>
