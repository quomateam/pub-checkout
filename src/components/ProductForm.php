<?php

namespace quoma\checkout\components;

class ProductForm extends \yii\base\Model {

    public $concept;
    public $qty;
    public $unit_net_price;
    public $unit_final_price;

    public function rules() {

        return [
            [['concept'], 'string'],
            [['qty', 'unit_net_price', 'unit_final_price'], 'number'],
        ];
    }

}
