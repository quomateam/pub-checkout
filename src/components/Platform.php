<?php
namespace quoma\checkout\components;

/**
 * Base para la implementacion de las distintas plataformas de pago
 *
 * @author mmoyano
 */
class Platform extends \yii\base\Widget{
    
    public $baseScriptUrl;
    
    public $cssFile;
    
    /**
     * Path alias de assets relativo a application.modules.checkout.components.platforms.Platform
     * @var type 
     */
    protected $assetsPathAlias = null;
    
    public function init(){
        
//        if($this->baseScriptUrl===null && $this->assetsPathAlias != null)
//			$this->baseScriptUrl=Yii::app()->getAssetManager()->publish($this->getPathAlias().'.'.$this->assetsPathAlias);
//
//		if($this->cssFile!==false)
//		{
//			if($this->cssFile===null)
//				$this->cssFile=$this->baseScriptUrl.'/styles.css';
//			Yii::app()->getClientScript()->registerCssFile($this->cssFile);
//		}
        
    }
    
    public function pay($payment){
        throw new CHttpException(500, Yii::t('app','Not implemented.'));
    }
    
    public function getPathAlias(){
        $name = new \ReflectionClass($this);
        $name = $name->getShortName();
        return 'quoma\checkout\platforms\\'.  $name;
        
    }
    
}
