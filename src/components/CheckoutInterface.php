<?php

namespace quoma\checkout\components;

/**
 * Description of checkout
 *
 * @author Gabriela
 */
interface CheckoutInterface {

    public function pay($payment);

    public function renderForm($payment);

    public function getCallbackUrl();

    public function cancelPayment($payment);

    public function checkWebPayment($payment);
}
