<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "web_payment_data".
 *
 * @property integer $web_payment_data_id
 * @property integer $external_payment_id
 * @property integer $web_payment_id
 * @property string $attribute
 */
class WebPaymentData extends \quoma\core\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'web_payment_data';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout_payment');
    }

    /**
     * @inheritdoc
     */
    /*
      public function behaviors()
      {
      return [
      'timestamp' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
      ],
      ],
      'date' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
      ],
      'value' => function(){return date('Y-m-d');},
      ],
      'time' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
      ],
      'value' => function(){return date('h:i');},
      ],
      ];
      }
     */

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['external_payment_id','web_payment_id'], 'required'],
            [['web_payment_id'], 'integer'],
            [['external_payment_id'], 'string', 'max' => 100],
            [['attribute'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'web_payment_data_id' => 'Web Payment Data ID',
            'web_payment_id' => 'Payment ID',
            'external_payment_id' => 'External Payment ID',
            'attribute' => 'Attribute',
        ];
    }

    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable() {
        return true;
    }

    /**
     * Guarda los datos de un pago
     * @param type $data
     * @param type $web_payment_id
     */
    public static function savePaymentData($topic, $ext_id, $web_payment_id) {
        
        $wpd = WebPaymentData::find()->where(['web_payment_id' => $web_payment_id, 'external_payment_id' => $ext_id])->one();

        if (!$wpd) {
            $wp = new WebPaymentData();
            $wp->web_payment_id = $web_payment_id;
            $wp->external_payment_id = $ext_id;
            $wp->attribute = $topic;
            $wp->save();
        }
    }

    public static function findPaymentData($web_payment_id) {

        return WebPaymentData::findAll(['web_payment_id' => $web_payment_id]);
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations() {
        
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if ($this->getDeletable()) {
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}
