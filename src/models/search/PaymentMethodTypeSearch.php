<?php

namespace quoma\checkout\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\checkout\models\PaymentMethodType;

/**
 * PaymentMethodTypeSearch represents the model behind the search form about `app\modules\checkout\models\PaymentMethodType`.
 */
class PaymentMethodTypeSearch extends PaymentMethodType {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['payment_method_type_id'], 'integer'],
            [['key', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = PaymentMethodType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'payment_method_type_id' => $this->payment_method_type_id,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
                ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}
