<?php

namespace quoma\checkout\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\checkout\models\Installment;

/**
 * InstallmentSearch represents the model behind the search form about `quoma\checkout\models\Installment`.
 */
class InstallmentSearch extends Installment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['installment_id', 'key', 'qty'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Installment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'installment_id' => $this->installment_id,
            'key' => $this->key,
            'qty' => $this->qty,
        ]);

        return $dataProvider;
    }
}
