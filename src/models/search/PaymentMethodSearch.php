<?php

namespace quoma\checkout\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\checkout\models\PaymentMethod;

/**
 * PaymentMethodSearch represents the model behind the search form about `quoma\checkout\models\PaymentMethod`.
 */
class PaymentMethodSearch extends PaymentMethod
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_method_id', 'platform_id'], 'integer'],
            [['name','code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentMethod::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'payment_method_id' => $this->payment_method_id,
            'code' => $this->code,
//            'payment_method_type' => $this->payment_method_type,
            'platform_id' => $this->platform_id
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
