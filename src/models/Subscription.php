<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "installment".
 *
 * @property integer $subscription_id
 * @property string $external_id
 * @property string $payer_id
 * @property string $status
 * @property string $reason
 * @property string $reference
 * @property string $date_created
 * @property string $last_modified
 * @property string $frequency
 * @property string $frequency_type
 * @property double $transaction_amount
 * @property string $currency_id
 * @property string $start_date
 * @property string $end_date
 * @property string $uuid
 *
 * @property WebPayment[] $web_payments
 */
class Subscription extends \quoma\core\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'subscription';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    /*
      public function behaviors()
      {
      return [
      'timestamp' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
      ],
      ],
      'date' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
      ],
      'value' => function(){return date('Y-m-d');},
      ],
      'time' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
      ],
      'value' => function(){return date('h:i');},
      ],
      ];
      }


      /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['subscription_id'], 'integer'],
            [['uuid'], 'string', 'max' => 100],
            [['external_id', 'payer_id', 'status', 'reason', 'reference', 'date_created', 'last_modified', 'frequency', 'frequency_type', 'currency_id', 'start_date', 'end_date'], 'string'],
            [['transaction_amount'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
        ];
    }

    public function beforeSave($insert) {


        if ($insert) {
            //Quitado por parche para enviar id de suscripcion y del primer pago creado a MP
//            if (empty($this->uuid)) {
//                $this->uuid = uniqid('', true);
//            }
            if($this->status == NULL){
                $this->status = 'pending';
            }
            $this->date_created = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebPayments() {
        return $this->hasMany(WebPayment::className(), ['subscription_id' => 'subscription_id']);
    }

    /**
     * @inheritdoc
     * Strong relations: SiteHasInstallments, Sites.
     */
    public function getDeletable() {
        if ($this->getWebPayments()->exists()) {
            return false;
        }

        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations() {
        
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if ($this->getDeletable()) {
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}
