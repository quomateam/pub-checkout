<?php


namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "web_receipt".
 *
 * The followings are the available columns in table 'web_receipt':
 * @property integer $web_receipt_id
 * @property string $currency
 * @property string $datetime
 * @property integer $autorization_code
 * @property integer $installments
 * @property string $owner
 * @property double $amount
 * @property string $card
 * @property string $email
 * @property string $operation_number
 * @property string $status
 * @property string $visa_address_validation
 * @property integer $visa_vbv_auth
 * @property integer $web_payment_id
 *
 * The followings are the available model relations:
 * @property WebPayment $webPaymentWebPayment
 */
class WebReceipt extends \quoma\core\db\ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return 'web_receipt';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            [['web_payment_id'], 'required'],
            [['autorization_code', 'installments', 'visa_vbv_auth', 'web_payment_id'], 'integer'],
            [['amount'], 'number'],
            [['currency', 'card', 'operation_number'], 'string','max' => 45],
            [['owner', 'email','platform_payment_id'], 'string', 'max' => 255],
            [['status', 'visa_address_validation'], 'string',  'max' => 10],
            [['datetime'], 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('web_receipt_id, currency, datetime, autorization_code, installments, owner, amount, card, email, operation_number, status, visa_address_validation, visa_vbv_auth, web_payment_id', 'safe', 'on' => 'search'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebPayment()
    {
        return $this->hasOne(WebPayment::className(), ['web_payment_id' => 'web_payment_id']);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'web_receipt_id' =>\quoma\checkout\CheckoutModule::t('Id'),
            'currency' =>\quoma\checkout\CheckoutModule::t('Currency'),
            'datetime' => \quoma\checkout\CheckoutModule::t('Datetime'),
            //TODO: corregir:
            'autorization_code' => \quoma\checkout\CheckoutModule::t('Autorization_code'),
            'installments' => \quoma\checkout\CheckoutModule::t('Installments'),
            'owner' => \quoma\checkout\CheckoutModule::t('Owner'),
            'amount' => \quoma\checkout\CheckoutModule::t('Amount'),
            'card' => \quoma\checkout\CheckoutModule::t('Card'),
            'email' => \quoma\checkout\CheckoutModule::t( 'Email'),
            'operation_number' => \quoma\checkout\CheckoutModule::t('operation_number'),
            'status' => \quoma\checkout\CheckoutModule::t('Status'),
            'visa_address_validation' => \quoma\checkout\CheckoutModule::t('Visa address validation'),
            'visa_vbv_auth' => \quoma\checkout\CheckoutModule::t('Visa vbv Auth'),
            'web_payment_id' => \quoma\checkout\CheckoutModule::t('Payment'),
        );
    }

}
