<?php

namespace quoma\checkout\models;

use quoma\checkout\components\PlatformFactory;
use Yii;

/**
 * This is the model class for table "site".
 *
 * @property integer $site_id
 * @property string $name
 * @property string $server_name
 * @property string $status
 * @property string $image
 * @property string $admin_email
 *
 * @property SiteHasInstallment[] $siteHasInstallments
 * @property Installment[] $installments
 * @property SiteHasPaymentMethod[] $siteHasPaymentMethods
 * @property PaymentMethod[] $paymentMethods
 * @property Platform[] $platform
 * @property WebPayment[] $webPayments
 */
class Site extends \quoma\core\db\ActiveRecord {

    private $_installments;
    private $_paymentMethods;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'site';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $rules = [
            [['status', 'admin_email'], 'string'],
            [['status', 'name', 'server_name', 'admin_email', 'platform_id', 'platform_timeout'], 'required'],
            [['installments', 'paymentMethods', 'platform'], 'safe'],
            [['name'], 'string', 'max' => 45],
            [['server_name'], 'string', 'max' => 100],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'platform' => \quoma\checkout\CheckoutModule::t('Platform'),
            'site_id' => \quoma\checkout\CheckoutModule::t('Site'),
            'name' => \quoma\checkout\CheckoutModule::t('Name'),
            'server_name' => \quoma\checkout\CheckoutModule::t('Server Name'),
            'status' => \quoma\checkout\CheckoutModule::t('Status'),
            'siteHasInstallments' => 'SiteHasInstallments',
            'installments' => 'Installments',
            'siteHasPaymentMethods' => 'SiteHasPaymentMethods',
            'paymentMethods' => 'PaymentMethods',
            'platform' => \quoma\checkout\CheckoutModule::t('Platform'),
            'webPayments' => 'WebPayments',
            'admin_email' => \quoma\checkout\CheckoutModule::t('Admin Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteHasInstallments() {
        return $this->hasMany(SiteHasInstallment::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstallments() {
        return $this->hasMany(Installment::className(), ['installment_id' => 'installment_id'])->viaTable('site_has_installment', ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteHasPaymentMethods() {
        return $this->hasMany(SiteHasPaymentMethod::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethods() {
        return $this->hasMany(PaymentMethod::className(), ['payment_method_id' => 'payment_method_id'])->viaTable('site_has_payment_method', ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethodTypes() {
        return $this->hasMany(PaymentMethodType::className(), ['payment_method_type_id' => 'payment_method_type_id'])->viaTable('payment_method_type_has_site', ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethodTypeHasSite() {
        return $this->hasMany(PaymentMethodTypeHasSite::className(), ['site_id' => 'site_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform() {
        return $this->hasOne(Platform::className(), ['platform_id' => 'platform_id']);
    }

    /**
     * Devuelve el experto de la plataforma de pago
     *
     * @return \class
     */
    public function loadPlatform(){

        $platform = $this->platform;
        $factory = new PlatformFactory();

        //busco plataformas que usa
        return $factory->getPlatform($platform);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebPayments() {
        return $this->hasMany(WebPayment::className(), ['site_id' => 'site_id']);
    }

    /**
     * @brief Sets Installments relation on helper variable and handles events insert and update
     */
    public function setInstallments($installments) {

        if (empty($installments)) {
            $installments = [];
        }

        $this->_installments = $installments;

        $saveInstallments = function($event){
            $this->unlinkAll('installments', true);
            foreach ($this->_installments as $id) {
                $this->link('installments', Installment::findOne($id));
            }
        };

        $this->on(self::EVENT_AFTER_INSERT, $saveInstallments);
        $this->on(self::EVENT_AFTER_UPDATE, $saveInstallments);

    }


    /**
     * @brief Sets PaymentMethods relation on helper variable and handles events insert and update
     */
    public function setPaymentMethodsTypes($paymentMethodsTypes) {

        if (empty($paymentMethodsTypes)) {
            $paymentMethodsTypes = [];
        }


        $this->_paymentMethodsTypes = $paymentMethodsTypes;

        $savePaymentMethodsTypes = function($event){
            $this->unlinkAll('paymentMethodTypes', true);
            foreach ($this->_paymentMethodsTypes as $id) {
                $this->link('paymentMethodTypes', PaymentMethodType::findOne($id));
            }

        };

        $this->on(self::EVENT_AFTER_INSERT, $savePaymentMethodsTypes);
        $this->on(self::EVENT_AFTER_UPDATE, $savePaymentMethodsTypes);

    }

    /**
     * @brief Sets PaymentMethods relation on helper variable and handles events insert and update
     */
    public function setPaymentMethods($paymentMethods) {

        if (empty($paymentMethods)) {
            $paymentMethods = [];
        }


        $this->_paymentMethods = $paymentMethods;

        $savePaymentMethods = function($event){
            $this->unlinkAll('paymentMethods', true);
            foreach ($this->_paymentMethods as $id) {
                $this->link('paymentMethods', PaymentMethod::findOne($id), []);
            }

        };

        $this->on(self::EVENT_AFTER_INSERT, $savePaymentMethods);
        $this->on(self::EVENT_AFTER_UPDATE, $savePaymentMethods);

    }


    /**
     * @inheritdoc
     * Strong relations: WebPayments.
     */
    public function getDeletable() {
        if ($this->getWebPayments()->exists()) {
            return false;
        }
        return true;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function fillPaymentMethods() {

        $select = [];

        $paymentMethods = $this->siteHasPaymentMethods;

        if ($paymentMethods)
            foreach ($paymentMethods as $model) {
                if (isset($model->payment_method_id))
                    $select[] = $model->payment_method_id;
                elseif (!is_object($model))
                    $select[] = $model;
            }

        return $select;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function fillPaymentMethodTypes() {

        $select = [];

        $paymentMethodTypes = $this->paymentMethodTypeHasSite;

        if ($paymentMethodTypes)
            foreach ($paymentMethodTypes as $model) {
                if (isset($model->payment_method_type_id))
                    $select[] = $model->payment_method_type_id;
                elseif (!is_object($model))
                    $select[] = $model;
            }

        return $select;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function findTimeOutPaymentMethodTypes($payment_method_type_id) {

        $paymentMethodType = $this->getPaymentMethodTypeHasSite()->andWhere(['payment_method_type_id' => $payment_method_type_id])->one();

        if ($paymentMethodType){
            return $paymentMethodType->time_out;
        }
        
        return NULL;
    }

    /**
     * Rellena la relación con los id en lugar de objetos
     */
    public function paymentMethodIsSelected($selected, $payment_method_id) {

        if ($selected)
            foreach ($selected as $id) {
                if ($id == $payment_method_id) {
                    return true;
                }
            }

        return false;
    }


    /**
     * Valida que el usuario haya seleccionado todas las relaciones necesarias
     */
    public function validateRelations($post) {

        if ($post['paymentMethods'] == '') {
            $this->addError('platform_id', 'Debe seleccionar los métodos de pago para la plataforma.');
            return false;
        }
        if (!isset($post['installments'])) {
            $this->addError('platform_id', 'Debe seleccionar las cuotas permitidas para la plataforma.');
            return false;
        }

        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations() {
        $this->unlinkAll('paymentMethods', true);
        $this->unlinkAll('paymentMethodTypes', true);
        $this->unlinkAll('installments', true);
    }

    public static function getStatus() {
        return [
            'enabled' => \quoma\checkout\CheckoutModule::t('Enabled'),
            'disabled' => \quoma\checkout\CheckoutModule::t('Disabled'),
        ];
    }

    public function getLabelStatus() {
        return $this->getStatus()[$this->status];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if ($this->getDeletable()) {
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }


    /*
     * Busca en un sitio la cuota mas alta
     */

    public function findInstallment() {

        return $this->getInstallments()->max('qty');
    }

    /**
     * Devuelve el interes en porcentaje
     * @param $installment_id
     * @return int|mixed
     */
    public function getInterest($installment_id){
        $installment = $this->getSiteHasInstallments()->where(['installment_id' => $installment_id])->one();
        return ($installment && $installment->interest>0) ? $installment->interest : 0;
    }

    /**
     * @param $installment_id
     * @param $amount
     * @return float|int
     */
    public function calculateAmount($installment_id, $amount){
        $interest = $this->getInterest($installment_id);

        return ($interest * $amount /100) + $amount;
    }

    /**
     * @param $installment_id
     * @param $amount
     * @return float|int
     */
    public function calculateAmountInstallment($installment_id, $amount){
        $installment= Installment::findOne($installment_id);
        $site_has_installment = $this->getSiteHasInstallments()->where(['installment_id' => $installment_id])->one();

        return $this->calculateAmount($installment_id, $amount) / $installment->qty ;
    }

}
