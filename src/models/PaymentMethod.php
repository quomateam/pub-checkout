<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "payment_method".
 *
 * @property integer $payment_method_id
 * @property integer $code
 * @property string $name
 * @property integer $platform_id
 * @property integer $accreditation_time
 * @property integer $payment_method_type_id
 *
 * @property SiteHasPaymentMethod[] $siteHasPaymentMethods
 * @property Site[] $sites
 */
class PaymentMethod extends \quoma\core\db\ActiveRecord {

    private $_sites;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'payment_method';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    /*
      public function behaviors()
      {
      return [
      'timestamp' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
      ],
      ],
      'date' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
      ],
      'value' => function(){return date('Y-m-d');},
      ],
      'time' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
      ],
      'value' => function(){return date('h:i');},
      ],
      ];
      }
     */

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['code', 'name', 'platform_id', 'payment_method_type_id'], 'required'],
            [['payment_method_id','accreditation_time'], 'integer'],
            [['sites'], 'safe'],
            [['name', 'code'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'payment_method_id' => \quoma\checkout\CheckoutModule::t('Payment Method'),
            'code' => \quoma\checkout\CheckoutModule::t('Code'),
            'name' => \quoma\checkout\CheckoutModule::t('Name'),
            'siteHasPaymentMethods' => \quoma\checkout\CheckoutModule::t('Site Payment Methods'),
            'sites' => \quoma\checkout\CheckoutModule::t('Sites'),
            'payment_method_type' => \quoma\checkout\CheckoutModule::t('Payment Method Type'),
            'platform_id' => \quoma\checkout\CheckoutModule::t('Platform'),
            'accreditation_time' => \quoma\checkout\CheckoutModule::t('Tiempo límite de procesamiento'),
            'payment_method_type_id' => \quoma\checkout\CheckoutModule::t('Payment Method Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteHasPaymentMethods() {
        return $this->hasMany(SiteHasPaymentMethod::className(), ['payment_method_id' => 'payment_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites() {
        return $this->hasMany(Site::className(), ['site_id' => 'site_id'])->viaTable('site_has_payment_method', ['payment_method_id' => 'payment_method_id']);
    }

    public function getPlatform() {
        return $this->hasOne(Platform::className(), ['platform_id' => 'platform_id']);
    }

    public function getPaymentMethodType() {
        return $this->hasOne(PaymentMethodType::className(), ['payment_method_type_id' => 'payment_method_type_id']);
    }

    /**
     * @brief Sets Sites relation on helper variable and handles events insert and update
     */
    public static function setSites($sites) {

        if (empty($sites)) {
            $sites = [];
        }

        $this->_sites = $sites;

        $saveSites = function($event) {
            $this->unlinkAll('sites', true);

            foreach ($this->_sites as $id) {
                $this->link('sites', Site::findOne($id));
            }
        };
        $this->on(self::EVENT_AFTER_INSERT, $saveSites);
        $this->on(self::EVENT_AFTER_UPDATE, $saveSites);
    }

    /**
     * @inheritdoc
     * Strong relations: SiteHasPaymentMethods, Sites.
     */
    public function getDeletable() {
        if ($this->getSiteHasPaymentMethods()->exists()) {
            return false;
        }
        if ($this->getSites()->exists()) {
            return false;
        }
        return true;
    }

    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations() {
        
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if ($this->getDeletable()) {
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}
