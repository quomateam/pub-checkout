<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "installment".
 *
 * @property integer $installment_id
 * @property integer $key
 * @property integer $qty
 * @property string $label
 *
 * @property SiteHasInstallment[] $siteHasInstallments
 * @property Site[] $sites
 */
class Installment extends \quoma\core\db\ActiveRecord
{

    private $_sites;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'installment';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }
    
    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'qty'], 'required'],
            [['installment_id', 'key', 'qty'], 'integer'],
            ['key' , 'unique'],
            [['label'], 'string'],
            [['sites', 'label'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'installment_id' => \quoma\checkout\CheckoutModule::t('ID'),
            'key' => \quoma\checkout\CheckoutModule::t('Key'),
            'qty' => \quoma\checkout\CheckoutModule::t('Installments'),
            'siteHasInstallments' => \quoma\checkout\CheckoutModule::t('Sites'),
            'sites' => \quoma\checkout\CheckoutModule::t('Sites'),
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteHasInstallments()
    {
        return $this->hasMany(SiteHasInstallment::className(), ['installment_id' => 'installment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::className(), ['site_id' => 'site_id'])->viaTable('site_has_installment', ['installment_id' => 'installment_id']);
    }
    
        
        
        
    /**
     * @brief Sets Sites relation on helper variable and handles events insert and update
     */
    public static function setSites($sites){

        if(empty($sites)){
            $sites = [];
        }

        $this->_sites = $sites;

        $saveSites = function($event){
            $this->unlinkAll('sites', true);

            foreach ($this->_sites as $id) {
                $this->link('sites', Site::findOne($id));
            }
        };
        $this->on(self::EVENT_AFTER_INSERT, $saveSites);
        $this->on(self::EVENT_AFTER_UPDATE, $saveSites);
    }
    
        
                 
    /**
     * @inheritdoc
     * Strong relations: SiteHasInstallments, Sites.
     */
    public function getDeletable()
    {
        if($this->getSiteHasInstallments()->exists()){
            return false;
        }
        if($this->getSites()->exists()){
            return false;
        }
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations(){
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}
