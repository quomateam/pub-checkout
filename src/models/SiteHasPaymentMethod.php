<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "site_has_payment_method".
 *
 * @property integer $site_id
 * @property integer $payment_method_id
 * @property integer $site_has_payment_method_id
 * @property integer $time_out
 *
 * @property PaymentMethod $paymentMethod
 * @property Site $site
 */
class SiteHasPaymentMethod extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_has_payment_method';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }
    
    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'payment_method_id'], 'required'],
            [['site_id', 'payment_method_id', 'time_out'], 'integer'],
            [['paymentMethod', 'site'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'payment_method_id' => 'Payment Method ID',
            'paymentMethod' => 'PaymentMethod',
            'site' => 'Site',
            'time_out' => 'Time Out'
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::className(), ['payment_method_id' => 'payment_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }
    
        
        
        
                 
    /**
     * @inheritdoc
     * Strong relations: PaymentMethod, Site.
     */
    public function getDeletable()
    {
        if($this->getPaymentMethod()->exists()){
            return false;
        }
        if($this->getSite()->exists()){
            return false;
        }
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations(){
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}
