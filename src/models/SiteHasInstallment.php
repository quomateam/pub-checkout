<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "site_has_installment".
 *
 * @property integer $site_id
 * @property integer $installment_id
 * @property integer $site_has_installment_id
 * @property double $interest
 *
 * @property Installment $installment
 * @property Site $site
 */
class SiteHasInstallment extends \quoma\core\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_has_installment';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }
    
    /**
     * @inheritdoc
     */
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['timestamp'],
                ],
            ],
            'date' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                ],
                'value' => function(){return date('Y-m-d');},
            ],
            'time' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['time'],
                ],
                'value' => function(){return date('h:i');},
            ],
        ];
    }
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id', 'installment_id'], 'required'],
            [['site_id', 'installment_id'], 'integer'],
            [['interest'], 'double'],
            [['installment', 'site'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'site_id' => 'Site ID',
            'installment_id' => 'Installment ID',
            'installment' => 'Installment',
            'site' => 'Site',
        ];
    }    


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstallment()
    {
        return $this->hasOne(Installment::className(), ['installment_id' => 'installment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }
    
        
        
        
                 
    /**
     * @inheritdoc
     * Strong relations: Installment, Site.
     */
    public function getDeletable()
    {
        if($this->getInstallment()->exists()){
            return false;
        }
        if($this->getSite()->exists()){
            return false;
        }
        return true;
    }
    
    /**
     * @brief Deletes weak relations for this model on delete
     * Weak relations: None.
     */
    protected function unlinkWeakRelations(){
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if($this->getDeletable()){
                $this->unlinkWeakRelations();
                return true;
            }
        } else {
            return false;
        }
    }

}
