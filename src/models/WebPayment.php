<?php

namespace quoma\checkout\models;

use quoma\checkout\components\PlatformFactory;
use quoma\checkout\modules\siteConfig\models\SiteConfig;
use quoma\checkout\platforms\DecidirPrisma\models\CybersourceData;
use quoma\core\helpers\DbHelper;
use Yii;
use \quoma\modules\config\models\Config;
use quoma\checkout\platforms\MercadoPago\MercadoPago;

/**
 * This is the model class for table "web_payment".
 *
 * The followings are the available columns in table 'web_payment':
 * @property string $web_payment_id
 * @property string $status
 * @property string $message
 * @property double $amount
 * @property string $uuid
 * @property string $concept
 * @property integer $platform_id
 * @property string $return_url
 * @property integer $invoice
 * @property integer $site_id
 * @property integer $bill_id
 * @property string $result_url
 * @property string $request_uuid
 * @property integer $installments
 * @property string $payer_email
 * @property integer $frequency
 * @property string $currency_id
 * @property string $start_date
 * @property string $end_date
 * @property string $reason
 * @property integer $payment_method_id
 * @property string $bin
 * @property float $interest //interes por cuotas (porcentaje)
 * @property float $interest_amount //interes por cuotas (monto que se agregará al precio bruto)
 * @property float $gross_amount // monto con intereses
 * @property boolean $is_market_place //Es un pago por market place
 * @property double $marketplace_fee //Costo por pago con Market Place
 * @property integer $mp_seller_id //Id del vendedor para MarketPlace
 * 
 *
 * The followings are the available model relations:
 * @property Bill $bill
 * @property WebReceipt[] $receipts
 * @property CybersourceData $cybersourceData
 */
class WebPayment extends \quoma\core\db\ActiveRecord {

    const TOLERANCE = '0.01';

    /* Dato del comercio para conectarse con Decidir */

    protected $commerce_number;

    /* Datos del comercio para conectarse con Mercado Pago */
    protected $client_id;
    protected $secret;

    public $cybersource_data;

    /**
     * @return string the associated database table name
     */
    public static function tableName() {

        $dbName = DbHelper::getDbName('db_checkout');

        return "$dbName.web_payment";
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb() {
        return Yii::$app->get('db_checkout');
    }

    /**
     * Generamos un id unico para luego efectuar un pago
     */
    public function init() {
        $this->on('onPay', ['\quoma\checkout\components\EventHandler', 'onPay']);
        $this->on('onError', ['\quoma\checkout\components\EventHandler', 'onError']);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            [['site_id', 'request_uuid'], 'required'],
            [['status', 'currency_id', 'start_date', 'end_date', 'reason', 'external_status', 'result_url'], 'string'],
            [['platform_id', 'site_id', 'bill_id', 'installments', 'frequency','payment_method_id', 'mp_seller_id'], 'integer'],
            [['status'], 'string', 'max' => 8],
            [['message', 'concept','platform_payment_id'], 'string', 'max' => 255],
            [['return_url'], 'string', 'max' => 100],
            [['uuid', 'request_uuid','expiration_date_from','expiration_date_to'], 'string', 'max' => 45],
            [['token'], 'string', 'max' => 50],
            [['bin'], 'string', 'max' => 6],
            [['amount', 'interest','gross_amount','net_amount','interest_amount', 'marketplace_fee'], 'double'],
            [['invoice','expires', 'is_market_place'], 'boolean'],
            [['payer_email'], 'email'],
            [['cybersource_data'], 'safe'],
            ['cybersource_data', 'validateCybersourceData', 'on' => 'insert'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            [['web_payment_id', 'status', 'message', 'amount', 'uuid', 'concept', 'site_id', 'invoice', 'return_url','platform_payment_id'], 'safe', /**'on' => 'search'**/],
        );
    }

    //TODO: hacer con un modelo (yii\base\Model) y reglas de validación
    public function validateCybersourceData(){

        if (empty($this->cybersource_data)){
            $this->addError('cybersource_data', Yii::t('app','Cybersource Data is required'));
            return false;
        }

        if (!isset($this->cybersource_data['first_name'])){
            $this->addError('cybersource_data', Yii::t('app','First Name is required'));
        }

        if (!isset($this->cybersource_data['last_name'])){
            $this->addError('cybersource_data', Yii::t('app','Last Name is required'));
        }

        if (!isset($this->cybersource_data['customer_id'])){
            $this->addError('cybersource_data', Yii::t('app','Customer ID is required'));
        }

        if (!isset($this->cybersource_data['currency'])){
            $this->addError('cybersource_data', Yii::t('app','Currency is required'));
        }

        if (!isset($this->cybersource_data['amount'])){
            $this->addError('cybersource_data', Yii::t('app','Amount is required'));
        }

        if (!isset($this->cybersource_data['items'])){
            $this->addError('cybersource_data', Yii::t('app','Items is required'));
        }

    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'web_payment_id' => \quoma\checkout\CheckoutModule::t('Web Payment'),
            'status' => \quoma\checkout\CheckoutModule::t('Status'),
            'message' => \quoma\checkout\CheckoutModule::t('Message'),
            'amount' => \quoma\checkout\CheckoutModule::t('Amount'),
            'uuid' => \quoma\checkout\CheckoutModule::t('UUID'),
            'concept' => \quoma\checkout\CheckoutModule::t('Concept'),
            'return_url' => \quoma\checkout\CheckoutModule::t('Return Url'),
            'site_id' => \quoma\checkout\CheckoutModule::t('Site'),
            'site' => \quoma\checkout\CheckoutModule::t('Site'),
            'invoice' => \quoma\checkout\CheckoutModule::t('Invoice'),
            'platform_id' => \quoma\checkout\CheckoutModule::t('Platform'),
            'interest' => \quoma\checkout\CheckoutModule::t('Interest'),
            'interest_amount' => \quoma\checkout\CheckoutModule::t('Interest Amount'),
            'gross_amount' => \quoma\checkout\CheckoutModule::t('Gross Amount'),
            'labelStatus' => \quoma\checkout\CheckoutModule::t('Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceipts() {
        return $this->hasMany(WebReceipt::className(), ['web_payment_id' => 'web_payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite() {
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform() {
        return $this->hasOne(Platform::className(), ['platform_id' => 'platform_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill() {
        return $this->hasMany(\app\modules\sale\models\Bill::className(), ['bill_id' => 'bill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedReceipts() {
        return $this->hasMany(WebReceipt::className(), ['web_payment_id' => 'web_payment_id'])->andWhere('status="approved"');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRejectedReceipts() {
        return $this->hasMany(WebReceipt::className(), ['web_payment_id' => 'web_payment_id'])->andWhere('status="rejected"');
    }

    /**
     * Returns true if the object is being used and can not be deleted. Otherwise returns false.
     * @return boolean
     */
    public function isUsed() {

        if (empty($this->receipts))
            return false;
        else
            return true;
    }

    public function beforeSave($insert)
    {
        if (empty($this->uuid)) {
            $this->uuid = uniqid('', true);
        }

        if (empty($this->status)) {
            $this->status = 'pending';
        }

        if ($insert) {
            $this->create_datetime = time();
            $this->type = 'payment';
        }

        return parent::beforeSave($insert);
    }

    /**
     * No permitimos que el objeto sea eliminado si esta siendo usado.
     * @return boolean
     */
    public function beforeDelete()
    {

        if ($this->isUsed){
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Controla que el importe abonado corresponda al importe del pago, con
     * una tolerancia defeinida por self::TOLERANCE
     * @return boolean
     */
    public function check()
    {

        $total = 0.0;

        foreach ($this->getApprovedReceipts()->all() as $receipt) {
            $total += (float)$receipt->amount;
        }

        $total_tolerated = (1 + self::TOLERANCE) * $total;

        //Validamos que el importe abonado corresponda con el importe de la factura
        if ($this->amount <= $total_tolerated)
            return true;

        return false;
    }

    /**
     * Devuelve el saldo pendiente de pago
     * @return boolean
     */
    public function getPendingAmount()
    {

        $paid = 0.0;
        foreach ($this->approvedReceipts as $receipt) {
            $paid += (float)$receipt->amount;
        }

        return $this->amount - $paid;
    }

    /**
     * Cambia el estado del pago y llama a los eventos asociados al nuevo estado
     */
    public function changeStatus($status)
    {

        $this->status = $status;

        if ($this->updateAttributes(['status'])) {

            if ($this->status == 'in_mediation') {
                $this->onInMediation(new \yii\base\Event(['sender' => $this]));
            } elseif ($this->status == 'charged_back') {
                $this->onChargedBack(new \yii\base\Event(['sender' => $this]));
            } else {
                $event_name = 'on' . ucfirst($this->status);
                $this->$event_name(new \yii\base\Event(['sender' => $this]));
            }

            $this->update_datetime = time();
            $this->save(['update_datetime']);
        }
    }

    /**
     * Recibo del pago
     * @param array $data
     * @return boolean
     */
    public function receipt($data)
    {

        //Generamos un recibo
        $receipt = new \quoma\checkout\models\WebReceipt();
        $receipt->web_payment_id = $this->web_payment_id;

        //Cargamos los datos recibidos
        $receipt->load($data, '');
        $receipt->datetime = ($data['datetime'] == '') ?  $data['datetime'] : date('Y-m-d H:i:s');
        $receipt->operation_number = $this->uuid;
        $receipt->platform_payment_id = (string)$data['id'];

        //Al guardar, cambiamos el estado del pago segun si el pago fue completo o no
        if ($receipt->save()) {

            return $this->checkWebPaymentStatus($receipt);
            
        } else {

            $this->changeStatus('error');

            $message = '';
            foreach ($receipt->getErrors() as $error) {
                $message .= $error[0];
            }

            $this->message = $message;
            $this->save();
            return $message;
        }
    }

    /**
     * Determina el estado del web payment
     * @param type $receipt
     * @return string
     */
    protected function checkWebPaymentStatus($receipt)
    {
        $this->updateAttributes(['external_status' => $receipt->status, 'platform_payment_id' => $receipt->platform_payment_id]);
            
        $status = $receipt->status;
        
        //Estado
        if ($receipt->status == 'in_process' || $receipt->status == 'authorized') {
            $status = 'pending';
        } elseif ($receipt->status == 'cancelled' || $receipt->status === 'annulled') {
            $status = 'canceled';
        }

        //Pago aprobado
        if ($receipt->status == 'approved') {
            //si el pago ya ha sido cancelado en cobros le notifico al administrador para que verifique si hubo sobreventa
            if (in_array($this->status, ['canceled', 'timeout', 'error'])) {
                $this->sendMail(Yii::$app->params['adminEmail']);
            } 

            if ($this->check()) {

                $this->changeStatus('paid');
            } else {
                $this->changeStatus('partial');
            }

            return 'approved';
        } else {

            $status = $status;
            if($receipt->status == 'unapproved'){
                $status = 'error';
            }
            //Pago no aprobado
            $this->changeStatus($status);
            return 'error';
        }
    }
    
    /**
     * Envía un correo
     * @param string $to
     */
    public function notifyAdmin($subject, $body) {

        Yii::$app->mailer->compose()
                ->setFrom(SiteConfig::get('checkout_admin_email',$this->site_id ))
                ->setTo(SiteConfig::get('checkout_admin_email', $this->site_id))
                ->setSubject($subject)
                ->setTextBody($body)
                ->send();
    }

    public function sendMail($to) {

        $body = 'El pago ' . $this->uuid . " fue aprobado por ". $this->platform->name."pero el mismo fue cancelado previamente.\n\n";
        $body .= "Por favor pedimos revisarlo.\n\n";
        $body .= "Datos del Pago:\n";
        $body .= ' Nro de operación en Cobros: ' . $this->uuid . "\n";
        $body .= ' Nro de operación en ' . $this->site->name . ': ' . $this->request_uuid . "\n";
        $body .= ' Estado: ' . $this->status . "\n";

        Yii::$app->mailer->compose()
                ->setFrom(SiteConfig::get('checkout_admin_email', $this->site_id))
                ->setTo($to)
                ->setSubject('URGENTE! Ocurrió algún error en un pago.')
                ->setTextBody($body)
                ->send();
    }

    /**
     * @brief Obtiene el ultimo datetime de todos los receipts aprobados del pago
     * @return type
     */
    public function getLastReceiptTimeout()
    {

        $receipts = $this->approvedReceipts;

        $datetime = 0;

        if (!empty($receipts))
            foreach ($receipts as $key => $receipt)
                if ($receipt->datetime > $datetime)
                    $datetime = $receipt->datetime;

        return $datetime;
    }

    /**
     * @brief Chequea si el pago no ha superado el tiempo de vida máximo
     * @return boolean
     */
    public function isOnTimeout()
    {
        $timeout_confim_payment = $this->site->platform_timeout;
        $timeout = $this->create_datetime + (60 * ($timeout_confim_payment - 1)) - 1;
        $now = time();

        if ($now > $timeout)
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Devuelve el tiempo en minutos (timeout) configurado para el sitio
     * @return int
     */
    public function getTimeOut($payment_method_key)
    {
        $time_out = 0;
        $site = $this->site;

        if ($site->platform_timeout != '') {
            $time_out = $site->platform_timeout;
        } else {
            $payment_method_type_has_site = $site->getPaymentMethodTypeHasSite()->where(['platform_id' => $this->platform_id, 'payment_method_type_id' => $payment_method_key])->one();
            if ($payment_method_type_has_site && $payment_method_type_has_site->time_out != '') {
                $time_out = $payment_method_type_has_site->time_out;
            }
        }

        return $time_out;
    }

    /**
     * Evento disparado al cambiar el estado de un pago a "paid"
     * @param type $event
     */
    public function onPaid($event)
    {
        $this->trigger('onPaid');
    }

    /**
     * Evento disparado al cambiar el estado de un pago a "error"
     * @param type $event
     */
    public function onError($event)
    {
        $this->trigger('onError');
    }

    /**
     * Evento disparado al cambiar el estado de un pago a "error"
     * @param type $event
     */
    public function onCanceled($event)
    {
        $this->trigger('onCanceled');
    }

    /**
     * Evento disparado al cambiar el estado de un pago a "error"
     * @param type $event
     */
    public function onTimeout($event)
    {
        $this->trigger('onTimeout');
    }

    public function onRejected($event)
    {
        $this->trigger('onRejected');
    }

    public function onRefunded($event)
    {
        $this->trigger('onRefunded');
    }

    public function onInMediation($event)
    {
        $this->trigger('onInMediation');
    }

    public function onChargedBack($event)
    {
        $this->trigger('onChargedBack');
    }
    
    public function onPending($event)
    {
        $this->trigger('onPending');
    }

    /*
     * Devuelve el uuid del pago consultado a Mercado Pago con el $preapproval_id
     */
    public static function getPaymentInfo($site_id, $preapproval_id)
    {

        $platform = \quoma\checkout\platforms\MercadoPago\MercadoPago::getPlatform();
        $site = Site::findOne($site_id);

        $secret = SiteData::getAttrSiteData($site->site_id, $platform->platform_id, 'secret');
        $client_id = SiteData::getAttrSiteData($site->site_id, $platform->platform_id, 'client_id');

        $mp = new \MP($client_id, $secret);

        $payment_info = $mp->get_preapproval_payment($preapproval_id);

        if ($payment_info["status"] == 200) {
            return $payment_info;
        }
    }

    /**
     * Crea una subsacripción para pagos con debito automatico
     * @param type $response
     * @return \quoma\checkout\models\Subscription
     */
    public static function getSubscription($response)
    {
        //Obtengo el uuid del pago desde el external_reference del pago . 
        //TODO: Cambiar esto!!!!En los pagos con debito se debe crear una suscripción y los pagos se deben ir creando a medida que 
        //Mercado pago envia notificaciones

        //busco si la suscripción ya existe  sino la creo
        $subscription = Subscription::find()->where(['uuid' => $response['external_reference']])->one();
       
        if (!$subscription) {
            $subscription = new Subscription();
            $subscription->status = 'pending';
        }

        $subscription->load($response, '');
        $subscription->frequency = (string)$response['auto_recurring']['frequency'];
        $subscription->frequency_type = $response['auto_recurring']['frequency_type'];
        $subscription->transaction_amount = $response['auto_recurring']['transaction_amount'];
        $subscription->currency_id = $response['auto_recurring']['currency_id'];
        $subscription->start_date = (isset($response['auto_recurring']['end_date'])) ? $response['auto_recurring']['end_date'] : '';
        $subscription->end_date = (isset($response['auto_recurring']['end_date'])) ? $response['auto_recurring']['end_date'] : NULL;
        if (isset($response['id'])) {
            $subscription->external_id = $response['id'];
        }
        $subscription->uuid = $response['external_reference'];
        
        if(isset($response['status'])){
             $subscription->status = $response['status'];
        }
        
         if(isset($response['payer_id'])){
             $subscription->payer_id = (string)$response['payer_id'];
        }
      
        if ($subscription->save()) {
            return $subscription;
        }else{
           
            \Yii::info($subscription);
            \Yii::info($subscription->getErrors());
        }
        return false;
    }


    public static function getPaymentUuid($external_reference)
    {
        $position = strpos($external_reference, '-');
        $uuid = substr($external_reference, $position+1);
        return $uuid;
    }
    
     public static function getSubscriptionUuid($external_reference)
     {
        $position = strpos($external_reference, '-');
        $uuid = substr($external_reference, 0,$position);
        return $uuid;
    }

    /**
     * Notifica por curl a un sitio los parametros pasados
     *
     * @param type $payment
     * @param type $data
     * @return type
     */
    public static function NotifyToSite($payment, $data = NULL)
    {
        
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        $url = str_replace('www.', '', $payment->result_url);
        curl_setopt($ch, CURLOPT_URL, $url);
        //si tengo que enviar parametros por post
        if ($data) {
            curl_setopt($ch, CURLOPT_POST, count($data));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        if (SiteConfig::get('checkout_mode_testing', $payment->site_id)) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        //execute post
        $result = curl_exec($ch);

        if ($result === false) {
            $result = curl_error($ch);
        }

        //close connection
        curl_close($ch);

        return $result;
    }
    
    public function getExpirationDateFrom()
    {
    }
    
    public function getExpirationDateTo()
    {
        //tiempo máx de vida de una transacción . Minutos
        $timeout = $this->site->platform_timeout;
        
        if(!$timeout){
            return NULL;
        }
       
        $expiration_date =  strtotime("+$timeout minutes",$this->create_datetime);
        $date_ISO_8601 = date('Y-m-d\TH:i:s', $expiration_date) . substr(microtime(), 1, 4) . date('P');
        $this->expiration_date_to = $date_ISO_8601;
        $this->save(['expiration_date_to']);
        
        return $date_ISO_8601;
    }
    
    public static function getStatus()
    {
        return [
            'init' => \quoma\checkout\CheckoutModule::t('Init'),
            'error' => \quoma\checkout\CheckoutModule::t('Error'),
            'paid' => \quoma\checkout\CheckoutModule::t('Paid'),
            'pending' => \quoma\checkout\CheckoutModule::t('Pending'),
            'partial' => \quoma\checkout\CheckoutModule::t('Partial'),
            'timeout' => \quoma\checkout\CheckoutModule::t('Timeout'),
            'canceled' => \quoma\checkout\CheckoutModule::t('Canceled'),
            'rejected' => \quoma\checkout\CheckoutModule::t('Rejected'),
            'refunded' => \quoma\checkout\CheckoutModule::t('Refunded'),
            'in_mediation' => \quoma\checkout\CheckoutModule::t('In Mediation'),
            'charged_back' => \quoma\checkout\CheckoutModule::t('Charged Back'),
            'invalid' => \quoma\checkout\CheckoutModule::t('Invalid'),
        ];
    }

    public function getLabelStatus()
    {
        return $this->getStatus()[$this->status];
    }
    
    public function clonePayment()
    {
        $payment = new WebPayment();
        $payment->load($this->getAttributes(), '');
        $payment->web_payment_id = null;
        $payment->uuid = NULL;
        $payment->status = NULL;
        $payment->setIsNewRecord(true);
        if($payment->save()){
            return $payment;
        }
        return $this;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!empty($this->cybersource_data)){
            $cybersourceData= $this->cybersourceData;
            if (empty($cybersourceData)){
                $cybersourceData= new CybersourceData(array_merge($this->cybersource_data, ['web_payment_id' => $this->web_payment_id, 'site_id' => $this->site_id]));
            }else{
                $cybersourceData->load($this->cybersource_data, '');
            }

            if (!$cybersourceData->save()){
                Yii::info('Cybersource Data not saved', 'Cybersource');
                Yii::info($cybersourceData->getErrors(), 'Cybersource');
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCybersourceData()
    {
        return $this->hasOne(CybersourceData::class, ['web_payment_id' => 'web_payment_id']);
    }

    public function getCybersourceArray()
    {
        $data= $this->cybersourceData;

        if (empty($data)){
            return [];
        }

        $array= [
            'send_to_cs' => true,
            'channel' => 'Web',
            'bill_to' => [
                'city' => $data->city,
                'country' => 'AR',
                'customer_id' => $data->customer_id,
                'email' => $data->email,
                'first_name' => $data->first_name,
                'last_name' => $data->last_name,
                'phone_number' => $data->phone,
                'postal_code' => $data->postal_code,
                'state' => $data->state,
                'street1' => $data->address
            ],
            'currency' => $data->currency,
            'amount' => $data->amount,
            'days_in_site' => $data->days_in_site,
            'is_guest' => $data->is_guest,
            'num_of_transactions' => $data->transactions,
            'cellphone_number' => $data->phone,
            'street' => $data->address,
            'ship_to' => [
                'city' => $data->city,
                'country' => 'AR',
                'customer_id' => $data->customer_id,
                'email' => $data->email,
                'first_name' => $data->first_name,
                'last_name' => $data->last_name,
                'phone_number' => $data->phone,
                'postal_code' => $data->postal_code,
                'state' => $data->state,
                'street1' => $data->address
            ],
            'dispatch_method' => 'storepickup', // TODO: Cambiar valor de acuerdo si se usa o no el modulo tracking o alguna forma de envio
        ];

        return $array;
    }

    public function getCybersourceItemArray()
    {
        $data= $this->cybersourceData;

        if (empty($data)){
            return [];
        }

        $items= [];

        foreach ($data->cybersourceItemDatas as $item){
            $items[]=[
                'csitproductcode' => $item->code,
                'csitproductdescription' => ($item->description ?  $item->description : $item->name),
                'csitproductname' => $item->name,
                'csitproductsku' => $item->sku,
                'csittotalamount' => $item->total_amount,
                'csitquantity' => $item->qty,
                'csitunitprice' => $item->unit_price
            ];
        }

        return $items;
    }

    /**
     * Verifica el estado del pago contra la plataforma
     */
    public function checkPaymentStatus()
    {
        $platform = PlatformFactory::getInstance()->getPlatform($this->platform);
        $platform->checkWebPayment($this);
    }

    /**
     * Cancela un pago pendiente contra la plataforma
     * @return boolean
     */
    public function cancelPayment()
    {
        if($this->status !== 'pending'){
            return false;
        }

        $platform = PlatformFactory::getInstance()->getPlatform($this->platform);
        $status = $platform->cancelPayment($this);

        $this->refresh();

        if($status['status'] == 'success'){
            return true;
        }else{
            return false;
        }
    }

}

