<?php

namespace quoma\checkout\models;

use Yii;

/**
 * This is the model class for table "mp_seller".
 *
 * @property int $mp_seller_id
 * @property int $model_id
 * @property string $model_class
 * @property string $access_token
 * @property string $refresh_token
 * @property int $expiration_time
 * @property string $app_id
 * @property int $site_id
 *
 * @property Site $site
 */
class MpSeller extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mp_seller';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_checkout');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'expiration_time', 'site_id'], 'integer'],
            [['model_class', 'access_token', 'refresh_token', 'app_id'], 'string', 'max' => 255],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'site_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mp_seller_id' => Yii::t('app', 'Mp Seller ID'),
            'model_id' => Yii::t('app', 'Model ID'),
            'model_class' => Yii::t('app', 'Model Class'),
            'access_token' => Yii::t('app', 'Access Token'),
            'refresh_token' => Yii::t('app', 'Refresh Token'),
            'expiration_time' => Yii::t('app', 'Expiration Time'),
            'app_id' => Yii::t('app', 'App ID'),
            'site_id' => Yii::t('app', 'Site ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }

    public function loadModel(){
        $modelClass = $this->model_class;
        return $modelClass::findOne($this->model_id);

    }
}
