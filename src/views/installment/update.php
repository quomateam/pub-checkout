<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\Installment */

$this->title = Yii::t('app', 'Update'). ' ' .\quoma\checkout\CheckoutModule::t('Installment'). ' ' . $model->installment_id;
$this->params['breadcrumbs'][] = ['label' => \quoma\checkout\CheckoutModule::t('Installments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->installment_id, 'url' => ['view', 'id' => $model->installment_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="installment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
