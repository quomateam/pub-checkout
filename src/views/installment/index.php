<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use quoma\checkout\CheckoutModule;

/* @var $this yii\web\View */
/* @var $searchModel quoma\checkout\models\search\InstallmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \quoma\checkout\CheckoutModule::t('Installments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="installment-index">
    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-plus'></span> " . Yii::t('app', 'Create'). ' ' .
                    CheckoutModule::t('Installment'), ['create'], ['class' => 'btn btn-success']);?>
        </p>
    </div>
    
    
<!--     <?// ExportMenu::widget([
//        'dataProvider' => $dataProvider,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//            'key',
//            'qty',
//        ],
//        'showConfirmAlert'=>false
//    ]); ?>-->


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'key',
            'qty',

            [
                'class' => 'quoma\core\grid\ActionColumn',
            ],
        ],
    ]); ?>

</div>
