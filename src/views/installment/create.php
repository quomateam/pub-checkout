<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\Installment */

$this->title = Yii::t('app', 'Create'). ' ' .\quoma\checkout\CheckoutModule::t('Installments');
$this->params['breadcrumbs'][] = ['label' => \quoma\checkout\CheckoutModule::t('Installment'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="installment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
