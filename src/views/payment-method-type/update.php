<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\PaymentMethodType */

$this->title = Yii::t('app', 'Update').' '.quoma\checkout\CheckoutModule::t('Payment Method Type'). ' ' . $model->name;

$this->params['breadcrumbs'][] = ['label' => quoma\checkout\CheckoutModule::t('Payment Method Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->payment_method_type_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="payment-method-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
