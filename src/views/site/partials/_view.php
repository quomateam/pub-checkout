<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="panel panel-primary">

    <div class="panel-heading">
        <h3 class="panel-title">Configuración para <?= $platform->name ?></h3>
    </div>
    <div class="panel-body">

        <div class="col-sm-6">

            <h4>Métodos de pago permitidos</h4>
            <hr>

            <?= \yii\helpers\Html::activeCheckboxList($model, 'paymentMethods', \yii\helpers\ArrayHelper::map($payment_methods, 'payment_method_id', 'name'), ['separator' => '<br/>']); ?>

        </div>

        <div class="col-sm-6">
            <div class="row">
                <h4>Cuotas permitidas</h4>
                <hr>
                <div class="col-sm-10">
                    <?= \yii\helpers\Html::activeCheckboxList($model, 'installments', \yii\helpers\ArrayHelper::map($installments, 'installment_id', 'qty'), ['separator' => '<br/>'])?>
                </div>
            </div>

        </div>
        <div class="col-sm-6">
            <h4>Minutos disponibles para realizar una compra.</h4>
            <hr>
            <?= \yii\helpers\Html::input('text', 'Site[platform_timeout]',$model->platform_timeout,['class' => 'form-control'])?>
        </div>
    </div>

</div>

