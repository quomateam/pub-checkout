<?php

use yii\helpers\Html;
use yii\grid\GridView;
use quoma\checkout\CheckoutModule;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\checkout\models\search\SiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = CheckoutModule::t('Sites');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-plus'></span> " . Yii::t('app', 'Create') . ' ' . 
                    CheckoutModule::t('Site'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'server_name',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return (!empty($data->status) ) ? $data->getLabelStatus() : '-';
                },
                'filter' => \quoma\checkout\models\Site::getStatus(),
            ],
            ['class' => 'quoma\core\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
