<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */


?>

    <div class="site-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col-lg-6 ">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 ">
            <?= $form->field($model, 'admin_email')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-lg-6 ">
            <?= $form->field($model, 'server_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 ">
            <?= $form->field($model, 'status')->dropDownList(['enabled' => quoma\checkout\CheckoutModule::t('Enabled'), 'disabled' => quoma\checkout\CheckoutModule::t('Disabled'),]) ?>
        </div>

            <?= $form->field($model, 'platform_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\quoma\checkout\models\Platform::find()->where('status="enabled"')->all(), 'platform_id', 'name'),
                'language' => 'es',
                'options' => ['id' => 'platform-select', 'placeholder' => 'Seleccione una plataforma...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
            <div id="config_platform">
                <?php if (!$model->isNewRecord): ?>
                    <?= $this->render('partials/_view', [
                        'model' => $model,
                        'payment_methods' => $model->platform->paymentMethods,
                        'installments' => \quoma\checkout\models\Installment::find()->all(),
                        'platform' => $model->platform,
                    ]) ?>
                <?php endif; ?>
            </div>

            <div class="form-group<?php if ($model->hasErrors('paymentMethods')) echo ' has-error' ?>">

            </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></div>

        <?php ActiveForm::end(); ?>

    </div>

    <script>

        var Platforms = new function () {

            this.init = function () {

                $(document).ready(function () {


                    var platform_id =  $("#platform-select").val();

                    if(platform_id){

                        var data = new Object;
                        data.select = platform_id;
                        data.site_id = <?= ($model->site_id) ? $model->site_id : NULL ?>

                            getConfig(data);

                    }


                    $('#platform-select').on('change', function () {

                        var data = new Object;
                        data.select = $(this).val();
                        <?php if(!$model->isNewRecord):?>
                        data.site_id = <?= $model->site_id?>
                        <?php endif;?>

                            getConfig(data);

                    });
                });
            };


            function getConfig(data){
                $.ajax({
                    url: '<?= yii\helpers\Url::toRoute(['site/get-config']); ?>',
                    data: data,
                    dataType: 'json',
                    type: 'post'
                }).done(function (json) {
                    if (json.status === 'success') {
                        $('#config_platform').html(json.html);
                    }
                });
            }
        };


    </script>

<?php
$this->registerJs('Platforms.init();');

        