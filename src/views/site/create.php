<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Site */

$this->title = Yii::t('app', 'Create').' '. quoma\checkout\CheckoutModule::t('Site');
$this->params['breadcrumbs'][] = ['label' => quoma\checkout\CheckoutModule::t('Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
