<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Site */

$this->title = Yii::t('app', 'Update') .' '. quoma\checkout\CheckoutModule::t('Site'). ' ' . $model->name;

$this->params['breadcrumbs'][] = ['label' => quoma\checkout\CheckoutModule::t('Site'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->site_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="site-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
