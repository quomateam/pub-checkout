<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Platform */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => quoma\checkout\CheckoutModule::t('Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platform-view">
    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-pencil'></span> " . Yii::t('app', 'Update'),
                    ['update', 'id' => $model->platform_id], ['class' => 'btn btn-primary']) ?>
            <?php if($model->deletable)
                echo Html::a("<span class='glyphicon glyphicon-remove'></span> " . Yii::t('app', 'Delete'), ['delete', 'id' => $model->platform_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'main_class',
            [
                'attribute'=>'status',
                'value'=> (!empty($model->status) ) ? $model->getLabelStatus() : '-',
            ],
        ],
    ]) ?>

</div>
