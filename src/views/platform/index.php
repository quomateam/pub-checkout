<?php

use yii\helpers\Html;
use yii\grid\GridView;
use quoma\checkout\CheckoutModule;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\checkout\models\search\PlatformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = quoma\checkout\CheckoutModule::t('Platforms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platform-index">
    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-plus'></span> " . Yii::t('app', 'Create').' ' . 
                    CheckoutModule::t('Platform'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'main_class',
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return (!empty($data->status) ) ? $data->getLabelStatus() : '-';
                },
                'filter' => \quoma\checkout\models\Platform::getStatus(),
            ],

            ['class' => 'quoma\core\grid\ActionColumn'],
        ],
    ]); ?>

</div>
