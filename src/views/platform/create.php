<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Platform */

$this->title = \Yii::t('app', 'Create').' '. quoma\checkout\CheckoutModule::t('Platform');
$this->params['breadcrumbs'][] = ['label' => quoma\checkout\CheckoutModule::t('Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="platform-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
