<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\PaymentMethod */

$this->title = Yii::t('app', 'Create'). ' ' .\quoma\checkout\CheckoutModule::t('Payment Method');
$this->params['breadcrumbs'][] = ['label' => \quoma\checkout\CheckoutModule::t('Payment Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-method-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
