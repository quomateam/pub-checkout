<?php

use yii\helpers\Html;
use yii\grid\GridView;
use quoma\checkout\CheckoutModule;
use yii\helpers\ArrayHelper;
use quoma\checkout\models\Platform;

/* @var $this yii\web\View */
/* @var $searchModel quoma\checkout\models\search\PaymentMethodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \quoma\checkout\CheckoutModule::t('Payment Methods');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-method-index">
    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-plus'></span> " . Yii::t('app', 'Create') . ' ' .
                    CheckoutModule::t('Payment Method'), ['create'], ['class' => 'btn btn-success']);
            ?>
        </p>
    </div>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'code',
            'name',
            [
                'attribute' => 'payment_method_type',
                'value' => function($data){
                    return ($data->paymentMethodType) ? $data->paymentMethodType->name : '-';
                },
                'filter' => ArrayHelper::map(\quoma\checkout\models\PaymentMethod::find()->all(),'payment_method_id','name'),
            ],
            [
                'attribute' => 'platform_id',
                'value' => function($data){
                    return ($data->platform) ? $data->platform->name : '-';
                },
                'filter' => ArrayHelper::map(Platform::findAll(['status' => "enabled"]), 'platform_id', 'name'),
            ],
            [
                'attribute' => 'accreditation_time',
                'value' => function($data){return $data->accreditation_time .' Minutos';}
            ],
            [
                'class' => 'quoma\core\grid\ActionColumn',
            ],
        ],
    ]);
    ?>

</div>
