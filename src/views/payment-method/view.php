<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\PaymentMethod */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => \quoma\checkout\CheckoutModule::t('Payment Methods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-method-view">
    <div class="title">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a("<span class='glyphicon glyphicon-pencil'></span> " . Yii::t('app', 'Update'), ['update', 'id' => $model->payment_method_id], ['class' => 'btn btn-primary'])
            ?>
            <?php
            if ($model->deletable)
                echo Html::a("<span class='glyphicon glyphicon-remove'></span> " . Yii::t('app', 'Delete'), ['delete', 'id' => $model->payment_method_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>
        </p>
    </div>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            'name',
            [
                'attribute' => 'payment_method_type',
                'value' => $model->paymentMethodType->name,
            ],
            [
                'attribute' => 'platform_id',
                'value' => (!empty($model->platform) ) ? $model->platform->name : '-',
            ],
//            'accreditation_time',
        ],
    ])
    ?>

</div>
