<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use quoma\checkout\models\Platform;
use yii\helpers\ArrayHelper;
use quoma\checkout\models\PaymentMethodType;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\PaymentMethod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-method-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'code')->textInput() ?>

    <?= $form->field($model, 'payment_method_type_id')->dropDownList(ArrayHelper::map(PaymentMethodType::find()->all(), 'payment_method_type_id', 'name'), ['prompt' => 'Seleccione']) ?>

    <?= $form->field($model, 'platform_id')->dropDownList(ArrayHelper::map(Platform::findAll(['status' => "enabled"]), 'platform_id', 'name'), ['prompt' => 'Seleccione']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
