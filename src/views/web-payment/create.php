<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\WebPayment */

$this->title = Yii::t('app', 'Create Web Payment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Web Payments'), 'url' => ['index', 'site_id' => $model->site_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-payment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
