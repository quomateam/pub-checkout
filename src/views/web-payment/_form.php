<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\WebPayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="web-payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList([
        'init' => \quoma\checkout\CheckoutModule::t('Init'),
        'error' => \quoma\checkout\CheckoutModule::t('Error'),
        'paid' => \quoma\checkout\CheckoutModule::t('Paid'),
        'pending' => \quoma\checkout\CheckoutModule::t('Pending'),
    ]) ?>

    <?= $form->field($model, 'message')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'concept')->textInput(['maxlength' => 255, 'disabled' => 'disabled']) ?>

    <?= $form->field($model, 'amount')->textInput(['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'uuid')->textInput(['maxlength' => 45])->textInput(['disabled' => 'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
