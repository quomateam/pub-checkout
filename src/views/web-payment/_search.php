<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\search\WebPaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="web-payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'web_payment_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'message') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'uuid') ?>

    <?php // echo $form->field($model, 'concept') ?>

    <?php // echo $form->field($model, 'platform_id') ?>

    <?php // echo $form->field($model, 'return_url') ?>

    <?php // echo $form->field($model, 'invoice') ?>

    <?php // echo $form->field($model, 'site_id') ?>

    <?php // echo $form->field($model, 'bill_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
