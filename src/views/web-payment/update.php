<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\WebPayment */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Web Payment',
]) . ' ' . $model->web_payment_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Web Payments'), 'url' => ['index', 'site_id' => $model->site_id]];
$this->params['breadcrumbs'][] = ['label' => $model->web_payment_id, 'url' => ['view', 'web_payment_id' => $model->web_payment_id, 'site_id' => $model->site_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="web-payment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
