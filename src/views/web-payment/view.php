<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model quoma\checkout\models\WebPayment */

$this->title = \quoma\checkout\CheckoutModule::t('Payment').' '.$model->web_payment_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Web Payments'), 'url' => ['index', 'site_id' => $model->site_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'web_payment_id',
            'request_uuid',
            'labelStatus',
            'message',
            'amount',
            'uuid',
            'concept',
            [
                'attribute' => 'platform_id',
                'value' => $model->platform->name
            ],
            'return_url:url',
        ],
    ])
    ?>

    <?php if($model->receipts): ?>
        <h3><?= \quoma\checkout\CheckoutModule::t('Receipt') ?></h3>
        <?php foreach ($model->receipts as $receipt) : ?>
        <?=
            DetailView::widget([
            'model' => $receipt,
            'attributes' => [
                'web_receipt_id', // title attribute (in plain text)
                'currency', // description attribute in HTML
                'datetime',
                'autorization_code',
                'installments',
                'owner',
                'amount',
                'card',
                'email',
                'operation_number',
                'status',
                'visa_address_validation',
                'visa_vbv_auth',
                'web_payment_id'
            ],
        ]);

        ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
