<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\checkout\models\Payment */

$this->title = quoma\checkout\CheckoutModule::t('Select Platform');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payments'), 'url' => ['#']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="checkout-default-index">

    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?= quoma\checkout\CheckoutModule::t('Select a payment method') ?></h3>
            </div>
            <div class="panel-body">
                <?php foreach ($platforms as $platform): ?>
                    <p class="col-lg-6">
                        <a class="btn btn-success btn-lg btn-block" href="<?= yii\helpers\Url::toRoute(['pay', 'web_payment_id' => $web_payment_id, 'platform' => $platform->platform_id]) ?>">
                            <?= $platform->name ?>
                        </a>
                    </p>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>